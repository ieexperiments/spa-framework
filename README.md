# README #

This is a rapid single page application framework. This app has been used as a core code base for many line of real time data driven business applications.

This App is built upon on a custom MEAN stack using the following technologies. 

Node Js 7x
Kendo UI
Redis
ES6
PM2
CENTOS 7

### getting Started ###

1. Run npm install (you might have to insert "sudo" before or "sudo !!" after)
2. After the node_modules are install run "pm2 start pm2.json"
3. Make sure redis is running by typing "sudo systemctl status redis.service"
4. If redis is not running then run the command "sudo systemctl status redis.service"
5. Go to your browser and type in "localhost:8090"

### If you run into problems ###
1. This app was built and tested on CENTOS 7 and is developed to run on this type of box. 

2. If you are trying to run the app on a CENTOS box it may require custom configuration.