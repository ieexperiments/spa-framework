/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const processManager = require('../shared/process-manager');

class Server{
	constructor(){
		global.pm = new processManager({
			main: this,
			appServer: true,
			modules: {
				Websocket: './lib/websocket',
				Feed: './lib/feed',
				Binary: 'shared/binary'
			}
		});

		global.redisClient = new RedisClient();
		global.server = new AppServer(conf.feedServer);
	}
	masterReady(){
		if(conf.appServer.mode === 'development'){
			let compiler = new Compiler();
		}
	}
	workerReady(pm){
		global.pm = pm;

		global.sqlite = new SQLite('app');

	}
}

new Server();
