/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const fs = require('fs');
const node7z = require('node-7z');
const extractor = new node7z();

const ApplicationFeed = {
  offset: 0,
	loadFile(){
		let fn = 'min' + Utils.lpad(this.offset, '0', 4) + '.bin';
		let src = '/opt/playback/' + fn + '.7z';
		let dst = './temp/' + fn;
		let proc = extractor.extractFull(src, './temp/');

		proc.then(function(){
			this.reader = new Binary.reader(fs.readFileSync(dst, 'binary'));
			setTimeout(function(){ this.parseFile(); }.bind(this), 1);
			fs.unlinkSync(dst);
		}.bind(this));

		proc.catch(function(err){
			console.error('error', err);
		});
	},
	parseFile(){
		this.writeHeader();
		this.getSecond();
	},
	getSecond(){
		if(this.reader.eof){
			this.offset++;
			this.loadFile();
			return;
		}

		this.writeSecond();

		if(Websocket.client){
			setTimeout(function(){ this.getSecond(); }.bind(this), 1000);
		}
	},
	writeSecond(){
		let len1 = this.reader.uint32();
    let raw1 = this.reader.raw(len1);
    let len2 = this.reader.uint32();
    let raw2 = this.reader.raw(len2);

		Websocket.send(2, raw1);
		Websocket.send(3, raw2);
	},
	writeHeader(){
    let len = this.reader.uint32();//,

		Websocket.send(1, this.reader.raw(len));
  }
};

module.exports = ApplicationFeed;
