
/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const ApplicationWebsocket = {
  register(conn){
		this.client = conn.socket;
		this.client.on('close', function(){ this.terminate(); }.bind(this));
		Feed.offset = 700;
		Feed.loadFile();
	},
	send(type, buffer){
		let writer = new Binary.writer();

		if(!this.client){ return; }

		writer.int8(type);
		writer.raw(buffer);

		this.client.send(writer.flush());
	},
	terminate: function(){
    this.client = false;
	}
};

module.exports = ApplicationWebsocket;
