/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const net = require('net');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder();

class JsonSocket{
	constructor(socket){
	this.socket = socket;
		this.contentLength = null;
		this.buffer = '';
		this.closed = false;
		socket.on('data', this.onData.bind(this));
		socket.on('connect', this.onConnect.bind(this));
		socket.on('close', this.onClose.bind(this));
		socket.on('err', this.onError.bind(this));
	}
	onData(data){
		data = decoder.write(data);

		console.log(data);

		try{
			this.handleData(data);
		} catch (e){
			this.sendError(e);
		}
	}
	handleData(data){
		this.buffer += data;

		if(this.contentLength === null){
			let i = this.buffer.indexOf('#');
			//Check if the buffer has a #, if not, the end of the buffer string might be in the middle of a content length string
			if(i !== -1){
				let rawContentLength = this.buffer.substring(0, i);
				this.contentLength = parseInt(rawContentLength);
				if(isNaN(this.contentLength)){
					this.contentLength = null;
					this.buffer = '';
					let err = new Error('Invalid content length supplied ('+rawContentLength+') in: '+ this.buffer);
					err.code = 'E_INVALID_CONTENT_LENGTH';
					throw err;
				}

				this.buffer = this.buffer.substring(i+1);
			}
		}

		if(this.contentLength !== null){
			let length = Buffer.byteLength(this.buffer, 'utf8');
			if(length === this.contentLength){
				this.handleMessage(this.buffer);
			} else if(length > this.contentLength){
				let message = this.buffer.substring(0, this.contentLength);
				let rest = this.buffer.substring(this.contentLength);
				this.handleMessage(message);
				this.onData(rest);
			}
		}
	}
	handleMessage(data){
		this.contentLength = null;
		this.buffer = '';
		let message;

		try{
			message = JSON.parse(data);
		} catch(e){
			let err = new Error('Could not parse JSON: '+e.message+'\nRequest data: '+data);
			err.code = 'E_INVALID_JSON';
			throw err;
		}

		message = message || {};
		this.socket.emit('message', message);
	}
	sendError(err){
		this.sendMessage(this.formatError(err));
	}
	sendEndError(err){
		this.sendEndMessage(this.formatError(err));
	}
	formatError(err){
		return {success: false, error: err.toString()};
	}
	sendMessage(message, callback){
		if(this.closed){
			if(callback){
				callback(new Error('The socket is closed.'));
			}
			return;
		}

		this.socket.write(this.formatMessageData(message), 'utf-8', callback);
	}
	sendEndMessage(message, callback){
		this.sendMessage(message, function(err){
			if(callback){ callback(err); }
		}.bind(this));
	}
	formatMessageData(message){
			let messageData = JSON.stringify(message);
			let length = Buffer.byteLength(messageData, 'utf8');
			let data = length + '#' + messageData;
			return data;
	}
	onClose(){
			this.closed = true;
	}
	onConnect(){
			this.closed = false;
	}
	onError(){
			this.closed = true;
	}
	isClosed(){
			return this.closed;
	}
}
