/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const async = require('async');

const Utils = {
	uuid(){
		let date = new Date().getTime();

		let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(char){
			let random = (date + Math.random() * 16) % 16 | 0;
			date = Math.floor(date / 16);
			return (char === 'x' ? random : (random & 0x7 | 0x8)).toString(16);
		});

	return uuid;
	},
	asyncAppend(async, module, method, payload){
		async.push(this.asyncFunc(module, method, payload));
	},
	asyncFunc(module, method, payload){
		return function(callback){ module[method](payload, callback); };
	},
	asyncBatch(obj, callback){
		let isArray = this.isArray(obj.payload);
		let list = isArray ? [] : {};

		obj.async = obj.async || 'series';
		callback = callback || function(){};

		if(isArray){
			for(let item of obj.payload){
				list.push(this.asyncFunc(obj.module, obj.method, item));
			}
		} else {
			for(let [key, payload] of Object.entries(obj.payload)){
				list[key] = this.asyncFunc(obj.module, obj.method, payload);
			}
		}

		if(obj.async === 'series'){
			async.series(list, callback);
		} else {
			async.parallel(list, callback);
		}
	},
	isNumber(val){
		return (typeof val === 'number');
	},
	isInteger(val){
		let x = parseFloat(val);
		return !isNaN(val) && (x | 0) === x;
	},
	isString(val){
		return (val instanceof String);
	},
	isArray(val){
		return (val instanceof Array);
	},
	isBoolean(val){
		return (typeof val === 'boolean');
	},
	isObject(val){
		return (val instanceof Object);
	},
	isDate(val){
		return (val instanceof Date);
	},
	isFunction(val){
		return (typeof val === 'function');
	},
	isUndefined(val){
		return (val === undefined);
	},
	isNull(val){
		return (val === null || val === '');
	},
	isNullOrUndefined(val){
		return (this.isNull(val) || this.isUndefined(val) || val === '');
	},
	isBetween(num, min, max){
		return (num <= max && num >= min);
	},
	lpad(value, pad, len){
		let str = this.isNumber(value) ? value.toString() : value;
		while(str.length < len){ str = pad + str; }
		return str;
	},
	rpad(value, pad, len){
		let str = this.isNumber(value) ? value.toString() : value;
		while(str.length < len){ str = str + pad;  }
		return str;
	},
	trim(str){
		return str.replace(/^\s+|\s+$/g, '');
	},
	arrayFirst(arr){
		return arr[0];
	},
	arrayLast(arr){
		return arr[arr.length - 1];
	},
	arrayRemove(arr, item){
		if(arr.indexOf(item) < 0){ return; }
		arr.splice(arr.indexOf(item), 1);
	},
	toRadians(num){
		return num * Math.PI / 180;
	},
	toDegrees(num){
		return num * 180 / Math.PI;
	},
	precision(value, precision){
		if(!precision){ return Math.round(value); }

		let multiplier = Math.pow(10, precision || 0);
		return Math.round(value * multiplier) / multiplier;
	},
	validateEmail: function(email){
		let regexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
		return regexp.test(email);
	},
	formatPhone: function(phone){
		phone = phone.toString().replace(/\D/g, '');
		let match = phone.match(/^(\d{3})(\d{3})(\d{4})$/);
		return (!match) ? null : '(' + match[1] + ') ' + match[2] + '-' + match[3];
	},
	formatBytes: function(size){
		let idx = 0;
		let units = [' Bytes',' KB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];

		while(size > 1000){
			size = size / 1000;
			idx++;
		}

		return Math.max(size, 0.1).toFixed(1) + units[idx];
	},
	sortObjArray(arr, prop){
		arr.sort(function(a, b){
			if(a[prop] < b[prop]){ return -1; }
			if(a[prop] > b[prop]){ return 1; }
			return 0;
		});

		return arr;
	}
};

module.exports = Utils;
