/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const int24 = require('int24');
const geoComp = (360 / Math.pow(2, 24));
const geoReduce = 4;

class BinaryReader{
	constructor(raw, buffer){
		this.setBuffer(raw, buffer);
	}
	concat(buffer1, buffer2){
		let size = buffer1.length + buffer2.length;

		return Buffer.concat([buffer1, buffer2], size);
	}
	setBuffer(raw, buffer){
		this.buffer = buffer ? buffer : new Buffer(raw, 'binary');
		this.offset = 0;
		this.len = this.buffer.length;
		this.eof = false;
	}
	setOffset(len){
		this.offset += len;
		if(this.offset === this.len){ this.eof = true; }
	}
	string(len){
		if(!len){ return null; }

		let str = this.buffer.toString('UTF8', this.offset, this.offset + len);
		this.setOffset(len);
		return str;
	}
	uint32(){
		let num = this.buffer.readUInt32BE(this.offset);
		this.setOffset(4);
		return num;
	}
	uint24(){
		let num = int24.readUInt24BE(this.buffer, this.offset);
		this.setOffset(3);
		return num;
	}
	uint16(){
		let num = this.buffer.readUInt16BE(this.offset);
		this.setOffset(2);
		return num;
	}
	uint8(){
		let num = this.buffer.readUInt8(this.offset);
		this.setOffset(1);
		return num;
	}
	int32(){
		let num = this.buffer.readInt32BE(this.offset);
		this.setOffset(4);
		return num;
	}
	int24(){
		let num = int24.readInt24BE(this.buffer, this.offset);
		this.setOffset(3);
		return num;
	}
	int16(){
		let num = this.buffer.readInt16BE(this.offset);
		this.setOffset(2);
		return num;
	}
	int8(){
		let num = this.buffer.readInt8(this.offset);
		this.setOffset(1);
		return num;
	}
	timestamp(){
		let high = this.uint32();
		let low = this.uint16();

		return Number((high.toString() + '0000')) + Number(low.toString());
	}
	raw(len){
		let buffer = new Buffer(len).fill(0);

		this.buffer.copy(buffer, 0, this.offset, (this.offset + len));
		this.setOffset(len);

		return buffer;
	}
	tell(){
		return this.offset;
	}
}

class BinaryWriter{
	constructor(){
		this.buffer = new Buffer(4096).fill(0);
		this.offset = 0;
	}
	extend(size){
		let remaining = this.buffer.length - this.offset;
		let len = size > 4096 ? size : 4096;

		if(remaining < size){
			let buffer = this.buffer;
			this.buffer = new Buffer(buffer.length + len).fill(0);
			buffer.copy(this.buffer);
		}
	}
	raw(buffer){
		let len = buffer.length;

		if(!len){ return; }

		this.extend(len);
		buffer.copy(this.buffer, this.offset);
		this.offset += len;
	}
	int32(num = 0){
		this.extend(4);
		this.buffer[this.offset++] = (num >>> 24 & 0xFF);
		this.buffer[this.offset++] = (num >>> 16 & 0xFF);
		this.buffer[this.offset++] = (num >>>  8 & 0xFF);
		this.buffer[this.offset++] = (num >>>  0 & 0xFF);
	}
	int24(num = 0){
		this.extend(3);
		this.buffer[this.offset++] = (num >>> 16 & 0xFF);
		this.buffer[this.offset++] = (num >>>  8 & 0xFF);
		this.buffer[this.offset++] = (num >>>  0 & 0xFF);
	}
	int16(num = 0){
		this.extend(2);
		this.buffer[this.offset++] = (num >>>  8 & 0xFF);
		this.buffer[this.offset++] = (num >>>  0 & 0xFF);
	}
	int8(num = 0){
		this.extend(1);
		this.buffer[this.offset++] = (num >>>  0 & 0xFF);
	}
	string(string){
		let len = Buffer.byteLength(string);

		if(Buffer.byteLength(string) !== string.length){ console.log('len', string); }
		if(!len){ return; }

		this.extend(len);
		this.buffer.write(string, this.offset, len);
		this.offset += len;
	}
	timestamp(ts = 0){
		ts = typeof(ts) === 'object' ? ts.getTime().toString() : ts.toString();
		let high = ts.substr(0, ts.length - 4);
		let low = ts.substr(-4);

		this.extend(6);
		this.int32(high);
		this.int16(low);
	}
	jsonBuffer(json){
		if(!json.length){ return; }

		for(let num of json){
			this.int8(num);
		}
	}
	copy(){
		let buffer = new Buffer(this.offset).fill(0);

		this.buffer.copy(buffer, 0, 0, this.offset);

		return buffer;
	}
	flush(){
		let buffer = this.buffer.slice(0, this.offset);

		this.buffer = new Buffer(4096).fill(0);
		this.offset = 0;

		return buffer;
	}
	tell(){
		return this.offset;
	}
}

const EncodePoint = function(point){
	let lon = point[0];
	let lat = point[1];

	if(lon < 0){ lon = 360 + lon; }
	if(lat < 0){ lat = 180 + lat; }

	lon = Math.round((lon / geoComp) / geoReduce);
	lat = Math.round((lat / geoComp) / geoReduce);

	return [lon, lat];
};

const DecodePoint = function(point){
	let lon = point[0];
	let lat = point[1];

	lon = (lon * geoComp) * geoReduce;
	lat = (lat * geoComp) * geoReduce;

	if(lon > 180){ lon = lon - 360; }
	if(lat > 90){ lat = lat - 180; }

	return [lon, lat];
};

const BitSubstr = function(bytes, msb, lsb){
	return (bytes >>> (lsb - 1)) & (0xFFFFFFFF >>> (32 - (msb - lsb + 1)));
};

module.exports = {
	reader: BinaryReader,
	writer: BinaryWriter,
	endcodePoint: EncodePoint,
	decodePoint: DecodePoint,
	bitSubstr: BitSubstr
};
