/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
global.Path = require('path');
global.conf = require('./conf');
const	cluster = require('cluster');
const os = require('os');

class ProcessManager{
	constructor(opts){
		opts = Object.assign({
				modules: {},
				onMasterReady: function(){},
				onWorkerReady: function(){},
				onMessage: function(){}
			}, opts);
		this.opts = opts;
		this.setPaths();
		this.setModules(opts);
		this.setClusters();
	}
	setClusters(){
		if(!this.opts.cluster){ return; }

		let threads = this.opts.threads || os.cpus().length;

		if(cluster.isMaster){
			for(let idx = 0; idx < threads; idx++){
				let worker = cluster.fork();
				this.setMessaging(worker);
			}

			this.opts.onMasterReady();
		} else {
			this.opts.onWorkerReady(this);
		}
	}
	setMessaging(worker){
		worker.on('message', function(msg){
			this.broadcast(msg);
		}.bind(this));
	}
	setModules(opts){
		const regex1 = /^(shared|app-server)\/.*$/;
		const regex2 = /^.\/.*$/;
		let modules = Object.assign(opts.modules, conf.defaultModules);

		if(opts.appServer){
			Object.assign(modules, {AppServer: 'app-server/server'});
		}

		for(let [key, value] of Object.entries(opts.modules)){
			if(global[key]){
				console.log('Cannot set global property', key);
				process.exit();
			}

			if(regex1.test(value)){
				global[key] = require(this.paths.base + value);
			} else if(regex2.test(value)) {
				global[key] = require(Path.resolve(this.paths.current, value));
			} else {
				global[key] = require(value);
			}
		}
	}
	broadcast(msg){
		if(cluster.isWorker){
			process.send(msg);
		} else {
			for(let [key, worker] of Object.entries(cluster.workers)){
				worker.send(msg);
			}
		}
	}
	setPaths(){
		const basePath = Path.dirname(__dirname);

		this.paths = {
			base: basePath + '/',
			current: Path.dirname(process.mainModule.filename) + '/',
			shared: Path.resolve(basePath, './shared' + '/')
		};

		for(let [key, value] of Object.entries(conf.directories)){
			this.paths[key] = Path.resolve(basePath, value) + '/';
		}
	}
}

module.exports = ProcessManager;
