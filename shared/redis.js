/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const redis = require('redis');
const conf = require('./conf');

class RedisClient{
	constructor(conn = conf.redis){
		this._client = redis.createClient(conn);
	}
	get client(){
		return this._client;
	}
}

module.exports = RedisClient;
