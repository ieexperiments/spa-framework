/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
module.exports = {
	application: {
		//title: 'Application Builder',
		//description: 'HTML5 rapid GIS application builder',
		title: 'E-IDS Demo',
		description: 'Produced by Concept Solutions',
		theme: 'metro',
		termsOfUse: {
			force: true
		},
		baseFont: {
			font: 'Arial, Helvetica, sans-serif',
			size: '14px'
		}
	},
	appServer: {
		mode: 'development',
		//mode: 'production',
		port: 8080,
		logging: false,
		secure: false,
		webServer: true,
		mapServer: true,
		socketServer: true,
		maxUpload: '100mb',
		ajaxRoutes: {
			logout: {module: 'security', method: 'logout'},
			ping: {module: 'security', method: 'ping'},
			clientData: {module: 'account', method: 'clientData'},
			adminGrid: {module: 'admin', method: 'adminGrid'},
			accountMerge: {module: 'admin', method: 'accountMerge'},
			globalSearch: {module: 'search', method: 'globalSearch'},
			uasSubmit: {module: 'uas', method: 'submit'},
			uasAllGrid: {module: 'uas', method: 'uasAllGrid'},
			uasUserGrid: {module: 'uas', method: 'uasUserGrid'},
			getUserSchedules: {module: 'uas', method: 'getUserSchedules'},
			uasUpdate: {module: 'uas', method: 'update'},
			uasCalendar: {module: 'uas', method: 'calendar'},
			fsAdd: {module: 'filesystem', method: 'add'},
			fsTree: {module: 'filesystem', method: 'getTree'},
			fsRename: {module: 'filesystem', method: 'rename'},
			fsMove: {module: 'filesystem', method: 'move'},
			arptDiagramSearch: {module: 'airports', method: 'diagramSearch'},
			arptDiagramGet: {module: 'airports', method: 'diagramGet'},
			pirepSubmit: {module: 'pirep', method: 'submit'},
			pirepAll: {module: 'pirep', method: 'getAllPireps'},
			suaSchedules: {module: 'sua', method: 'schedules'}
		}
	},
	feedServer: {
		mode: 'development',
		port: 8090,
		logging: false,
		secure: false,
		webServer: false,
		mapServer: false,
		socketServer: true
	},
	redis: {
			host: 'localhost',
			port: 6379
	},
	sessionOptions: {
		resave: false,
		saveUninitialized: true,
		secret: '$*%@er0Sp@ti@1%*$',
		name: 'sid'
	},
	mbtiles: {
		path: 'opt/mbtiles',
		tiles: ['osm', 'custom', 'weather']
	},
	directories: {
		shared: './shared',
		logs: './logs',
		data: './data',
		appServer: './app-server',
		appDev: './application/src',
		appProd: './application/dist',
		appHtdocs: './application/htdocs',
		favicon: './application/htdocs/favicon',
		upload: './data/upload'
	},
	defaultModules: {
		async: 'async',
		fs: 'fs',
		moment: 'moment',
		Utils: 'shared/utils',
		RedisClient: 'shared/redis'
	}
};
