/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const fs = require('fs-extra');
const mime = require('mime');
const Path = require('path');
const Readline = require('readline');
const csv = require('fast-csv');
const request = require('request');

const FileManager = {
	download(uri, path, callback){
		let req = request(uri);
		let stream = fs.createWriteStream(path);

		req.pipe(stream);

		req.on('end', function(response, body){
			stream.end();
			callback();
		});
	},
	readFile(path, toString){
		let file = fs.readFileSync(path);

		if(toString){
			file = file.toString();
		}

		return file;
	},
	readJSON(path){
		if(!this.exists(path)){ return false; }
		return JSON.parse(fs.readFileSync(path));
	},
	writeFile(path, content){
		fs.writeFileSync(path, content);
	},
	writeJSON(path, json, indent){
		fs.writeFileSync(path, JSON.stringify(json, false, indent));
	},
	readCSV(obj){
		let stream = fs.createReadStream(obj.path, {encoding: 'utf8'});
		let parser = csv();
		let arr = [];

		obj.onClose = obj.onClose || function(){};

		parser.on('data', function(data){
			arr.push(data);
		});

		parser.on('end', function(){
			obj.onClose(arr);
		});

		stream.pipe(parser);
	},
	readLines(obj){
		let stream = fs.createReadStream(obj.path, {encoding: 'utf8'});
		let reader = Readline.createInterface({input: stream});
		let count = 0;
		obj.onLine = obj.onLine || function(){};
		obj.onClose = obj.onClose || function(){};

		reader.on('line', function(line){
			count++;
			obj.onLine(line, count);
		});

		reader.on('close', function(){
			obj.onClose(count);
		});
	},
	copy(target, dest){
		fs.copySync(target, dest);
	},
	link(target, dest){
		if(this.exists(dest)){ this.unlink(dest); }
		fs.linkSync(target, dest);
	},
	unlink(path){
		if(!this.exists(path)){ return; }
		fs.unlinkSync(path);
	},
	mkdir(path){
		if(this.exists(path)){ return; }

		fs.mkdirSync(path);
	},
	rmdir(path){
		let options = false;
		let contents = false;

		if(!this.exists(path)){ return; }

		options = {
			recursive: true,
			directories: true,
			hidden: true,
			files: true
		};
		/*
		contents = self.getList(path, options).reverse();

		contents.forEach(function(item){
			if(item.directory){
				self.fs.rmdirSync(item.path);
			} else {
				self.unlink(item.path);
			}
		});

		self.fs.rmdirSync(path);
		*/
	},
	exists(path){
		try{
			fs.lstatSync(path);
			return true;
		} catch(err){
			return false;
		}
	},
	cat(list){
			let output = '';

			for(let path of list){
					output += this.readFile(path);
			}

			return output;
	},
	getListOutput(opts, output){
		if(opts.detailed){ return output; }

		let arr = [];
		for(let item of output){
			arr.push(item.path);
		}

		return arr;
	},
	fileList(opts){
		opts.directories = false;
		opts.files = true;

		let output = this.getList(opts);

		return this.getListOutput(opts, output);
	},
	dirList(opts){
		opts.directories = true;
		opts.files = false;

		let output = this.getList(opts);

		return this.getListOutput(opts, output);
	},
	getList(opts, output = []){
		let base = fs.realpathSync(opts.path) + '/';
		let list = fs.readdirSync(base);

		for(let item of list){
			let path = base + item;
			let stats = this.getListStats(path, opts);

			if(!stats.filter){
				if(stats.directory){
					if(opts.directories){ output.push(stats); }
					if(opts.recursive){
						opts.path = path;
						output = this.getList(opts, output);
					}
				} else {
					if(opts.files){ output.push(stats); }
				}
			}
		}

		return output;
	},
	getListStats(path, opts = {}){
		let regExp = /^([.]).*$/;
		let stats = fs.lstatSync(path);
		let basename = Path.basename(path);
		let output = {
			basename: basename,
			path: path,
			stats: stats,
			ext: this.getExtension(path),
			mime: this.getMimeType(path),
			directory: stats.isDirectory(),
			symbolicLink: stats.isSymbolicLink(),
			hidden: regExp.test(basename),
			filter: false
		};

		output.filter = this.checkListFilters(output, opts);

		return output;
	},
	getExtension(path){
		return Path.extname(path).replace('.', '');
	},
	getMimeType(path){
		return mime.lookup(path.toLowerCase());
	},
	checkListFilters(out, opts){
		if(out.hidden && !opts.hidden){ return true; }
		if(out.symbolicLink && !opts.symbolicLinks){ return true; }
		if(out.directory){
			if(opts.dirExcludes && opts.dirExcludes.indexOf(out.basename) >= 0){ return true; }
			if(opts.dirIncludes && opts.dirIncludes.indexOf(out.basename) < 0){ return true; }
		} else {
			if(opts.extExcludes && opts.extExcludes.indexOf(out.ext) >= 0){ return true; }
			if(opts.extIncludes && opts.extIncludes.indexOf(out.ext) < 0){ return true; }
		}

		if(opts.regexInclude){
			if(!opts.regexInclude.test(out.path)){ return true; }
		}
		if(opts.regexExclude){
			if(opts.regexExclude.test(out.path)){ return true; }
		}

		return false;
	}
};

module.exports = FileManager;

//FileManager.fileList({path: '../', regexExclude: /.*kendo.*/, recursive: true, extIncludes: ['js'], hidden: false});
