/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Utils = require('./utils');
const Path = require('path');
//const sqlite = require('sqlite3');
const sqlite = require('spatialite');

class SQLite{
	constructor(list){
		this.path = Path.resolve(Path.dirname(__dirname), './data/sqlite') + '/';
		this.connections = this.openDatabases(list);
	}
	openDatabases(list){
		let connections = {};
		list = Utils.isArray(list) ? list : [list];

		for(let db of list){
			connections[db] = new sqlite.Database(this.path + db + '.sqlite');
		}

		return connections;
	}
	series(payload, callback){
		Utils.asyncBatch({module: this, method: 'exec', payload: payload}, callback);
	}
	exec(obj, callback){
		let db = this.connections[obj.db];

		obj.bind = obj.bind || {};

		//db.serialize(function(err){
		db.spatialite(function(err){
			if(err){
				callback(err, null);
				return;
			}

			db.all(obj.sql, obj.bind, function(err, results){
				if(err){
					callback(err, null);
					return;
				}

				callback(null, results);
			});
		});
	}
	bulk(obj, callback){
		let db = this.connections[obj.db];

		db.spatialite(function(err){
			if(err){
				callback(err, null);
				return;
			}

			let stmt = db.prepare(obj.sql);

			for(let binds of obj.binds){
				stmt.run(binds);
			}

			stmt.finalize();

			callback(null);
		});
	}
}

module.exports = SQLite;
