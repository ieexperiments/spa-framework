/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Airports = {
	diagramSearch(req, res, data){
		let str = data.str.toLowerCase();
		let sql = 'select * from arpt_diagram_search where search like (\''+str+'%\') or search like(\'% '+str+'%\') order by rank';

		if(!str.length){
			server.sendJSON(res, []);
			return;
		}

		sqlite.exec({db: 'spatial', sql: sql}, function(err, results){
			let out = [];

			for(let row of results){
				out.push({name: row.arptnm, value: row.faa, group: 'airports'});
			}

			server.sendJSON(res, out);
		}.bind(this));
	},
	diagramGet(req, res, data){
		let args = req.query;
		let name = args.faa + '.pdf';
		let path = pm.paths.data + 'diagrams/' + name;

		res.status(200);
		res.set('Content-Type', 'application/pdf');
		res.set('Content-disposition', 'filename=' + name);
		res.sendFile(path);
	}
};

module.exports = Airports;
