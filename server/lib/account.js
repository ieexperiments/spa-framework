
/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Account = {
	clientData(req, res, next){
		let data = {account: req.session.account};
		let sql = 'select * from uas_schedules where user_id = $1';
		let bind = {$1: req.session.account.id};

		sqlite.exec({db: 'spatial', sql: sql, bind: bind}, function(err, results){
			data.uas = results;
			server.sendJSON(res, data);
		}.bind(this));
	},
	accountLookup(req, res, next){
		let args = req.body;
		let sql = 'select id, first_nm, last_nm from accounts_view ';
		sql += 'where (lower(first_nm) like $1 or lower(last_nm) like $1 or lower(first_nm ||\' \'|| last_nm) like $1) ';
		sql += 'order by last_nm, first_nm';

		let bind = {$1: args.filter.filters[0].value.toLowerCase() + '%'};

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			let data = [];
			results.forEach(function(item){
				data.push({id: item.id, name: item.first_nm + ' ' + item.last_nm});
			});

			server.sendJSON(res, data);
		});
	},
	myProfile(req, res, next){
		let sql = 'select * from accounts where id = $1';
		let bind = {$1: req.session.account.id};

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, results[0]);
		});
	},
	saveMyProfile(req, res, next){
		let data = req.body;
		let sql = 'update accounts set {1} where id = $1';

		let fields = [
			'first_nm=$2',
			'last_nm=$3',
			'title=$4',
			'addr1=$5',
			'addr2=$6',
			'city=$7',
			'state=$8',
			'zip=$9',
			'phone=$10',
			'email=$11'
		];

		let bind = {
			$1: data.acctId,
			$2: data.first_nm,
			$3: data.last_nm,
			$4: data.title,
			$5: data.addr1,
			$6: data.addr2,
			$7: data.city,
			$8: data.state,
			$9: data.zip,
			$10: data.phone,
			$11: data.email
		};

		sql = sql.replace('{1}', fields.join(','));

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, {code: 0});
		});
	},
	emailExists(req, res, next){
		let bind = {$1: req.body.email};
		let sql = 'select count(*) cnt from accounts where email = $1';

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, {cnt: results[0].cnt});
		});

	}
};

module.exports = Account;
