/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Security = {
	authenticate(req, res, next){
		let data = req.body;
		let bind = {$1: data.username};
		let sql = 'select * from accounts where username = $1';

		if(!data.username || !data.password){
			server.sendJSON(res, {code: 1});
			return;
		}

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			if(err){
				server.sendJSON(res, {code: 1});
				return;
			}
			this.validateAccount(req, res, results[0], data.password);
		}.bind(this));
	},
	validateAccount(req, res, account, password){
		if(!account){
			server.sendJSON(res, {code: 1});
			return;
		}

		if(!bcrypt.compareSync(password, account.password)){
			server.sendJSON(res, {code: 1});
			return;
		}

		if(!account.active){
			server.sendJSON(res, {code: 2});
			return;
		}

		if(account.locked){
			server.sendJSON(res, {code: 3});
			return;
		}

		account.fullName = [account.first_nm, account.last_nm].join(' ');
		account.roles = JSON.parse(account.roles);
		delete account.password;
		delete account.active;
		delete account.locked;
		delete account.skills;

		req.session.uuid = account.id;
		req.session.account = account;

		server.sendJSON(res, {code: 0});
	},
	logout(req, res, data){
		req.session.destroy();
		server.sendJSON(res, {code: 0});
	},
	ping(req, res, data){
		server.sendJSON(res, {code: 0});
	}
};

module.exports = Security;
