/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Search = {
	globalSearch(req, res, data){
		let str = data.str.toLowerCase();
		let out = [];
		let order = "CASE WHEN iso = 'US' THEN 1 WHEN iso = 'CA' THEN 2 WHEN iso = 'MX' THEN 3 ELSE 4 END";
		let batch = {
			arpt: {db: 'spatial', sql: this.getArptSql(str, order)},
			city: {db: 'spatial', sql: this.getCitySql(str, order)}
		};

		if(!str.length){
			server.sendJSON(res, []);
			return;
		}

		sqlite.series(batch, function(err, obj){
			for(let row of obj.city){
				out.push({name: row.city_nm, value: [row.lon, row.lat], group: 'Cities'});
			}
			for(let row of obj.arpt){
				out.push({name: row.arpt_nm, value: [row.lon, row.lat], group: 'Airports'});
			}

			server.sendJSON(res, out);
		}.bind(this));
	},
	getCitySql(str, order){
		let sql = 'select * from city_view where search like (\'' + str + '%\') order by ' + order + ', population, city_nm limit 10';

		return sql;
	},
	getArptSql(str, order){
		let sql = 'select * from arpt_search_view where search like (\''+str+'%\') or search like(\'% '+str+'%\') order by ' + order + ', search limit 10';

		return sql;
	}
};

module.exports = Search;
