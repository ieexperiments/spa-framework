/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Filesystem = {
	add(req, res, data){
		let sql = 'insert into filesystem values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)';
		let bind = {
			$1: data.uuid,
			$2: data.fs,
			$3: data.parent,
			$4: data.size || 0,
			$5: data.modified || new Date().getTime(),
			$6: data.text,
			$7: data.mimetype,
			$8: data.ext,
			$9: req.session.account.id,
			$10: new Date().getTime(),
			$11: data.folder
		};

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, {code: 1});
		}.bind(this));
	},
	getTree(req, res, data){
		let sql = 'select * from fs_view where fs = $1';
		let bind = {$1: data.fs};

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, results);
		}.bind(this));
	},
	rename(req, res, data){
		let sql = 'update filesystem set text = $1 where uuid = $2';
		let bind = {$1: data.text, $2: data.uuid};

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, {code: 0});
		}.bind(this));
	},
	move(req, res, data){
		let sql = 'update filesystem set parent = $1 where uuid = $2';
		let bind = {$1: data.parent, $2: data.uuid};

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, {code: 0});
		}.bind(this));
	},
	preview(req, res, data){
		let args = req.query;
		let sql = 'select * from filesystem where uuid = $1';
		let bind = {$1: args.uuid};
		let path = pm.paths.upload + args.uuid;

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			let file = results[0];

			res.status(200);
			res.set('Content-Type', file.mimetype);
			res.set('Content-Length', file.size);
			res.set('Content-disposition', 'filename=' + file.text);
			res.sendFile(path);
		}.bind(this));
	},
	download(req, res, data){
		let args = req.query;
		let sql = 'select * from filesystem where uuid = $1';
		let bind = {$1: args.uuid};
		let path = pm.paths.upload + args.uuid;

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			let file = results[0];

			res.status(200);
			res.set('Content-Type', file.mimetype);
			res.set('Content-Length', file.size);
			res.set('Content-disposition', 'attachment; filename=' + file.text);
			res.sendFile(path);
		}.bind(this));
	}
};

module.exports = Filesystem;
