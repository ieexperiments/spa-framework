/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Administration = {
	adminGrid(req, res, next){
		let args = req.body;
		let data = {};
		let order = [];
		let cnt = 'select count(*) total from accounts_view where {1}';
		let sql = 'select * from accounts_view where {1} order by {2} LIMIT $3 OFFSET $4';

		if(args.sort && !args.sort.length){ delete args.sort; }

		let sort = args.sort || [{dir: 'asc', field: 'last_nm'}, {dir: 'asc', field: 'first_nm'}];

		for(let item of sort){
			order.push(item.field + ' ' + item.dir);
		}

		let bind1 = {
			$1: args.filters.dept || '%',
			$2: args.filters.user ? args.filters.user.id : '%'
		};

		let bind2 = {
			$1: args.filters.dept || '%',
			$2: args.filters.user ? args.filters.user.id : '%',
			$3: args.take,
			$4: args.skip
		};

		let where = 'department like $1 and id like $2';

		cnt = cnt.replace('{1}', where);
		sql = sql.replace('{1}', where);
		sql = sql.replace('{2}', order.join(','));

		let series = {
			count: {db: 'app', sql: cnt, bind: bind1},
			accounts: {db: 'app', sql: sql, bind: bind2}
		};

		sqlite.series(series, function(err, results){
			data.total = results.count[0].total;
			data.results = results.accounts;

			data.results.forEach(function(item){
				item.roles = JSON.parse(item.roles);
				item.skills = JSON.parse(item.skills);
				delete item.password;
			});

			server.sendJSON(res, data);
		}.bind(this));
	 },
	 adminData(req, res, next){
		 let series = {
			 roles: {db: 'app', sql: 'select * from roles order by name'},
			 depts: {db: 'app', sql: 'select a.id value, a.name name from departments a order by name'}
		 };

		 sqlite.series(series, function(err, results){
			 server.sendJSON.sendJSON(res, results);
		 }.bind(this));
	 },
	 accountMerge(req, res, next){
		let data = req.body;
		let values = [];
		let sql = 'insert or replace into accounts ({1}) values ({2})';

		let fields = [
			'id',
			'username',
			'first_nm',
			'last_nm',
			'admin',
			'active',
			'locked',
			'expires',
			'department',
			'title',
			'addr1',
			'addr2',
			'city',
			'state',
			'zip',
			'phone',
			'email',
			'roles',
			'skills'
		];

		let bind = {
			$1: data.acctId,
			$2: data.username,
			$3: data.first_nm,
			$4: data.last_nm,
			$5: data.roles.indexOf('admin') >= 0 ? 1 : 0,
			$6: data.active ? 1 : 0,
			$7: data.locked ? 1 : 0,
			$8: data.expires,
			$9: data.department,
			$10: data.title,
			$11: data.addr1,
			$12: data.addr2,
			$13: data.city,
			$14: data.state,
			$15: data.zip,
			$16: data.phone,
			$17: data.email,
			$18: JSON.stringify(data.roles),
			$19: JSON.stringify(data.skills),
		};

		if(data.password){
			fields.push('password');
			bind.$20 = bcrypt.hashSync(data.password, 10);
		}

		Object.keys(bind).forEach(function(key){
			values.push(key);
		});

		sql = sql.replace('{1}', fields.join(','));
		sql = sql.replace('{2}', values.join(','));

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			console.log(err);
			server.sendJSON(res, {code: 0});
		});
	 },
	 accountsExport(req, res, next){
		let rows = [];
		 let sql = 'select * from accounts_view order by last_nm, first_nm';

		 sqlite.exec({db: 'app', sql: sql}, function(err, results){
			 results.forEach(function(item){
				 item.phone = this.utils.formatPhoneNumber(item.phone);
				 item.active = item.active ? 'true' : 'false';
				 item.admin = item.admin ? 'true' : 'false';
				 rows.push([item.username, item.first_nm, item.last_nm, item.dept_nm, item.title, item.email, item.phone, item.active, item.admin]);
			 });

			 this.spreadsheet.create({
				 res: res,
				 worksheets: [{
					 title: 'Accounts',
					 header: ['Username', 'First Name', 'Last Name', 'Department', 'Title', 'Email', 'Phone', 'Active', 'Admin'],
					 rows: rows
				 }]
			 });
		 });
	 },
	 getPasswordPolicy(req, res, next){
		 let bind = {$1: 'policy'};
		 let sql = 'select * from password_policy where id = $1';

		 sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			 server.sendJSON(res, results[0]);
		 }.bind(this));
	 },
	 setPasswordPolicy(req, res, next){
		let data = req.body;
		let sql = 'insert or replace into password_policy values ($1,$2,$3,$4,$5,$6,$7,$8)';

		 let bind = {
			 $1: 'policy',
			 $2: data.min_length,
			 $3: data.min_upper,
			 $4: data.min_lower,
			 $5: data.min_numeric,
			 $6: data.min_symbol,
			 $7: data.min_age,
			 $8: data.max_age
		 };

		sqlite.exec({db: 'app', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, {code: 0});
		}.bind(this));
	}
};

module.exports = Administration;
