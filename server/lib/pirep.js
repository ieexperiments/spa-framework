/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const PIREP = {
	submit(req, res, data){
		data.status = 'pending';
		let sql = 'insert into pirep values ($1,$2,$3,$4,$5,$6,$7,$8)';
		let bind = {
			$1: data.uuid,
			$2: req.session.account.id,
			$3: new Date(data.reported).getTime(),
			$4: data.type,
			$5: data.severity,
			$6: data.alt,
			$7: data.lat,
			$8: data.lon
		};

		sqlite.exec({db: 'spatial', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, {code: 0});
		}.bind(this));

		this.sendMessage(req.session.account.id, {type: 'received', id: data.uuid});
	},
	getAllPireps(req, res, data){
		let sql = 'select * from pirep';

		sqlite.exec({db: 'spatial', sql: sql}, function(err, results){
			server.sendJSON(res, results);
		}.bind(this));
	},
	sendMessage(uid, payload){
		payload.route = 'pirep';

		pm.broadcast({
			route: 'ws',
			msg: {
				users: uid,
				payload: payload
			}
		});
	}
};

module.exports = PIREP;
