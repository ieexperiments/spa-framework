/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const UAS = {
	submit(req, res, data){
		data.status = 'pending';
		let sql = 'insert into uas_schedules values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24)';
		let bind = {
			$1: data.uuid,
			$2: req.session.account.id,
			$3: 'pending',
			$4: new Date().getTime(),
			$5: null,
			$6: data.cert,
			$7: data.user,
			$8: data.phone,
			$9: new Date(data.date1[0]).getTime(),
			$10: new Date(data.date2[0]).getTime(),
			$11: new Date(data.date3[0]).getTime(),
			$12: new Date(data.date1[1]).getTime(),
			$13: new Date(data.date2[1]).getTime(),
			$14: new Date(data.date3[1]).getTime(),
			$15: data.freq,
			$16: data.days.join(','),
			$17: data.alt[0],
			$18: data.alt[1],
			$19: data.altType,
			$20: data.gtype,
			$21: data.radius,
			$22: data.units,
			$23: JSON.stringify(data.geom),
			$24: ''
		};

		sqlite.exec({db: 'spatial', sql: sql, bind: bind}, function(err, results){
			console.log(err);
			this.getUserSchedules(req, res, data);
		}.bind(this));

		setTimeout(function(){
			this.validate(data);
		}.bind(this), 1);
	},
	update(req, res, data){
		let fields = ['status','user','phone','uas_cert','start1','start2','start3','end1','end2','end3','frequency','days','alt1','alt2','alt_type'];
		let sql = 'update uas_schedules set {a} where id = $1';
		let arr = [];
		let key = false;
		let offset = 1;
		let bind = {$1: data.uuid};

		data.start1 = new Date(data.date1[0]).getTime();
		data.start2 = new Date(data.date2[0]).getTime();
		data.start3 = new Date(data.date3[0]).getTime();
		data.end1 = new Date(data.date1[1]).getTime();
		data.end2 = new Date(data.date2[1]).getTime();
		data.end3 = new Date(data.date3[1]).getTime();
		data.days = data.days.join(',');
		data.alt1 = data.alt[0];
		data.alt2 = data.alt[1];

		for(let [idx, field] of fields.entries()){
			offset++;
			key = '$' + offset;
			arr.push(field + '=' + key);
			bind[key] = data[field];
		}

		if(data.isAdmin){
			bind.$2 = data.status;
			offset++;
			key = '$' + offset;
			arr.push('action_dt' + '=' + key);
			bind[key] = new Date().getTime();

			offset++;
			key = '$' + offset;
			arr.push('comments' + '=' + key);
			bind[key] = data.comments;
		} else {
			bind.$2 = 'pending';
		}

		sql = sql.replace('{a}', arr.join(','));

		pm.broadcast({
			route: 'ws',
			msg: {
				users: data.user_id,
				payload: {
					route: 'uas',
					type: 'update',
					status: data.status,
					id: data.uuid
				}
			}
		});

		sqlite.exec({db: 'spatial', sql: sql, bind: bind}, function(err, results){
			console.log('err', err);
			this.getUserSchedules(req, res, data);
		}.bind(this));
	},
	getUserSchedules(req, res, data){
		let sql = 'select * from uas_schedules where user_id = $1';
		let bind = {$1: req.session.account.id};

		sqlite.exec({db: 'spatial', sql: sql, bind: bind}, function(err, results){
			server.sendJSON(res, results);
		}.bind(this));
	},
	uasAllGrid(req, res, args){
		this.uasGrid(req, res, args, true);
	},
	uasUserGrid(req, res, args){
		this.uasGrid(req, res, args, false);
	},
	uasGrid(req, res, args, admin){
		let data = {};
		let order = [];
		let cnt = 'select count(*) total from uas_schedules where {1}';
		let sql = 'select * from uas_schedules where {1} order by {2} LIMIT $2 OFFSET $3';

		if(args.sort && !args.sort.length){ delete args.sort; }

		let sort = args.sort || [{dir: 'asc', field: 'load_dt'}];

		for(let item of sort){
			order.push(item.field + ' ' + item.dir);
		}

		let bind1 = {
			$1: admin ? '%': req.session.account.id
		};

		let bind2 = {
			$1: admin ? '%': req.session.account.id,
			$2: args.take,
			$3: args.skip
		};

		let where;

		if(admin){
			where = 'user_id like $1';
		} else {
			where = 'user_id = $1';
		}

		cnt = cnt.replace('{1}', where);
		sql = sql.replace('{1}', where);
		sql = sql.replace('{2}', order.join(','));

		let series = {
			count: {db: 'spatial', sql: cnt, bind: bind1},
			schdules: {db: 'spatial', sql: sql, bind: bind2}
		};

		sqlite.series(series, function(err, results){
			data.total = results.count[0].total;
			data.results = results.schdules;

			server.sendJSON(res, data);
		}.bind(this));
	 },
	checkIntersect(table, fields, geom, bbox){
		let sql = 'SELECT ' + fields + ' FROM ' + table + ' a WHERE INTERSECTS(a.geometry, GeomFromGeoJSON($1)) > 0 AND a.ROWID IN ';
		let idx = '(SELECT ROWID FROM SpatialIndex WHERE f_table_name=\'' + table + '\' AND search_frame=GeomFromGeoJSON($1))';
		let obj = {
			db: 'spatial',
			sql: sql + idx,
			bind: {$1: geom}
		};

		return obj;
	},
	setStatus(id, status){
		let sql = 'update uas_schedules set status = $1 where id = $2';
		let bind = {$1: status, $2: id};

		sqlite.exec({db: 'spatial', sql: sql, bind: bind}, function(err, results){
			return;
		}.bind(this));
	},
	validate(data){
		console.log(data.geom);
		let geom = JSON.stringify(data.geom.geometry);
		let batch = {
			classb: this.checkIntersect('class_b', 'a.name', geom, data.bbox),
			classc: this.checkIntersect('class_c', 'a.name', geom, data.bbox),
			classd: this.checkIntersect('class_d', 'a.name', geom, data.bbox),
			obstructions: this.checkIntersect('obstructions', 'a.id', geom, data.bbox)
		};

		sqlite.series(batch, function(err, obj){
			this.processConflicts(data, obj);
		}.bind(this));
	},
	processConflicts(data, obj){
		let count = obj.classb.length + obj.classc.length + obj.classd.length;
		let status = count ? 'pending' : 'approved';

		this.setStatus(data.uuid, status);
		this.sendMessage(data.user_id, {type: 'update', status: status, id: data.uuid, warnings: obj});
	},
	sendMessage(uid, payload){
		payload.route = 'uas';

		console.log('send msg', payload);

		pm.broadcast({
			route: 'ws',
			msg: {
				users: uid,
				payload: payload
			}
		});
	},
	calendar(req, res, data){
		let sql = 'select * from uas_schedules';
		let scheds = [];
		let id = 0;

		sqlite.exec({db: 'spatial', sql: sql}, function(err, results){
			for(let item of results){
				let title = 'FAA-UAS-' + item.id.substr(0, 8);

				if(item.frequency === 'single'){
					let start = new Date(item.start1);
					let end = new Date(item.end1);
					scheds.push({id: id++, title: title, start: start, end: end});
				} else {
					let days = item.days.split(',');
					let date1 = new Date(item.start2);
					let date2 = new Date(item.end2);
					let time1 = new Date(item.start3);
					let time2 = new Date(item.end3);
					let dates = this.getRecuringDates(date1, date2, time1, time2, days);

					for(let obj of dates){
						scheds.push({id: id++, title: title, start: obj.start, end: obj.end});
					}
				}
			}

			server.sendJSON(res, scheds);
		}.bind(this));
	},
	getRecuringDates(date1, date2, time1, time2, days){
		let dt1 = moment(date1);
		let dt2 = moment(date2);
		let tm1 = moment(time1);
		let tm2 = moment(time2);
		let hh1 = tm1.hour();
		let hh2 = tm2.hour();
		let mm1 = tm1.minute();
		let mm2 = tm2.minute();
		let curr = dt1.clone();
		let dates = [];
		let index = {
			sun: days[0] === 'true',
			mon: days[1] === 'true',
			tue: days[2] === 'true',
			wed: days[3] === 'true',
			thu: days[4] === 'true',
			fri: days[5] === 'true',
			sat: days[6] === 'true',
		};

		while(curr.isSameOrBefore(dt2)){
			let key = curr.format('ddd').toLowerCase();
			if(index[key]){
				let start = curr.clone();
				let end = curr.clone();
				start.hour(hh1).minute(mm1).second(0);
				end.hour(hh2).minute(mm2).second(0);
				dates.push({start: start, end: end});
			}
			curr.add(1, 'day');
		}

		return dates;
	}
};

module.exports = UAS;
