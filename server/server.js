/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const processManager = require('../shared/process-manager');
const Compiler = require('../deploy/compile');

class Server{
	constructor(){
		global.pm = new processManager({
			main: this,
			appServer: true,
			cluster: true,
			//threads: 1,
			modules: {
				security: './lib/security',
				account: './lib/account',
				admin: './lib/administration',
				search: './lib/search',
				airports: './lib/airports',
				filesystem: './lib/filesystem',
				uas: './lib/uas',
				sua: './lib/sua',
				pirep: './lib/pirep',
				Websocket: './lib/websocket',
				SQLite: 'shared/sqlite',
				bcrypt: 'bcrypt',
			},
			onMasterReady: function(){ this.masterReady(); }.bind(this),
			onWorkerReady: function(pm){ this.workerReady(pm); }.bind(this)
		});
	}
	masterReady(){
		if(conf.appServer.mode === 'development'){
			let compiler = new Compiler();
		}
	}
	workerReady(pm){
		global.pm = pm;
		global.redisClient = new RedisClient();
		global.sqlite = new SQLite(['app', 'spatial']);
		global.server = new AppServer(conf.appServer);

		process.on('message', function(msg){
			this.routeMessage(msg);
		}.bind(this));
	}
	routeMessage(msg){
		switch(msg.route){
			case 'ws':
				server.broadcast(msg.msg);
			break;
		}
	}
}

new Server();
