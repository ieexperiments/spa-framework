/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
global.Utils = require('../shared/utils');
global.fm = require('../shared/file-manager');
const config = require('./modules/config');
const Exporter = require('./modules/export');
const Path = require('path');
const SQLite = require('../shared/sqlite');
const wget = require('wget-improved');
const async = require('async');

class MapStyles{
	constructor(){
		this.glyphs = {};
		this.layerMap = {};
		this.setPaths();
		this.openDatabase();
	}
	openDatabase(){
		let batch = [];
		let regExp = /\{1\}/g;
		let trunc1 = 'delete from backup_{1}';
		let trunc2 = 'delete from {1}';
		let insert = 'insert into backup_{1} select * from {1}';
		let tables = ['sources', 'source_layers', 'styles', 'style_layers'];

		this.db = 'map';
		this.sqlite = new SQLite(this.db);

		for(let table of tables){
			batch.push({db: this.db, sql: trunc1.replace(regExp, table)});
			batch.push({db: this.db, sql: insert.replace(regExp, table)});
			batch.push({db: this.db, sql: trunc2.replace(regExp, table)});
		}

		this.sqlite.series(batch, function(err, results){
			this.getStyles();
		}.bind(this));
	}
	getStyles(){
		let path = 'https://api.mapbox.com/styles/v1/mapbox/%1%' + config.mapbox.token;
		let obj = false;
		let payload = [];

		for(let [key, style] of Object.entries(config.mapbox.styles)){
			obj = {
				orig: style,
				name: key,
				url: path.replace('%1%', style),
				dest: './temp/styles/' + key + '/style.json'
			};

			fm.mkdir('./temp/styles/' + key);
			fm.mkdir('./temp/sources/' + key);
			fm.mkdir(this.paths.sprites + key);

			payload.push(obj);
		}

		Utils.asyncBatch({module: this, method: 'download', payload: payload}, function(err, styles){
			console.log('stage 1:', err || 'success');
			this.parseStyles(styles);
		}.bind(this));
	}
	parseStyles(styles){
		let batch = [];

		for(let style of styles){
			this.parseHeader(batch, style);
			this.getSource(batch, style);
			this.getSprite(batch, style);

			for(let [idx, layer] of style.json.layers.entries()){
				this.parseLayer(batch, style, layer, idx);
			}
		}

		this.getGlyphs(batch);

		//console.log(JSON.stringify(this.layerMap));

		async.series(batch, function(err, styles){
			console.log('stage 2:', err || 'success');
			this.parseSources();
		}.bind(this));
	}
	parseHeader(batch, style){
		let obj = {
			db: this.db,
			sql: 'INSERT INTO styles VALUES ($1,$2,$3,$4,$5,$6,$7,$8)',
			bind: {
				$1: style.name,
				$2: config.version,
				$3: '[0, 0]',
				$4: 4,
				$5: 0,
				$6: 0,
				$7: style.json.transition || '{"duration": 300, "delay": 0}',
				$8: style.json.metadata ? JSON.stringify(style.json.metadata) : ''
			}
		};

		Utils.asyncAppend(batch, this.sqlite, 'exec', obj);
	}
	getSource(batch, style){
		let url = false;
		let obj = false;
		let dest = './temp/sources/' + style.name + '/source.json';

		for(let [key, source] of Object.entries(style.json.sources)){
			if(source.url.match(/^mapbox:.*/)){
				url = source.url.replace('mapbox://', 'https://api.mapbox.com/v4/') + '.json' + config.mapbox.token;
			} else {
				url = source.url;
			}

			Utils.asyncAppend(batch, this, 'download', {url: url, dest: dest});
		}
	}
	getSprite(async, style){
		let extensions = ['.json', '.png', '@2x.json', '@2x.png'];
		let dest = this.paths.sprites + style.name + '/';
		let obj = false;
		let url = false;

		if(!style.json.sprite){ return; }

		url = style.json.sprite.replace('mapbox://sprites', 'https://api.mapbox.com/styles/v1') + '/sprite{1}' + config.mapbox.token;

		for(let ext of extensions){
			obj = {
				url:  url.replace('{1}', ext),
				dest: dest + 'sprite' + ext
			};

			Utils.asyncAppend(async, this, 'download', obj);
		}
	}
	parseLayer(batch, style, layer, idx){
		this.parseGlyphs(layer);

		layer.id = layer.id.replace(/_/g, '-').replace(/ /g, '-').toLowerCase();
		layer.id = config.layerMap[layer.id] || layer.id;

		if(layer.ref){
			layer.ref = layer.ref.replace(/_/g, '-').replace(/ /g, '-').toLowerCase();
			layer.ref = config.layerMap[layer.ref] || layer.ref;
		}

		if(!this.layerMap[layer.id]){ this.layerMap[layer.id] = []; }
		this.layerMap[layer.id].push(style.name);

		let obj = {
			db: this.db,
			sql: 'INSERT INTO style_layers VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)',
			bind: {
				$1: style.name,
				$2: layer.id,
				$3: idx,
				$4: layer.type,
				$5: layer.interactive ? 'true' : 'false',
				$6: layer.ref,
				$7: layer.minzoom || '',
				$8: layer.maxzoom || '',
				$9: 'osm',
				$10: layer['source-layer'],
				$11: JSON.stringify(layer.paint),
				$12: JSON.stringify(layer.layout),
				$13: JSON.stringify(layer.filter),
				$14: JSON.stringify(layer.metadata)
			}
		};

		Utils.asyncAppend(batch, this.sqlite, 'exec', obj);
	}
	parseGlyphs(layer){
		if(!layer.layout || !layer.layout['text-font']){ return; }

		let obj = layer.layout['text-font'];
		let font;

		if(obj.stops){
			for(let arr of obj.stops){
				font = arr[1].join('').replace(/ /g, '');

				if(!this.glyphs[font]){
					this.glyphs[font] = arr[1].join(',').replace(/ /g, '%20');
				}

				arr[1] = [font];
			}
		} else {
			font = obj.join('').replace(/ /g, '');

			if(!this.glyphs[font]){
				this.glyphs[font] = obj.join(',').replace(/ /g, '%20');
			}

			obj = [font];
		}

		layer.layout['text-font'] = obj;
	}
	getGlyphs(batch){
		let obj;
		let dir;
		let low;
		let high;
		let url = 'https://api.mapbox.com/fonts/v1/mapbox/{1}/{2}-{3}.pbf' + config.mapbox.token;

		for(let [key, glyph] of Object.entries(this.glyphs)){
			dir = this.paths.glyphs + key + '/';

			fm.mkdir(dir);

			for(let idx = 0; idx < 256; idx++){
				low = idx * 256;
				high = low + 255;

				obj = {
					url: url.replace('{1}', glyph).replace('{2}', low).replace('{3}', high),
					dest: dir + low + '-' + high + '.pbf'
				};

				if(!fm.exists(obj.dest)){
					Utils.asyncAppend(batch, this, 'download', obj);
				}
			}
		}
	}
	parseSources(){
		let batch = [];

		for(let [key, style] of Object.entries(config.mapbox.styles)){
			let source = {
				orig: style,
				name: key,
				json: fm.readJSON('./temp/sources/' + key + '/source.json')
			};

			this.parseSourceHeader(batch, source);

			for(let [idx, layer] of source.json.vector_layers.entries()){
				this.parseSourceLayer(batch, source, layer, idx);
			}
		}

		async.series(batch, function(err, styles){
			console.log('stage 3:', err || 'success');
			fm.rmdir('./temp');
			new Exporter();
		});
	}
	parseSourceHeader(batch, source){
		let obj = {
			db: this.db,
			sql: 'INSERT INTO sources VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)',
			bind: {
				$1: source.name,
				$2: source.json.format,
				$3: source.json.scheme,
				$4: '2.1.0',
				$5: '[0,0,0]',
				$6: '[-180,-85.0511,180,85.0511]',
				$7: source.json.minzoom,
				$8: source.json.maxzoom,
				$9: source.json.sprite
			}
		};

		Utils.asyncAppend(batch, this.sqlite, 'exec', obj);
	}
	parseSourceLayer(batch, source, layer, idx){
		let obj = {
			db: this.db,
			sql: 'INSERT INTO source_layers VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)',
			bind: {
				$1: source.name,
				$2: layer.id,
				$3: idx,
				$4: layer.minzoom || 0,
				$5: layer.maxzoom || 22,
				$6: 'osm',
				$7: 'osm',
				$8: layer.description || '',
				$9: layer.fields ? JSON.stringify(layer.fields) : '{}'
			}
		};

		Utils.asyncAppend(batch, this.sqlite, 'exec', obj);
	}
	download(obj, callback){
		let download = wget.download(obj.url, obj.dest);

		download.on('error', function(err){
			callback(err, obj);
		});

		download.on('end', function(output){
			if(obj.name){ obj.json = fm.readJSON(obj.dest); }

			callback(null, obj);
		});
	}
	setPaths(){
		const basePath = Path.dirname(__dirname);

		let paths = {
			base: basePath + '/',
			current: Path.dirname(process.mainModule.filename) + '/',
			data: Path.resolve(basePath, './data') + '/',
			glyphs: Path.resolve(basePath, './data/map/glyphs') + '/',
			sources: Path.resolve(basePath, './data/map/sources') + '/',
			sprites: Path.resolve(basePath, './data/map/sprites') + '/',
			styles: Path.resolve(basePath, './data/map/styles') + '/'
		};

		fm.mkdir('./temp');
		fm.mkdir('./temp/styles');
		fm.mkdir('./temp/sources');

		this.paths = paths;
	}
}

global.main = new MapStyles();
