/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Export{
	constructor(){
		this.sources = {};
		this.styles = {};
		this.getData();
	}
	build(){
		this.buildSources();
		this.buildStyles();

		for(let [key, value] of Object.entries(this.sources)){
			let path = main.paths.sources + key + '.json';
			fm.writeJSON(path, value, 4);
		}

		for(let [key, value] of Object.entries(this.styles)){
			let path = main.paths.styles + key + '.json';
			fm.writeJSON(path, value, 4);
		}
	}
	buildSources(){
		let sources = this.tables.sources.concat(this.tables.customSources);
		let layers = this.tables.sourceLayers.concat(this.tables.customSourceLayers);

		for(let source of sources){
			let mbtile = source.id === 'custom' ? 'custom' : 'osm';

			this.sources[source.id] = {
				name: source.id,
				center: JSON.parse(source.center),
				bounds: JSON.parse(source.bounds),
				tilejson: source.tilejson,
				scheme: source.scheme,
				format: source.format,
				minzoom: source.minzoom,
				maxzoom: source.maxzoom > 14 ? 14 : source.maxzoom,
				tiles: ['{host}/map/pbf/' + mbtile + '/{z}/{x}/{y}.vector.pbf'],
				vector_layers: []
			};
		}

		for(let layer of layers){
			let obj = {
				id: layer.id,
				minzoom: layer.minzoom,
				maxzoom: layer.maxzoom,
				source: layer.source,
				'source_name': layer.sourcename
			};

			//if(layer.description){ obj.description = layer.description; }
			//if(layer.fields){ obj.fields = JSON.parse(layer.fields); }

			this.sources[layer.source_id].vector_layers.push(obj);
		}
	}
	buildStyles(){
		for(let table of this.tables.styles){
			let obj = {
				name: table.name,
				version: table.version,
				center: JSON.parse(table.center),
				zoom: table.zoom,
				bearing: table.bearing,
				pitch: table.pitch,
				transitions: JSON.parse(table.transitions),
				//metadata: JSON.parse(table.metadata),
				sprite: '{host}/map/sprites/' + table.name + '/sprite',
				glyphs: '{host}/map/glyphs/{fontstack}/{range}.pbf',
				layers: [],
				sources: {
					osm: {type: 'vector', url: 'map/sources/' + table.name + '.json'},
					custom: {type: 'vector', url: 'map/sources/custom.json'},
					weather: {type: 'vector', url: 'map/sources/weather.json'},
					vfr: {type: 'raster', url: 'map/sources/charts-vfr.json', tileSize: 256},
					ifrlo: {type: 'raster', url: 'map/sources/charts-ifrlo.json', tileSize: 256},
					ifrhi: {type: 'raster', url: 'map/sources/charts-ifrhi.json', tileSize: 256},
					satellite: {type: 'raster', url: 'map/sources/satellite.json', tileSize: 256}
				}
			};

			this.styles[table.name] = obj;
		}

		for(let layer of this.tables.styleLayers){
			this.processLayer(layer.style, layer);
		}

		//this.rasterLayers(layer);

		for(let style of this.tables.styles){
			this.rasterLayers(style);
			this.weatherLayers(style);
			for(let layer of this.tables.customStyleLayers){
				this.processLayer(style.name, layer);
			}
		}

		console.log('stage 4: success');
	}
	rasterLayers(style){
		let layers = this.styles[style.name].layers;

		layers.push({id: 'vfr', type: 'raster', source: 'vfr', layout: {visibility: 'none'}});
		layers.push({id: 'ifrlo', type: 'raster', source: 'ifrlo', layout: {visibility: 'none'}});
		layers.push({id: 'ifrhi', type: 'raster', source: 'ifrhi', layout: {visibility: 'none'}});
		layers.push({id: 'satellite', type: 'raster', source: 'satellite', layout: {visibility: 'none'}});
	}
	weatherLayers(style){
		let layers = this.styles[style.name].layers;
		let colors = ['#00ECEC','#01A0F6','#0000F6','#00FF00','#00C800','#009000','#FFFF00','#E7C000','#FF9000','#FF0000','#D60000','#C00000','#FF00FF','#9955C9','#FFFFFF'];

		for(let idx = 1; idx <= 15; idx++){
			let id = 'nexrad' + Utils.lpad(idx, '0', 2);
			let src = 'level_' + Utils.lpad(idx, '0', 2);

			layers.push({
				id: id,
				type: 'fill',
				source: 'weather',
				'source-layer': src,
				layout: {visibility: 'none'},
				paint: {'fill-color': colors[idx]}
			});
		}

	}
	processLayer(style, layer){
		let layers = this.styles[style].layers;
		let regexp1 = /^place-city-(md|lg)-n$/;
		let regexp2 = /^place-city-(md|lg)-s$/;

		let obj = {
			id: layer.id.replace(' ', '-'),
			type: layer.type,
			//interactive: layer.interactive === 'true' ? true : false,
			ref: layer.ref ? layer.ref.replace(' ', '-') : layer.ref,
			minzoom: layer.minzoom,
			maxzoom: layer.maxzoom,
			source: layer.source,
			'source-layer': layer.layer,
			paint: JSON.parse(layer.paint),
			layout: JSON.parse(layer.layout),
			filter: JSON.parse(layer.filters),
			//metadata: JSON.parse(layer.metadata)
		};

		for(let [key, value] of Object.entries(obj)){
			if(Utils.isNullOrUndefined(value)){
				delete obj[key];
			}
		}

		if(obj.ref){ delete obj.source; }

		if(obj.id.match(regexp1)){
			obj.id = obj.id.match(/(^.*)-n$/)[1];

			let filter = [];

			for(let item of obj.filter){
				if(item[1] !== 'ldir'){
					filter.push(item);
				}
			}

			obj.filter = filter;
		}

		if(!obj.id.match(regexp2)){
			layers.push(obj);
		}
	}
	getData(){
		let batch = {};
		let order;
		let tables = [
			{key: 'sources', table: 'sources'},
			{key: 'sourceLayers', table: 'source_layers', order: 'source_id, layer_order'},
			{key: 'styles', table: 'styles'},
			{key: 'styleLayers', table: 'style_layers', order: 'style, layer_order'},
			{key: 'customSources', table: 'custom_sources'},
			{key: 'customSourceLayers', table: 'custom_source_layers', order: 'source_id, layer_order'},
			{key: 'customStyleLayers', table: 'custom_style_layers', order: 'style, layer_order'}
		];

		for(let obj of tables){
			order = obj.order ? (' order by ' + obj.order) : '';
			batch[obj.key] = {db: main.db, sql: 'select * from ' + obj.table + order};
		}

		main.sqlite.series(batch, function(err, results){
			if(err){ console.log(err); }
			this.tables = results;
			this.build();
		}.bind(this));
	}
}

module.exports = Export;
