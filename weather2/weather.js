/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const request = require('request');
const Utils = require('../shared/Utils');
const exec = require('child_process').exec;
const md5 = require('md5');
const fs = require('fs');

class Weather{
	constructor(){
		this.hash = false;
		this.setMapLevels();
		this.download();

		setInterval(function(){
			this.download();
		}.bind(this), 60000);
	}
	download(){
		let url = 'http://mesonet.agron.iastate.edu/data/gis/images/4326/USCOMP/n0r_0.tif.Z';
		let path = './data/n0r.tif.Z';

		let req = request(url);
		req.pipe(fs.createWriteStream(path));
		req.on('end', function(response, body){
			this.checkMD5(path);
		}.bind(this));
	}
	checkMD5(path){
		fs.readFile(path, function(err, buf){
			let hash = md5(buf);

			console.log(hash, this.hash);

			if(hash === this.hash){
				console.log('no change');
				return;
			}

			this.hash = hash;
			this.unzip(path);
		}.bind(this));
	}
	unzip(path){
		let cmd = 'gzip -d -f ' + path;

		exec(cmd, function(err, stdout, stderr){
			this.translate();
		}.bind(this));
	}
	translate(){
		let batch = [];
		let template = 'gdal_translate -a_nodata 0 ./vrt/n0r_{idx}.vrt ./data/radar_{idx}.tif';
		//let template = 'gdal_translate -a_nodata 0 ./vrt/n0r_{idx}.vrt ./data/base_{idx}.tif';

		for(let idx = 1; idx <= 15; idx++){
			let cmd = template.replace(/\{idx\}/g, Utils.lpad(idx, '0', 2));
			batch.push(cmd);
		}

		Utils.asyncBatch({module: this, method: 'execute', async: 'parallel', payload: batch}, function(err, result){
			this.polygonize();
			//this.densify();
		}.bind(this));
	}
	densify(){
		let batch = [];
		let template = 'gdalwarp -ts 12000 0 -r cubic ./data/base{idx}.tif ./data/radar_{idx}.tif';

		for(let idx = 1; idx <= 15; idx++){
			let cmd = template.replace(/\{idx\}/g, Utils.lpad(idx, '0', 2));
			batch.push(cmd);
		}

		Utils.asyncBatch({module: this, method: 'execute', async: 'parallel', payload: batch}, function(err, result){
			this.polygonize();
		}.bind(this));
	}
	polygonize(){
		let batch = [];
		let template = 'gdal_polygonize.py ./data/radar_{idx}.tif -f "GeoJSON" ./output/level_{idx}.json';

		for(let idx = 1; idx <= 15; idx++){
			let cmd = template.replace(/\{idx\}/g, Utils.lpad(idx, '0', 2));
			batch.push(cmd);
		}

		Utils.asyncBatch({module: this, method: 'execute', async: 'parallel', payload: batch}, function(err, result){
			this.mbtile();
		}.bind(this));
	}
	mbtile(){
		let cmd = 'tippecanoe -f -r 0 -z 8 -o ./output/weather.mbtiles ./output/*.json';

		exec(cmd, function(err, stdout, stderr){
			this.running = false;
		}.bind(this));
	}
	execute(cmd, callback){
		exec(cmd, function(err, stdout, stderr){
			callback();
		}.bind(this));
	}
	setMapLevels(){
		this.levels = [
			{level: 1, color: '#00ECEC', dn: 7},
			{level: 1, color: '#01A0F6', dn: 8},
			{level: 1, color: '#0000F6', dn: 9},
			{level: 2, color: '#00FF00', dn: 10},
			{level: 2, color: '#00C800', dn: 11},
			{level: 2, color: '#009000', dn: 12},
			{level: 3, color: '#FFFF00', dn: 13},
			{level: 3, color: '#E7C000', dn: 14},
			{level: 3, color: '#FF9000', dn: 15},
			{level: 3, color: '#FF0000', dn: 16},
			{level: 3, color: '#D60000', dn: 17},
			{level: 3, color: '#C00000', dn: 18},
			{level: 3, color: '#FF00FF', dn: 19},
			{level: 3, color: '#9955C9', dn: 20}
		];
	}
}

new Weather();
