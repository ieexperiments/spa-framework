/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const net = require('net');

let port = 32001; //The same port that the server is listening on
let host = 'pub-vrs.adsbexchange.com';

let socket = new net.Socket();
socket.connect(port, host, function(){
});

let buffer = '';

socket.on('data', function(data){
	buffer += data.toString();
	let arr = buffer.split('{"acList":');
	arr.shift();
	let dupe = arr.slice(0);

	for(let item of arr){
		let str = '{"acList":' + item;
		try{
			let json = JSON.parse(str);
			console.log(JSON.stringify(json, false, 4));
			process.exit();
			dupe.shift();
		} catch(err){
			return false;
		}
	}

	buffer = dupe.join('{"acList":');
});
