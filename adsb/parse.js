/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
global.archivePath = '/Users/mikeshields/Downloads/adsb';
global.tempPath = './temp/';
global.fm = require('../shared/file-manager');
global.SQLite = require('../shared/sqlite');
global.fm = require('../shared/file-manager');
global.async = require('async');
global.moment = require('moment');
global.Utils = require('../shared/utils');
global.binary = require('../shared/binary');
global.vector2d = require('../gis/Vector2d');
global.GisUtils = require('../gis/utils');
global.Geojson = require('../gis/geojson');
global.Reader = require('./modules/reader');
global.Tracks = {};
global.Seconds = 0;
global.ident = false;

class Tracks{
	constructor(){
		global.sqlite = new SQLite(['tracks']);

		let sql = 'delete from tracks;';

		sqlite.exec({db: 'tracks', sql: sql}, function(err, results){
			if(err){ console.log(err); }
			this.process();
		}.bind(this));
	}
	process(){
		let files = fm.fileList({path: archivePath});

		for(let file of files){
			Reader.process(file);
		}
	}
}

new Tracks();
