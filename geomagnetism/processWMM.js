/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const fm = require('../shared/file-manager');
const Utils = require('../shared/utils');
const moment = require('moment');

class GeoMagParser{
	constructor(){
		this.output = {wmm: []};

		fm.readLines({
			path: './WMM.COF',
			onLine: function(line, lineNum){ this.parseLine(line, lineNum); }.bind(this),
			onClose: function(count){ this.write(); }.bind(this)
		});
	}
	write(){
		fm.writeJSON('./gmcof.json', this.output);
	}
	parseLine(line, lineNum){
		let arr = Utils.trim(line).replace(/ +/g, '|').split('|');

		if(lineNum === 1){
			let date = new Date(arr[2]);
			this.output.epoch = Number(arr[0]);
			this.output.name = arr[1];
			this.output.date = date;
			return;
		} else if(arr.length === 6){
			this.output.wmm.push({
				n: Number(arr[0]),
				m: Number(arr[1]),
				gnm: Number(arr[2]),
				hnm: Number(arr[3]),
				dgnm: Number(arr[4]),
				dhnm: Number(arr[5])
			});
		}
	}
}

new GeoMagParser();
