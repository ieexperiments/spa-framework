/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
global.fm = require('../shared/file-manager');
global.Path = require('path');
global.conf = require('../shared/conf');
const Config = require('./modules/config');
const Validate = require('./modules/validate');
const Compress = require('./modules/compress');
const Workers = require('./modules/workers');
const Html = require('./modules/html');
const Libs = require('./modules/libs');

class Compile{
	constructor(){
		let paths = this.setPaths();
		let libs = new Libs(paths);
		let config = new Config(paths);
		let validator = new Validate(paths);
		let compressor = new Compress(paths);
		let html = new Html(paths, compressor.lists);
		let workers = new Workers(paths, compressor.lists);
	}
	setPaths(){
		const basePath = Path.dirname(__dirname);

		let paths = {
			base: basePath + '/',
			current: Path.dirname(process.mainModule.filename) + '/',
			data: Path.resolve(basePath, './data') + '/',
			app: Path.resolve(basePath, './application') + '/',
			logs: Path.resolve(basePath, './logs') + '/',
			node: Path.resolve(basePath, '../node_modules') + '/',
			shared: Path.resolve(basePath, './shared') + '/',
			templates: Path.resolve(basePath, './data/templates') + '/',
		};

		for(let [key, value] of Object.entries(conf.directories)){
			paths[key] = Path.resolve(basePath, value) + '/';
		}

		paths.css = paths.appHtdocs + 'css/';
		paths.lib = paths.appHtdocs + 'lib/';
		paths.pubDist = paths.appProd + 'public/';
		paths.secDist = paths.appProd + 'secure/';
		paths.pubSrc = paths.appDev + 'public/';
		paths.secSrc = paths.appDev + 'secure/';
		paths.pubSrcWorkers = paths.appDev + 'public/workers/';
		paths.secSrcWorkers = paths.appDev + 'secure/workers/';
		paths.pubDistWorkers = paths.appProd + 'public/workers/';
		paths.secDistWorkers = paths.appProd + 'secure/workers/';
		paths.core = paths.appDev + 'core/';

		return paths;
	}
}

module.exports = Compile;
