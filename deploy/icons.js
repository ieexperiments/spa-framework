/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const fm = require('../shared/file-manager');
const conf = require('../shared/conf');
const Path = require('path');
const Imagemagick = require('imagemagick');

class Icons{
	constructor(){
		this.paths = this.setPaths();
		this.source = this.paths.assets + 'icon.png';
		this.sizes = [16, 24, 32, 48, 64, 96, 128, 256, 512];
		this.counter = 0;
		this.len = this.sizes.length;

		this.resize();
	}
	resize(){
		let cmd;
		let size;
		let path;

		for(let pixels of this.sizes){
			size = pixels + 'x' + pixels;
			path = this.paths.images + '/icon-' + size + '.png';
			cmd = [this.source, '-resize', size, path];
			this.createImage(cmd);
		}
	}
	favicon(){
		let sizes = [16, 24, 32, 48, 64];
		let size;
		let cmd = [];

		for(let pixels of sizes){
			size = pixels + 'x' + pixels;
			cmd.push(this.paths.images + '/icon-' + size + '.png');
		}

		cmd.push(this.paths.appHtdocs + 'favicon.ico');

		Imagemagick.convert(cmd, function(err){
			if(err){ console.log(err); }
		});
	}
	createImage(cmd){
		Imagemagick.convert(cmd, function(err){
			this.counter++;

			if(err){ console.log(err); }

			if(this.counter === this.len){
				this.favicon();
			}
		}.bind(this));
	}
	setPaths(){
		const basePath = Path.dirname(__dirname);

		let paths = {
			base: basePath + '/',
			current: Path.dirname(process.mainModule.filename) + '/',
			assets: Path.resolve(basePath, './data/assets') + '/',
			logs: Path.resolve(basePath, './logs') + '/'
		};

		for(let [key, value] of Object.entries(conf.directories)){
			paths[key] = Path.resolve(basePath, value) + '/';
		}

		paths.images = paths.appHtdocs + 'images/';

		return paths;
	}
}

module.exports = Icons;

new Icons();
