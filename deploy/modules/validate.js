/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const jshint = require('jshint').JSHINT;
const plato = require('es6-plato');

class Validate{
	constructor(paths){

		let list = fm.fileList({
			path: paths.base,
			recursive: true,
			extIncludes: ['js'],
			regexExclude: /(.*htdocs\/lib.*)|(.*dist.*)/
		});

		this.process(list, paths);
		/*
		plato.inspect(list, null, {}, function(reports){
			let overview = plato.getOverviewReport(reports);
			fm.writeJSON(paths.logs + 'code-report.json', overview, 4);
		});
		*/
	}
	process(list, paths){
		let errors = {};
		let unused = {};

		for(let path of list){
			jshint(fm.readFile(path, true), {}, {});

			let results = jshint.data();

			this.checkErrors(path, results, errors);
			this.checkUnused(path, results, unused);
		}

		fm.writeJSON(paths.logs + 'js-errors.json', errors, 4);
		fm.writeJSON(paths.logs + 'js-unused.json', unused, 4);
	}
	checkErrors(path, results, errors){
		if(!results.errors){ return; }

		errors[path] = [];

		for(let item of results.errors){
			errors[path].push({line: item.line || 0, reason: item.reason || 'too many errors'});
		}
	}
	checkUnused(path, results, unused){
		if(!results.unused){ return; }

		unused[path] = [];

		for(let item of results.unused){
			unused[path].push({line: item.line, variable: item.name});
		}
	}
}

module.exports= Validate;
