/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Html{
	constructor(paths, lists){
		let relative = this.setPaths(paths, lists);

		this.createFile(paths, relative, true, false);
		this.createFile(paths, relative, true, true);
		this.createFile(paths, relative, false, false);
		this.createFile(paths, relative, false, true);
	}
	createFile(paths, relative, sec, dist){
		let template = fm.readFile(paths.templates + 'index.htm', true);
		let headers = this.getHeaders(sec);
		let path;
		let script;
		let css;

		if(dist){
			css = headers.css.concat(['css/style.min.css']);
			script = headers.script.concat(sec ? ['secure/core.js'] : ['public/core.js']);
		} else {
			css = headers.css.concat(relative.css);
			script = headers.script.concat(sec ? relative.sec : relative.pub);
		}

		css = this.getLinks(css);
		script = this.getScripts(script);

		template = template.replace('{title}', conf.application.title);
		template = template.replace('{desc}', conf.application.description);
		template = template.replace('{link}', css);
		template = template.replace('{script}', script);

		if(sec && dist){ path = paths.secDist; }
		if(sec && !dist){ path = paths.secSrc; }
		if(!sec && dist){ path = paths.pubDist; }
		if(!sec && !dist){ path = paths.pubSrc; }

		fm.writeFile(path + 'index.htm', template);
	}
	getLinks(css){
		let out = [];
		let tag;

		for(let path of css){
			tag = ('<link rel=\'stylesheet\' href=\'{href}\' />').replace('{href}', path);
			out.push(tag);
		}

		return out.join('\n');
	}
	getScripts(script){
		let out = [];
		let tag;

		for(let path of script){
			tag = ('<script src=\'{src}\'></script>').replace('{src}', path);
			out.push(tag);
		}

		return out.join('\n');
	}
	getHeaders(secure){
		let headers = {};

		headers.script = [
			'lib/jquery.min.js',
			'lib/kendo/js/kendo.all.min.js',
			'lib/async.min.js',
			'lib/moment.min.js'
		];

		headers.css = [
			'lib/kendo/styles/kendo.common.min.css',
			'lib/kendo/styles/kendo.{theme}.min.css'.replace('{theme}', conf.application.theme),
			'lib/kendo/styles/kendo.{theme}.mobile.min.css'.replace('{theme}', conf.application.theme),
			'lib/Cesium/Widgets/widgets.css'
		];

		if(conf.appServer.mapServer && secure){
			headers.css.push('lib/mapbox/mapbox-gl.css');
			headers.script.push('lib/mapbox/mapbox-gl.js');
			headers.script.push('lib/Cesium/Cesium.js');
			headers.script.push('lib/jsts.min.js');
			headers.script.push('lib/suncalc.js');
		}

		return headers;
	}
	setPaths(paths, lists){
			let out = {css: [], pub: [], sec: []};

			for(let path of lists.css){
				out.css.push(this.getRelative(paths, path));
			}

			for(let path of lists.pub){
				out.pub.push(this.getRelative(paths, path));
			}

			for(let path of lists.sec){
				out.sec.push(this.getRelative(paths, path));
			}

			return out;
	}
	getRelative(paths, path){
		let out = Path.relative(paths.app, path);
		out = out.replace(/^src\//, '');
		out = out.replace(/^dist\//, '');
		out = out.replace(/^htdocs\//, '');

		return out;
	}
}

module.exports = Html;
