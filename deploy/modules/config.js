/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Config{
	constructor(paths){
		let template = fm.readFile(paths.templates + 'conf.js', true);
		let config = conf.application;
		let tou = config.termsOfUse;
		let show = tou ? true : false;
		let force = (tou && show) ? (tou.force || false) : false;

		template = template.replace('{title}', config.title);
		template = template.replace('{desc}', config.description);
		template = template.replace('{show}', show);
		template = template.replace('{force}', force);

		fm.writeFile(paths.core + 'conf.js', template);
	}
}

module.exports = Config;
