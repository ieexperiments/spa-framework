/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const compressor = require('node-minify');

class Compress{
	constructor(paths){
		let lists = this.getLists(paths);
		let css = lists.css;
		let pub = lists.core.concat(lists.pub);
		let sec = lists.core.concat(lists.sec);

		this.babelrc = paths.data + 'babel.json';
		this.cssBody(paths);
		this.compileJS(paths.pubDist + 'core.js', pub);
		//this.compileJS(paths.secDist + 'core.js', sec);
		this.compileCSS(paths.css + 'style.min.css', css);

		this._lists = {
			css: css,
			pub: pub,
			sec: sec,
			lib: lists.lib,
			pubWorker: lists.pubWorker,
			secWorker: lists.secWorker
		};
	}
	compileJS(path, files){
		compressor.minify({
			compressor: 'babili',
			input: files,
			output: path,
			options: {
				babelrc: this.babelrc
			},
			callback: function (err, min){
				if(err){ console.log('ERROR!!!!', err); }
			}
		});
	}
	compileCSS(path, files){
		compressor.minify({
			compressor: 'clean-css',
			input: files,
			output: path,
			options: {
				advanced: true
			},
			callback: function (err, min){
				if(err){ console.log(err); }
			}
		});
	}
	cssBody(paths){
		let template = fm.readFile(paths.templates + 'body.css', true);
		let fontFamily = conf.application.baseFont.font;
		let fontSize = conf.application.baseFont.size;

		template = template.replace('{fontFamily}', fontFamily).replace('{fontSize}', fontSize);

		fm.writeFile(paths.css + 'body.css', template);
	}
	getLists(paths){
		let path = paths.css + 'style.min.css';
		fm.unlink(path);

		let lists = {
			core: fm.fileList({
				path: paths.core,
				recursive: true,
				extIncludes: ['js']
			}),
			lib: fm.fileList({
				path: paths.lib,
				recursive: true,
				extIncludes: ['js']
			}),
			pub: fm.fileList({
				path: paths.pubSrc,
				recursive: true,
				extIncludes: ['js'],
				regexExclude: /.*public\/workers.*/
			}),
			sec: fm.fileList({
				path: paths.secSrc,
				recursive: true,
				extIncludes: ['js'],
				regexExclude: /.*secure\/workers.*/
			}),
			pubWorker: fm.fileList({
				path: paths.pubSrcWorkers,
				recursive: true,
				extIncludes: ['js']
			}),
			secWorker: fm.fileList({
				path: paths.secSrcWorkers,
				recursive: true,
				extIncludes: ['js']
			}),
			css: fm.fileList({
				path: paths.css,
				recursive: true,
				extIncludes: ['css'],
				dirExcludes: ['lib']
			})
		};

		return lists;
	}
	get lists(){
		return this._lists;
	}
}

module.exports = Compress;
