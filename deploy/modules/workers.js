/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const compressor = require('node-minify');

class Workers{
	constructor(paths, lists){
		this.paths = paths;
		this.babelrc = paths.data + 'babel.json';
		this.build(paths, lists, 'sec');
		this.build(paths, lists, 'pub');
	}
	build(paths, lists, type){
		let srcPath = type === 'pub' ? paths.pubSrcWorkers : paths.secSrcWorkers;
		let dstPath = type === 'pub' ? paths.pubDistWorkers : paths.secDistWorkers;
		let workers = fm.readJSON(srcPath + 'workers.json');
		let list = [];

		if(type === 'pub'){
			list = lists.pub.concat(lists.pubWorker.concat(lists.lib));
		} else {
			list = lists.sec.concat(lists.secWorker.concat(lists.lib));
		}

		if(!workers){ return; }

		for(let worker of workers){
			this.createDev(worker, srcPath);
			this.createProd(worker, dstPath, list);
		}
	}
	createDev(worker, dest){
		let template = fm.readFile(this.paths.templates + 'worker.include', true);

		template = template.replace('{main}', worker.main).replace('{includes}', worker.includes.join("','"));

		fm.writeFile(dest + worker.name + '.include', template);
	}
	createProd(worker, dest, list){
		let template = fm.readFile(this.paths.templates + 'worker.include', true);
		let relative = this.getRelative(dest + worker.main + '.js');
		let includes = [];

		template = template.replace('{main}', worker.main).replace('{includes}', relative);

		fm.writeFile(dest + worker.name + '.include', template);
		for(let include of worker.includes){
			let regex = new RegExp(include, 'g');

			for(let path of list){
				if(path.match(regex)){
					includes.push(path);
				}
			}
		}

		compressor.minify({
			compressor: 'babili',
			input: includes,
			output: dest + worker.main + '.js',
			options: {
				babelrc: this.babelrc
			},
			callback: function (err, min){
				if(err){ console.log(err); }
			}
		});

	}
	getRelative(path){
		let out = Path.relative(this.paths.app, path);
		out = out.replace(/^src\//, '');
		out = out.replace(/^dist\//, '');
		out = out.replace(/^htdocs\//, '');

		return out;
	}
}

module.exports = Workers;
