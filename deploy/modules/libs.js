/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Libs{
	constructor(paths, lists){
		fm.copy(paths.node + 'mapbox-gl/dist', paths.lib + 'mapbox');
		fm.copy(paths.node + 'jquery/dist/jquery.min.js', paths.lib + 'jquery.min.js');
		fm.copy(paths.node + 'async/dist/async.min.js', paths.lib + 'async.min.js');
		fm.copy(paths.node + 'jsts/dist/jsts.min.js', paths.lib + 'jsts.min.js');
		fm.copy(paths.node + 'moment/min/moment.min.js', paths.lib + 'moment.min.js');
	}
}

module.exports = Libs;
