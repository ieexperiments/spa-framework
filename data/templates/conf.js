/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Conf = {
  title: '{title}',
  description: '{desc}',
  termsOfUse: {
    show: {show},
    require: {force},
    agreeText: 'I agree to the Terms of Use',
    terms: [
      {bold: true, text: '**WARNING**WARNING**WARNING**'},
      {bold: false, text: 'This is a Federal Aviation Administration (FAA) computer system. FAA systems, including all related equipment, networks, and network devices (specifically including Internet access) are provided for the processing of official U.S. Government information. Unauthorized access or use of this computer system may subject violators to criminal, civil, and/or administrative action.'},
      {bold: false, text: 'All information on this computer system may be intercepted, recorded, read, copied, and disclosed by and to authorized personnel for official purposes, including criminal investigations. Access or use of this computer system by any person, whether authorized or unauthorized, constitutes consent to these terms.'},
      {bold: true, text: '**WARNING**WARNING**WARNING**'}
    ]
	}
};
