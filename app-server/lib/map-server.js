/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const mbtiles = require('mbtiles');
const tilelive = require('tilelive');
const etag = require('etag');
const request = require('request');
const Utils = require('../../shared/utils');

class MapServer{
	constructor(webserver, app){
		this.base = Path.resolve(pm.paths.base + 'data');
		this.webserver = webserver;
		this.sources = {};
		this.styles = {};
		this.tilers = {};
		this.setTiler();
		this.setRegexp();
		this.setRoutes(app);
	}
	setTiler(){
		mbtiles.registerProtocols(tilelive);

		for(let key of conf.mbtiles.tiles){
			let path = ('mbtiles://{1}/map/mbtiles/{2}.mbtiles').replace('{1}', this.base).replace('{2}', key);

			this.loadMbtile(key, path);
		}
	}
	loadMbtile(key, path){
		tilelive.load(path, function(err, source){
			this.tilers[key] = source;
		}.bind(this));
	}
	setRoutes(app){
		app.use(function(req, res, next){
				res.header('Access-Control-Allow-Origin', '*');
				res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
				next();
		});

		app.all('/map/*', function(req, res, next){
			this.sendAsset(req, res, next);
		}.bind(this));
	}
	sendAsset(req, res, next){
		if(!this.webserver.authenticated(req)){
			this.webserver.httpCode(res, '401', 'Unauthorized');
			return;
		}

		let path = req.path;
		let route = req.path.match(this.regexp.routing)[1];
		let local = this.base  + path;

		switch(route){
			case 'pbf':
				this.sendPbf(req, res, route, local, path);
			break;
			case 'glyphs':
			case 'sprites':
			case 'geojson':
				this.sendFile(req, res, route, local, path);
			break;
			case 'sources':
				this.sendSource(req, res, route, local, path);
			break;
			case 'styles':
				this.sendStyle(req, res, route, local, path);
			break;
			case 'charts':
				this.sendChart(req, res, route, local, path);
			break;
			case 'raster':
				this.sendRaster(req, res, route, local, path);
			break;
		}
	}
	sendRaster(req, res, route, local, path){
		let match = path.match(this.regexp.pbf);
		let z = match[2];
		let x = match[3];
		let y = match[4];
		let url = 'http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}';

		url = url.replace('{z}', z).replace('{x}', x).replace('{y}', y);

		request.get(url).pipe(res);
	}
	sendChart(req, res, route, local, path){
			path = path.replace('/map/', '/opt/') + '.jpg.gz';

			res.status(200);
			res.set('content-encoding', 'gzip');
			res.sendFile(path);
	}
	sendPbf(req, res, route, local, path){
		let match = path.match(this.regexp.pbf);
		let src = match[1];
		let z = match[2];
		let x = match[3];
		let y = match[4];
		let tiler = this.tilers[src];

		tiler.getTile(z, x, y, function(err, tile, headers){
			if(err){
				res.status(404);
				res.send(err.message);
			} else {
				res.set(headers);
				res.send(tile);
			}
		});
	}
	sendSource(req, res, route, local, path){
		let key = path.match(this.regexp.source)[1];

		if(!this.sources[key]){
			let host = this.webserver.getRequestURI(req);
			let src = fm.readFile(local).toString().replace(/\{host\}/g, host);
			this.sources[key] = {content: src, etag: etag(src)};
		}

		res.status(200);
		res.set('Content-Type', 'application/json');
		res.set('Cache-Control', 'public, max-age=259200');
		res.set('Expires', new Date(Date.now() + 259200000).toUTCString());
		res.set('ETag', this.sources[key].etag);
		res.end(this.sources[key].content);
	}
	sendStyle(req, res, route, local, path){
		let key = path.match(this.regexp.style)[1];

		if(!this.sources[key]){
			let host = this.webserver.getRequestURI(req);
			let src = fm.readFile(local).toString().replace(/\{host\}/g, host);
			this.styles[key] = {content: src, etag: etag(src)};
		}

		res.status(200);
		res.set('Content-Type', 'application/json');
		res.set('Cache-Control', 'public, max-age=259200');
		res.set('Expires', new Date(Date.now() + 259200000).toUTCString());
		res.set('ETag', this.styles[key].etag);
		res.end(this.styles[key].content);
	}
	sendFile(req, res, route, local, path){
		if(route === 'glyphs'){
			res.status(200);
			res.set('content-type', 'application/x-protobuf');
			res.set('content-encoding', 'gzip');
		}

		res.sendFile(local);
	}
	setRegexp(){
		this.regexp = {
			routing: /^\/map\/(\w*)\/.*$/,
			source: /^.*\/(.*)\.json$/,
			style: /^.*\/(.*)\.json$/,
			pbf: /^.*\/(.*)\/(\d*)\/(\d*)\/(\d*).*$/
		};
	}
}

module.exports = MapServer;
