/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const expressFavicon = require('express-favicon');
const connectBusboy = require('connect-busboy');
const bodyParser = require('body-parser');
const compression = require('compression');
const morgan = require('morgan');
const streamRotator = require('file-stream-rotator');
const MapServer = require('./map-server');
const fs = require('fs');

class WebServer{
	constructor(app, opts, express){
		this.ajaxRoutes = opts.ajaxRoutes;
		this.setPaths(opts);
		this.setExpress(app);
		this.setLogger(app, opts);
		this.setRoutes(app, opts, express);

		if(opts.mapServer){
			this.mapServer = new MapServer(this, app);
		}
	}
	setPaths(opts){
		let dev = opts.mode === 'development' ? true : false;

		this.paths = {
			htdocs: pm.paths.appHtdocs,
			core: pm.paths.appDev + 'core/',
			pub: (dev ? pm.paths.appDev : pm.paths.appProd) + 'public/',
			sec: (dev ? pm.paths.appDev : pm.paths.appProd) + 'secure/',
			upload: pm.paths.upload
		};
	}
	setExpress(app){
		let limit = conf.appServer.maxUpload || '1mb';

		app.use(expressFavicon(pm.paths.appHtdocs + 'favicon.ico'));
		app.use(compression());
		app.use(connectBusboy({immediate: true}));
		app.use(bodyParser.raw({limit: limit}));
		app.use(bodyParser.text({limit: limit}));
		app.use(bodyParser.json({limit: limit}));
		app.use(bodyParser.urlencoded({limit: limit, extended: false}));
	}
	setLogger(app, opts){
		if(!opts.logging){ return; }

		let stream = streamRotator.getStream({
			date_format: 'YYYYMMDD',
			filename: path.join(pm.paths.logs, 'access-%DATE%.log'),
			frequency: 'daily',
			verbose: false
		});

		app.use(morgan('combined', {stream: stream}));
	}
	setRoutes(app, opts, express){
		app.use('/', express.static(this.paths.htdocs));
		app.use('/public', express.static(this.paths.pub));
		app.use('/secure', function(req, res, next){ this.sendSecure(req, res, next); }.bind(this), express.static(this.paths.sec));
		app.use('/core', function(req, res, next){ this.sendCore(req, res, next); }.bind(this), express.static(this.paths.core));

		app.all('/', function(req, res, next){
			this.root(req, res, next);
		}.bind(this));

		app.all('/auth/', function(req, res, next){
			security.authenticate(req, res, next);
		}.bind(this));

		app.all('/ajax/', function(req, res, next){
			this.routeAjax(req, res, next);
		}.bind(this));

		app.all('/upload/', function(req, res, next){
			console.log('upload');
			this.upload(req, res, next);
		}.bind(this));

		app.all('/filePreview/', function(req, res, next){
			this.filePreview(req, res, next);
		}.bind(this));

		app.all('/fileDownload/', function(req, res, next){
			this.fileDownload(req, res, next);
		}.bind(this));

		this.routes = opts.ajaxRoutes;
	}
	sendCore(req, res, next){
		if(conf.appServer.mode === 'development'){
			next();
		} else {
			this.httpCode(res, '401', 'Unauthorized');
		}
	}
	sendSecure(req, res, next){
		if(!this.authenticated(req)){
			this.httpCode(res, '401', 'Unauthorized');
		} else {
			next();
		}
	}
	routeAjax(req, res){
		let data = req.method === 'POST' ? req.body : req.query;
		let route = this.routes[data.route];

		if(!this.authenticated(req)){
			this.httpCode(res, '401', 'Unauthorized');
			return;
		}

		delete data.route;

		for(let [key, value] of Object.entries(data)){
			try{
					data[key] = JSON.parse(value);
			} catch(err){}
		}

		global[route.module][route.method](req, res, data);
	}
	root(req, res){
		let path;

		if(!this.authenticated(req)){
			path = this.paths.pub + 'index.htm';
		} else {
			path = this.paths.sec + 'index.htm';
		}
		res.sendFile(path);
	}
	getRequestURL(req){
		return req.protocol + '://' + req.get('host') + req.originalUrl;
	}
	getRequestURI(req){
		return req.protocol + '://' + req.get('host');
	}
	sendJSON(res, json){
		res.writeHead(200, {'Content-Type':  'text/javascript'});
		res.end(JSON.stringify(json));
	}
	filePreview(req, res, next){
		if(!this.authenticated(req)){
			this.httpCode(res, '401', 'Unauthorized');
			return;
		}

		filesystem.preview(req, res, next);
	}
	fileDownload(req, res, next){
		if(!this.authenticated(req)){
			this.httpCode(res, '401', 'Unauthorized');
			return;
		}

		filesystem.download(req, res, next);
	}
	upload(req, res, next){
		if(!this.authenticated(req)){
			this.httpCode(res, '401', 'Unauthorized');
			return;
		}
		let files = [];
		let fields = {};

		req.busboy.on('field', function(key, value, keyTruncated, valueTruncated){
			fields[key] = value;
		});

		req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype){
			let uuid = Utils.uuid();
			let path = this.paths.upload + uuid;

			console.log(path);

			files.push({
				uuid: uuid,
				filename: filename,
				mimetype: mimetype
			});

			file.pipe(fs.createWriteStream(path));

			console.log(fieldname, filename, encoding, mimetype);
			/*
			file.on('data', function(data) {
				console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
			});
			file.on('end', function() {
				console.log('File [' + fieldname + '] Finished');
			});
			*/
		}.bind(this));

		req.busboy.on('finish',function(){
			this.sendJSON(res, files);
		}.bind(this));
	}
	httpCode(res, code, message){
		if(message){
			res.status(code).send(message);
		} else {
			res.status(code);
		}
	}
	authenticated(req){
		return (req.session && req.session.uuid) ? true : false;
	}
}

module.exports = WebServer;
