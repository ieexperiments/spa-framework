/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const WsServer = require('ws').Server;

class SocketServer{
	constructor(server){
		this.server = server;
		this.clients = {};
		this.start(server);
	}
	start(appServer){
		this.wss = new WsServer({
			server: appServer.server,
			perMessageDeflate: true
		});

		this.wss.on('connection', function(socket){
			this.authenticate(socket);
		}.bind(this));
	}
	authenticate(socket){
		this.server.sessionParser(socket.upgradeReq, {}, function(){
			let session = socket.upgradeReq.session;

			let conn = {
				uuid: session.uuid,
				guid: session.guid,
				socket: socket,
				username: session.username
			};

			if(!session.uuid){
				this.terminate(conn);
				return;
			}

			this.clients[conn.uuid] = conn;

			Websocket.register(conn);
		}.bind(this));
	}
	broadcast(msg){
		msg = JSON.stringify(msg.payload);

		for(let [key, conn] of Object.entries(this.clients)){
			try	{
				conn.socket.send(msg);
			} catch(err){
				this.terminate(conn);
			}
		}
	}
	terminate(conn){
		try {
			conn.socket.close();
		} catch(err){
			console.log(err);
		}

		delete this.clients[conn.uuid];
	}
}

module.exports = SocketServer;
