/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const http = require('http');
const https = require('https');
const express = require('express');
const WebServer = require('./lib/web-server');
const SocketServer = require('./lib/socket-server');
const expressSession = require('express-session');
const RedisStore = require('connect-redis')(expressSession);

class AppServer{
	constructor(opts){
		this.opts = opts;
		this.start(opts);
	}
	start(opts){
		const app = express();
		const server = opts.secure ? https.createServer({}, app) : http.createServer(app);

		this.setStore(app, opts);
		this.app = app;
		this.server = server;

		if(opts.webServer){
			this.webserver = new WebServer(this.app, opts, express);
		}

		if(opts.socketServer){
			this.socketserver = new SocketServer(this);
		}

		this.server.listen(opts.port);
		console.log('Application Server running on port:', opts.port);
	}
	setStore(app, opts){
		if(!global.redisClient){
			global.redisClient = new RedisClient();
		}

		conf.sessionOptions.genid = function(req){ return Utils.uuid(); };
		conf.sessionOptions.cookie = {secure: opts.secure};
		conf.sessionOptions.store = new RedisStore({client: redisClient.client});
		this.sessionParser = expressSession(conf.sessionOptions);

		app.use(this.sessionParser);
	}
	broadcast(msg){
		this.socketserver.broadcast(msg);
	}
	sendJSON(res, json){
		res.json(json);
	}
}

module.exports = AppServer;
