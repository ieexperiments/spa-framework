/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const UnitConversion = {
	units: {
		distance: {
			km: 1,
			mi: 1000 / 1609.344,
			nm: 1000 / 1852,
			m: 1000,
			yd: 1000 / 0.9144,
			ft: 1000 / 0.3048,
			in: 1000 / 0.0254
		}
	},
	distance: function(dist, from, to){
		if(from === to){ return dist; }

		let a = this.units.distance[from];
		let b = this.units.distance[to];

		return ((dist / a) * b);
	}
};

module.exports = UnitConversion;
