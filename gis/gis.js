/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
let geojson = require('./geojson');
let vector2d = require('./vector2d');

const GIS = {
	geojson: geojson,
	vector2d: vector2d
};

module.exports = GIS;
