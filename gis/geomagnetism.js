/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Geomagnetism = {
	compute(coord, elevation = 0, date = new Date()){
		if(!this.initialized){ this.initialize(); }
		let elev = (elevation / 3280.8399) || 0; // convert h (in feet) to kilometers or set default of 0
		let time = this.decimalDate(date);
		let dt = time - this.epoch;
		let lon = coord[0];
		let lat = coord[1];
		let radLat = this.deg2rad(lat);
		let radLon = this.deg2rad(lon);
		let sinLon = Math.sin(radLon);
		let sinLat = Math.sin(radLat);
		let cosLon = Math.cos(radLon);
		let cosLat = Math.cos(radLat);
		let sqrSin = sinLat * sinLat;
		let sqrCos = cosLat * cosLat;
		let br = 0.0;
		let bt = 0.0;
		let bp = 0.0;
		let bpp = 0.0;
		let temp1 = false;
		let temp2 = false;
		let gv = false;

		this.sp[1] = sinLon;
		this.cp[1] = cosLon;

		let q = Math.sqrt(this.a2 - this.c2 * sqrSin);
		let q1 = elev * q;
		let q2 = ((q1 + this.a2) / (q1 + this.b2)) * ((q1 + this.a2) / (q1 + this.b2));
		let ct = sinLat / Math.sqrt(q2 * sqrCos + sqrSin);
		let st = Math.sqrt(1.0 - (ct * ct));
		let r2 = (elev * elev) + 2.0 * q1 + (this.a4 - this.c4 * sqrSin) / (q * q);
		let r = Math.sqrt(r2);
		let d = Math.sqrt(this.a2 * sqrCos + this.b2 * sqrSin);
		let ca = (elev + d) / r;
		let sa = this.c2 * cosLat * sinLat / (r * d);

		for(let m = 2; m <= this.maxord; m++){
			this.sp[m] = this.sp[1] * this.cp[m - 1] + this.cp[1] * this.sp[m - 1];
			this.cp[m] = this.cp[1] * this.cp[m - 1] - this.sp[1] * this.sp[m - 1];
		}

		let aor = this.re / r;
		let ar = aor * aor;

		for (let n = 1; n <= this.maxord; n++){
			ar = ar * aor;
			let m = 0;

			for(let idx = (n + m + 1); idx > 0; idx--){
				if(n === m){
					this.p[m][n] = st * this.p[m - 1][n - 1];
					this.dp[m][n] = st * this.dp[m - 1][n - 1] + ct * this.p[m - 1][n - 1];
				} else if(n === 1 && m === 0){
					this.p[m][n] = ct * this.p[m][n - 1];
					this.dp[m][n] = ct * this.dp[m][n - 1] - st * this.p[m][n - 1];
				} else if(n > 1 && n !== m){
					if(m > n - 2){ this.p[m][n - 2] = 0; }
					if(m > n - 2){ this.dp[m][n - 2] = 0.0; }
					this.p[m][n] = ct * this.p[m][n - 1] - this.k[m][n] * this.p[m][n - 2];
					this.dp[m][n] = ct * this.dp[m][n - 1] - st * this.p[m][n - 1] - this.k[m][n] * this.dp[m][n - 2];
				}

				this.tc[m][n] = this.c[m][n] + dt * this.cd[m][n];

				if(m !== 0){
					this.tc[n][m - 1] = this.c[n][m - 1] + dt * this.cd[n][m - 1];
				}

				let par = ar * this.p[m][n];

				if(m === 0){
					temp1 = this.tc[m][n] * this.cp[m];
					temp2 = this.tc[m][n] * this.sp[m];
				} else {
					temp1 = this.tc[m][n] * this.cp[m] + this.tc[n][m - 1] * this.sp[m];
					temp2 = this.tc[m][n] * this.sp[m] - this.tc[n][m - 1] * this.cp[m];
				}

				bt = bt - ar * temp1 * this.dp[m][n];
				bp += (this.fm[m] * temp2 * par);
				br += (this.fn[n] * temp1 * par);

				if(st === 0.0 && m === 1){
					if(n === 1){
						this.pp[n] = this.pp[n - 1];
					} else {
						this.pp[n] = ct * this.pp[n - 1] - this.k[m][n] * this.pp[n - 2];
					}
					let parp = ar * this.pp[n];
					bpp += (this.fm[m] * temp2 * parp);
				}

				m++;
			}
		}

		bp = (st === 0.0 ? bpp : bp / st);
		let northComp = -bt * ca - br * sa;
		let eastComp = bp;
		let verticalComp = bt * sa - br * ca;
		let horizIntensity = Math.sqrt((northComp * northComp) + (eastComp * eastComp));
		let totalField = Math.sqrt((horizIntensity * horizIntensity) + (verticalComp * verticalComp));
		let declination = this.rad2deg(Math.atan2(eastComp, northComp));
		let inclination = this.rad2deg(Math.atan2(verticalComp, horizIntensity));

		 if(Math.abs(lat) >= 55.0){
			if(lat > 0.0 && lon >= 0.0){
				gv = declination - lon;
			} else if(lat > 0.0 && lon < 0.0){
				gv = declination + Math.abs(lon);
			} else if(lat < 0.0 && lon >= 0.0){
				gv = declination + lon;
			} else if(lat < 0.0 && lon < 0.0){
				gv = declination - Math.abs(lon);
			}

			if(gv > 180.0){
				gv -= 360.0;
			} else if(gv < -180.0){
				gv += 360.0;
			}
		}

		return {
			declination: declination,
			inclination: inclination,
			totalField: totalField,
			horizIntensity: horizIntensity,
			northComp: northComp,
			eastComp: eastComp,
			verticalComp: verticalComp,
			lat: lat,
			lon: lon,
			changePerYear: gv,
			epoch: this.epoch
		};
	},
	initialize(){
		let a = 6378.137;
		let b = 6356.7523142;
		let snorm = this.multiArray(13);

		this.epoch = this.cof.epoch;
		this.maxord = 12;
		this.re = 6371.2;
		this.fn = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
		this.fm = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
		this.pp = this.multiArray(1);
		this.sp = this.multiArray(1);
		this.cp = this.multiArray(1);
		this.p = this.multiArray(13);
		this.dp = this.multiArray(13);
		this.tc = this.multiArray(13);
		this.c = this.multiArray(13);
		this.cd = this.multiArray(13);
		this.k = this.multiArray(13);
		this.a2 = a * a;
		this.b2 = b * b;
		this.c2 = this.a2 - this.b2;
		this.a4 = this.a2 * this.a2;
		this.b4 = this.b2 * this.b2;
		this.c4 = this.a4 - this.b4;

		this.tc[0][0] = 0;
		this.sp[0] = 0.0;
		this.cp[0] = 1.0;
		this.pp[0] = 1.0;
		this.p[0][0] = 1;
		this.k[1][1] = 0.0;
		snorm[0][0] = 1;

	for(let item of this.cof.wmm){
			if(item.m <= item.n){
				this.c[item.m][item.n] = item.gnm;
				this.cd[item.m][item.n] = item.dgnm;

				if(item.m !== 0){
					this.c[item.n][item.m - 1] = item.hnm;
					this.cd[item.n][item.m - 1] = item.dhnm;
				}
			}
		}

		for(let n = 1; n <= this.maxord; n++){
			snorm[0][n] = snorm[0][n - 1] * (2 * n - 1) / n;
			let j = 2;
		let m = 0;

			for(let idx = (n - m + 1); idx > 0; idx--){
				this.k[m][n] = (((n - 1) * (n - 1)) - (m * m)) / ((2 * n - 1) * (2 * n - 3));

				if(m > 0){
					let flnmj = ((n - m + 1) * j) / (n + m);
					snorm[m][n] = snorm[m - 1][n] * Math.sqrt(flnmj);
					j = 1;
					this.c[n][m - 1] = snorm[m][n] * this.c[n][m - 1];
					this.cd[n][m - 1] = snorm[m][n] * this.cd[n][m - 1];
				}

				this.c[m][n] = snorm[m][n] * this.c[m][n];
				this.cd[m][n] = snorm[m][n] * this.cd[m][n];
				m++;
			}
		}

	this.initialized = true;
	},
	multiArray: function(dimension){
		let arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		let multi = [];

		for(let idx = 0; idx < dimension; idx++){
			multi.push(arr.slice());
		}

		return multi;
	},
	decimalDate: function(date = new Date()){
		let year = date.getFullYear();
		let days = 365 + (((year % 400 === 0) || (year % 4 === 0 && (year % 100 > 0))) ? 1 : 0);
		let ms = days * 24 * 60 * 60 * 1000;

		return date.getFullYear() + (date.valueOf() - (new Date(year, 0)).valueOf()) / ms;
	},
	rad2deg: function(rad){
		return rad * (180 / Math.PI);
	},
	deg2rad: function(deg){
		return deg * (Math.PI / 180);
	},
	model: false,
	cof: {
		epoch: '2015',
		model: 'WMM-2015',
		modelDate: '12/15/2014',
		wmm: [
			{n: 1, m: 0, gnm: -29438.5, hnm: 0, dgnm: 10.7, dhnm: 0},
			{n: 1, m: 1, gnm: -1501.1, hnm: 4796.2, dgnm: 17.9, dhnm: -26.8},
			{n: 2, m: 0, gnm: -2445.3, hnm: 0, dgnm: -8.6, dhnm: 0},
			{n: 2, m: 1, gnm: 3012.5, hnm: -2845.6, dgnm: -3.3, dhnm: -27.1},
			{n: 2, m: 2, gnm: 1676.6, hnm: -642, dgnm: 2.4, dhnm: -13.3},
			{n: 3, m: 0, gnm: 1351.1, hnm: 0, dgnm: 3.1, dhnm: 0},
			{n: 3, m: 1, gnm: -2352.3, hnm: -115.3, dgnm: -6.2, dhnm: 8.4},
			{n: 3, m: 2, gnm: 1225.6, hnm: 245, dgnm: -0.4, dhnm: -0.4},
			{n: 3, m: 3, gnm: 581.9, hnm: -538.3, dgnm: -10.4, dhnm: 2.3},
			{n: 4, m: 0, gnm: 907.2, hnm: 0, dgnm: -0.4, dhnm: 0},
			{n: 4, m: 1, gnm: 813.7, hnm: 283.4, dgnm: 0.8, dhnm: -0.6},
			{n: 4, m: 2, gnm: 120.3, hnm: -188.6, dgnm: -9.2, dhnm: 5.3},
			{n: 4, m: 3, gnm: -335, hnm: 180.9, dgnm: 4, dhnm: 3},
			{n: 4, m: 4, gnm: 70.3, hnm: -329.5, dgnm: -4.2, dhnm: -5.3},
			{n: 5, m: 0, gnm: -232.6, hnm: 0, dgnm: -0.2, dhnm: 0},
			{n: 5, m: 1, gnm: 360.1, hnm: 47.4, dgnm: 0.1, dhnm: 0.4},
			{n: 5, m: 2, gnm: 192.4, hnm: 196.9, dgnm: -1.4, dhnm: 1.6},
			{n: 5, m: 3, gnm: -141, hnm: -119.4, dgnm: 0, dhnm: -1.1},
			{n: 5, m: 4, gnm: -157.4, hnm: 16.1, dgnm: 1.3, dhnm: 3.3},
			{n: 5, m: 5, gnm: 4.3, hnm: 100.1, dgnm: 3.8, dhnm: 0.1},
			{n: 6, m: 0, gnm: 69.5, hnm: 0, dgnm: -0.5, dhnm: 0},
			{n: 6, m: 1, gnm: 67.4, hnm: -20.7, dgnm: -0.2, dhnm: 0},
			{n: 6, m: 2, gnm: 72.8, hnm: 33.2, dgnm: -0.6, dhnm: -2.2},
			{n: 6, m: 3, gnm: -129.8, hnm: 58.8, dgnm: 2.4, dhnm: -0.7},
			{n: 6, m: 4, gnm: -29, hnm: -66.5, dgnm: -1.1, dhnm: 0.1},
			{n: 6, m: 5, gnm: 13.2, hnm: 7.3, dgnm: 0.3, dhnm: 1},
			{n: 6, m: 6, gnm: -70.9, hnm: 62.5, dgnm: 1.5, dhnm: 1.3},
			{n: 7, m: 0, gnm: 81.6, hnm: 0, dgnm: 0.2, dhnm: 0},
			{n: 7, m: 1, gnm: -76.1, hnm: -54.1, dgnm: -0.2, dhnm: 0.7},
			{n: 7, m: 2, gnm: -6.8, hnm: -19.4, dgnm: -0.4, dhnm: 0.5},
			{n: 7, m: 3, gnm: 51.9, hnm: 5.6, dgnm: 1.3, dhnm: -0.2},
			{n: 7, m: 4, gnm: 15, hnm: 24.4, dgnm: 0.2, dhnm: -0.1},
			{n: 7, m: 5, gnm: 9.3, hnm: 3.3, dgnm: -0.4, dhnm: -0.7},
			{n: 7, m: 6, gnm: -2.8, hnm: -27.5, dgnm: -0.9, dhnm: 0.1},
			{n: 7, m: 7, gnm: 6.7, hnm: -2.3, dgnm: 0.3, dhnm: 0.1},
			{n: 8, m: 0, gnm: 24, hnm: 0, dgnm: 0, dhnm: 0},
			{n: 8, m: 1, gnm: 8.6, hnm: 10.2, dgnm: 0.1, dhnm: -0.3},
			{n: 8, m: 2, gnm: -16.9, hnm: -18.1, dgnm: -0.5, dhnm: 0.3},
			{n: 8, m: 3, gnm: -3.2, hnm: 13.2, dgnm: 0.5, dhnm: 0.3},
			{n: 8, m: 4, gnm: -20.6, hnm: -14.6, dgnm: -0.2, dhnm: 0.6},
			{n: 8, m: 5, gnm: 13.3, hnm: 16.2, dgnm: 0.4, dhnm: -0.1},
			{n: 8, m: 6, gnm: 11.7, hnm: 5.7, dgnm: 0.2, dhnm: -0.2},
			{n: 8, m: 7, gnm: -16, hnm: -9.1, dgnm: -0.4, dhnm: 0.3},
			{n: 8, m: 8, gnm: -2, hnm: 2.2, dgnm: 0.3, dhnm: 0},
			{n: 9, m: 0, gnm: 5.4, hnm: 0, dgnm: 0, dhnm: 0},
			{n: 9, m: 1, gnm: 8.8, hnm: -21.6, dgnm: -0.1, dhnm: -0.2},
			{n: 9, m: 2, gnm: 3.1, hnm: 10.8, dgnm: -0.1, dhnm: -0.1},
			{n: 9, m: 3, gnm: -3.1, hnm: 11.7, dgnm: 0.4, dhnm: -0.2},
			{n: 9, m: 4, gnm: 0.6, hnm: -6.8, dgnm: -0.5, dhnm: 0.1},
			{n: 9, m: 5, gnm: -13.3, hnm: -6.9, dgnm: -0.2, dhnm: 0.1},
			{n: 9, m: 6, gnm: -0.1, hnm: 7.8, dgnm: 0.1, dhnm: 0},
			{n: 9, m: 7, gnm: 8.7, hnm: 1, dgnm: 0, dhnm: -0.2},
			{n: 9, m: 8, gnm: -9.1, hnm: -3.9, dgnm: -0.2, dhnm: 0.4},
			{n: 9, m: 9, gnm: -10.5, hnm: 8.5, dgnm: -0.1, dhnm: 0.3},
			{n: 10, m: 0, gnm: -1.9, hnm: 0, dgnm: 0, dhnm: 0},
			{n: 10, m: 1, gnm: -6.5, hnm: 3.3, dgnm: 0, dhnm: 0.1},
			{n: 10, m: 2, gnm: 0.2, hnm: -0.3, dgnm: -0.1, dhnm: -0.1},
			{n: 10, m: 3, gnm: 0.6, hnm: 4.6, dgnm: 0.3, dhnm: 0},
			{n: 10, m: 4, gnm: -0.6, hnm: 4.4, dgnm: -0.1, dhnm: 0},
			{n: 10, m: 5, gnm: 1.7, hnm: -7.9, dgnm: -0.1, dhnm: -0.2},
			{n: 10, m: 6, gnm: -0.7, hnm: -0.6, dgnm: -0.1, dhnm: 0.1},
			{n: 10, m: 7, gnm: 2.1, hnm: -4.1, dgnm: 0, dhnm: -0.1},
			{n: 10, m: 8, gnm: 2.3, hnm: -2.8, dgnm: -0.2, dhnm: -0.2},
			{n: 10, m: 9, gnm: -1.8, hnm: -1.1, dgnm: -0.1, dhnm: 0.1},
			{n: 10, m: 10, gnm: -3.6, hnm: -8.7, dgnm: -0.2, dhnm: -0.1},
			{n: 11, m: 0, gnm: 3.1, hnm: 0, dgnm: 0, dhnm: 0},
			{n: 11, m: 1, gnm: -1.5, hnm: -0.1, dgnm: 0, dhnm: 0},
			{n: 11, m: 2, gnm: -2.3, hnm: 2.1, dgnm: -0.1, dhnm: 0.1},
			{n: 11, m: 3, gnm: 2.1, hnm: -0.7, dgnm: 0.1, dhnm: 0},
			{n: 11, m: 4, gnm: -0.9, hnm: -1.1, dgnm: 0, dhnm: 0.1},
			{n: 11, m: 5, gnm: 0.6, hnm: 0.7, dgnm: 0, dhnm: 0},
			{n: 11, m: 6, gnm: -0.7, hnm: -0.2, dgnm: 0, dhnm: 0},
			{n: 11, m: 7, gnm: 0.2, hnm: -2.1, dgnm: 0, dhnm: 0.1},
			{n: 11, m: 8, gnm: 1.7, hnm: -1.5, dgnm: 0, dhnm: 0},
			{n: 11, m: 9, gnm: -0.2, hnm: -2.5, dgnm: 0, dhnm: -0.1},
			{n: 11, m: 10, gnm: 0.4, hnm: -2, dgnm: -0.1, dhnm: 0},
			{n: 11, m: 11, gnm: 3.5, hnm: -2.3, dgnm: -0.1, dhnm: -0.1},
			{n: 12, m: 0, gnm: -2, hnm: 0, dgnm: 0.1, dhnm: 0},
			{n: 12, m: 1, gnm: -0.3, hnm: -1, dgnm: 0, dhnm: 0},
			{n: 12, m: 2, gnm: 0.4, hnm: 0.5, dgnm: 0, dhnm: 0},
			{n: 12, m: 3, gnm: 1.3, hnm: 1.8, dgnm: 0.1, dhnm: -0.1},
			{n: 12, m: 4, gnm: -0.9, hnm: -2.2, dgnm: -0.1, dhnm: 0},
			{n: 12, m: 5, gnm: 0.9, hnm: 0.3, dgnm: 0, dhnm: 0},
			{n: 12, m: 6, gnm: 0.1, hnm: 0.7, dgnm: 0.1, dhnm: 0},
			{n: 12, m: 7, gnm: 0.5, hnm: -0.1, dgnm: 0, dhnm: 0},
			{n: 12, m: 8, gnm: -0.4, hnm: 0.3, dgnm: 0, dhnm: 0},
			{n: 12, m: 9, gnm: -0.4, hnm: 0.2, dgnm: 0, dhnm: 0},
			{n: 12, m: 10, gnm: 0.2, hnm: -0.9, dgnm: 0, dhnm: 0},
			{n: 12, m: 11, gnm: -0.9, hnm: -0.2, dgnm: 0, dhnm: 0},
			{n: 12, m: 12, gnm: 0, hnm: 0.7, dgnm: 0, dhnm: 0}
		]
	}
};

module.exports = Geomagnetism;
