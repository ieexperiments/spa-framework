/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Regression{
	constructor(type, data, order = 2){
		let results = false;

		switch(type){
			case 'linear':
				results = this.linear(data);
			break;
			case 'linearThroughOrigin':
				results = this.linearThroughOrigin(data);
			break;
			case 'exponential':
				results = this.exponential(data);
			break;
			case 'logarithmic':
				results = this.logarithmic(data);
			break;
			case 'power':
				results = this.power(data);
			break;
			case 'polynomial':
				results = this.polynomial(data, order);
			break;
			case 'lastvalue':
				results = this.lastvalue(data);
			break;
		}

		return results;

	}
	linear(data){
		let sum = [0, 0, 0, 0, 0];
		let results = [];
		let len = data.length;
		let n = 0;

		for(n = 0; n < len; n++){
			if(data[n][1] !== null){
				sum[0] += data[n][0];
				sum[1] += data[n][1];
				sum[2] += data[n][0] * data[n][0];
				sum[3] += data[n][0] * data[n][1];
				sum[4] += data[n][1] * data[n][1];
			}
		}

		let gradient = (n * sum[3] - sum[0] * sum[1]) / (n * sum[2] - sum[0] * sum[0]);
		let intercept = (sum[1] / n) - (gradient * sum[0]) / n;

		for(let i = 0; i < len; i++){
			let coordinate = [data[i][0], data[i][0] * gradient + intercept];
			results.push(coordinate);
		}

		let string = 'y = ' + Math.round(gradient * 100) / 100 + 'x + ' + Math.round(intercept * 100) / 100;

		return {equation: [gradient, intercept], points: results, string: string};
	}
	linearThroughOrigin(data){
		let sum = [0, 0];
		let results = [];
		let len = data.length;

		for(let n = 0; n < data.length; n++){
			if(data[n][1] !== null){
				sum[0] += data[n][0] * data[n][0];
				sum[1] += data[n][0] * data[n][1];
			}
		}

		let gradient = sum[1] / sum[0];

		for(let i = 0; i < len; i++){
			let coordinate = [data[i][0], data[i][0] * gradient];
			results.push(coordinate);
		}

		let string = 'y = ' + Math.round(gradient * 100) / 100 + 'x';

		return {equation: [gradient], points: results, string: string};
	}
	exponential(data){
		let sum = [0, 0, 0, 0, 0, 0];
		let results = [];
		let n = 0;
		let len = data.length;

		for(n = 0; n < len; n++){
			if(data[n][1] !== null){
				sum[0] += data[n][0];
				sum[1] += data[n][1];
				sum[2] += data[n][0] * data[n][0] * data[n][1];
				sum[3] += data[n][1] * Math.log(data[n][1]);
				sum[4] += data[n][0] * data[n][1] * Math.log(data[n][1]);
				sum[5] += data[n][0] * data[n][1];
			}
		}

		let denominator = (sum[1] * sum[2] - sum[5] * sum[5]);
		let A = Math.pow(Math.E, (sum[2] * sum[3] - sum[5] * sum[4]) / denominator);
		let B = (sum[1] * sum[4] - sum[5] * sum[3]) / denominator;

		for(let i = 0; i < len; i++){
			let coordinate = [data[i][0], A * Math.pow(Math.E, B * data[i][0])];
			results.push(coordinate);
		}

		let string = 'y = ' + Math.round(A * 100) / 100 + 'e^(' + Math.round(B * 100) / 100 + 'x)';

		return {equation: [A, B], points: results, string: string};
	}
	logarithmic(data){
		let sum = [0, 0, 0, 0];
		let results = [];
		let n = 0;
		let len = data.length;

		for(n = 0; n < len; n++){
			if(data[n][1] !== null){
				sum[0] += Math.log(data[n][0]);
				sum[1] += data[n][1] * Math.log(data[n][0]);
				sum[2] += data[n][1];
				sum[3] += Math.pow(Math.log(data[n][0]), 2);
			}
		}

		let B = (n * sum[1] - sum[2] * sum[0]) / (n * sum[3] - sum[0] * sum[0]);
		let A = (sum[2] - B * sum[0]) / n;

		for(let i = 0; i < len; i++){
			let coordinate = [data[i][0], A + B * Math.log(data[i][0])];
			results.push(coordinate);
		}

		let string = 'y = ' + Math.round(A * 100) / 100 + ' + ' + Math.round(B * 100) / 100 + ' ln(x)';

		return {equation: [A, B], points: results, string: string};
	}
	power(data){
		let sum = [0, 0, 0, 0];
		let results = [];
		let n = 0;
		let len = data.length;

		for(n = 0; n < len; n++){
			if(data[n][1] !== null){
				sum[0] += Math.log(data[n][0]);
				sum[1] += Math.log(data[n][1]) * Math.log(data[n][0]);
				sum[2] += Math.log(data[n][1]);
				sum[3] += Math.pow(Math.log(data[n][0]), 2);
			}
		}

		let B = (n * sum[1] - sum[2] * sum[0]) / (n * sum[3] - sum[0] * sum[0]);
		let A = Math.pow(Math.E, (sum[2] - B * sum[0]) / n);

		for(let i = 0; i < len; i++){
			let coordinate = [data[i][0], A * Math.pow(data[i][0] , B)];
			results.push(coordinate);
		}

		let string = 'y = ' + Math.round(A * 100) / 100 + 'x^' + Math.round(B * 100) / 100;

		return {equation: [A, B], points: results, string: string};
	}
	polynomial(data, order){
		let lhs = [];
		let rhs = [];
		let results = [];
		let k = order + 1;
		let a = 0;
		let b = 0;
		let len = data.length;

		for(let i = 0; i < k; i++){
			for(let l = 0; l < len; l++){
				if(data[l][1] !== null){
					a += Math.pow(data[l][0], i) * data[l][1];
				}
			}

			lhs.push(a);
			a = 0;
			len = data.length;
			let c = [];

			for(let j = 0; j < k; j++){
				for(let l = 0, len = data.length; l < len; l++){
					if(data[l][1] !== null){
						b += Math.pow(data[l][0], i + j);
					}
				}
				c.push(b);
				b = 0;
			}
			rhs.push(c);
		}

		rhs.push(lhs);
		let equation = this.gaussianElimination(rhs, k);

		for(let i = 0; i < len; i++){
			let answer = 0;

			for(let w = 0; w < equation.length; w++){
				answer += equation[w] * Math.pow(data[i][0], w);
			}

			results.push([data[i][0], answer]);
		}

		let string = 'y = ';

		for(let i = equation.length-1; i >= 0; i--){
			if(i > 1){
				string += Math.round(equation[i] * Math.pow(10, i)) / Math.pow(10, i)  + 'x^' + i + ' + ';
			} else if(i === 1){
				string += Math.round(equation[i]*100) / 100 + 'x' + ' + ';
			} else{
				string += Math.round(equation[i]*100) / 100;
			}
		}

		return {equation: equation, points: results, string: string};
	}
	lastvalue(data){
		let results = [];
		let lastvalue = null;
		let len = data.length;

		for(let i = 0; i < len; i++){
			if(data[i][1]){
				lastvalue = data[i][1];
				results.push([data[i][0], data[i][1]]);
			} else {
				results.push([data[i][0], lastvalue]);
			}
		}

		return {equation: [lastvalue], points: results, string: "" + lastvalue};
	}
	gaussianElimination(a, o){
		let n = a.length - 1;
		let x = [o];
		let tmp = 0;

		for(let i = 0; i < n; i++){
				let maxrow = i;

				for(let j = i + 1; j < n; j++){
					if(Math.abs(a[i][j]) > Math.abs(a[i][maxrow])){ maxrow = j; }
				}

				for(let k = i; k < n + 1; k++){
					tmp = a[k][i];
					a[k][i] = a[k][maxrow];
					a[k][maxrow] = tmp;
				}

				for(let j = i + 1; j < n; j++){
					for(let k = n; k >= i; k--){
							a[k][j] -= a[k][i] * a[i][j] / a[i][i];
					}
				}
		}
		for(let j = n - 1; j >= 0; j--){
				tmp = 0;
				for(let k = j + 1; k < n; k++){
					tmp += a[k][j] * x[k];
				}

				x[j] = (a[n][j] - tmp) / a[j][j];
		}

		return (x);
	}
}

let data = [[-10, -738],[-9, -520],[-8, -350],[-7, -222],[-6, -130],[-5, -68],[-4, -30],[-3, -10],[-2, -2],[-1, 0],[0, 2],[1, 10],[2, 30],[3, 68],[4, 130],[5, 222],[6, 350],[7, 520],[8, 738],[9, 1010],[10, 1342]];

//console.log('linear', new Regression('linear', data));
//console.log('linearThroughOrigin', new Regression('linearThroughOrigin', data));
//console.log('exponential', new Regression('exponential', data));
//console.log('logarithmic', new Regression('logarithmic', data));
//console.log('power', new Regression('power', data));
//console.log('polynomial', new Regression('polynomial', data));
console.log('lastvalue', new Regression('lastvalue', data));
