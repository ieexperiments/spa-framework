/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Vector2d = require('./vector2d');

const GisUtils = {
	densify: function(data, distance = 50, units = 'km'){
		let obj = this.toSegments(data);

		for(let [key, segments] of obj.segments.entries()){
			let arr = [];

			for(let [idx, segment] of segments.entries()){
				let p1 = segment[0];
				let p2 = segment[1];
				let min = p1[0] < p2[0] ? p1[0] : p2[0];
				let max = p1[0] > p2[0] ? p1[0] : p2[0];
				let vector = new Vector2d(p1);
				let length = vector.distance(p2, units);
				let prev = false;

				if(length >= distance){
					let count = Math.ceil(length / distance);

					for(let offset = 1; offset < count; offset++){
						let point = vector.intermediatePoint(p2, offset/count);
						if(point[0] < min){ point[0] += 360; }
						if(point[0] > max){ point[0] -= 360; }
						arr.push([prev || p1, point]);
						prev = point;
					}
					arr.push([prev, p2]);
					obj.segments[key] = arr;
				}
			}
		}

		return this.fromSegments(obj);
	},
	interpolate(data){
		let obj = this.toSegments(data);

		for(let [key, segments] of obj.segments.entries()){
			for(let [idx, curr] of segments.entries()){
				let next = segments[idx + 1];
			}
		}
	},
	lineLength(data, units){
		let obj = this.toSegments(data);
		let dist = 0;

		for(let [idx, segment] of obj.segments[0].entries()){
			let p1 = segment[0];
			let p2 = segment[1];
			let vector = new Vector2d(p1);

			dist += vector.distance(p2, units);
		}

		return {distance: dist, units: units};
	},
	pointOnLine(line, distance, units){
		let travelled = 0;
		let point = false;

		if(!distance){
			return line[0];
		}

		for(let [idx, curr] of line.entries()){
			let next = line[idx + 1];

			if(!next){
				point = curr;
				break;
			}

			let vector = new Vector2d(curr);
			let dist = vector.distance(next, units);

			travelled += dist;

			if(travelled >= distance){
				let overshot = distance - travelled;
				if(!overshot){
					point = next;
					break;
				} else {
					let bearing = vector.bearing(next);
					point = vector.pointAtBearing(bearing, overshot, units);
					break;
				}
			}
		}

		return point;

	},
	spline(line, tension = 0.5, closed = false, segments = 16){
		let points = line.slice(0);
		let out = [];

		if(closed){
			points.unshift(line[line.length - 1]);
			points.unshift(line[line.length - 1]);
			points.push(line[0]);
		} else {
			points.unshift(line[0]);
			points.push(line[line.length - 1]);
		}

		for(let i = 1; i < (points.length - 2); i++){
			for(let t = 0; t <= segments; t++){
				let t1x = (points[i+1][0] - points[i-1][0]) * tension;
				let t2x = (points[i+2][0] - points[i][0]) * tension;
				let t1y = (points[i+1][1] - points[i-1][1]) * tension;
				let t2y = (points[i+2][1] - points[i][1]) * tension;
				let step = t / segments;
				let c1 = 2 * Math.pow(step, 3) - 3 * Math.pow(step, 2) + 1;
				let c2 = -(2 * Math.pow(step, 3)) + 3 * Math.pow(step, 2);
				let c3 = Math.pow(step, 3) - 2 * Math.pow(step, 2) + step;
				let c4 = Math.pow(step, 3) - Math.pow(step, 2);
				let x = c1 * points[i][0] + c2 * points[i+1][0] + c3 * t1x + c4 * t2x;
				let y = c1 * points[i][1] + c2 * points[i+1][1] + c3 * t1y + c4 * t2y;

				out.push([x, y]);
			}
		}

		return out;
	},
	toSegments(obj){
		let str = JSON.stringify(obj);
		let match = str.match(/(\[\[-?\d(.(?!\]\]))*\d\]\])/g);
		let output = {segments: []};

		for(let [key, value] of match.entries()){
			let arr = JSON.parse(value);
			let segments = [];
			str = str.replace(value, '{' + key + '}');

			for(let [idx, curr] of arr.entries()){
				let next = arr[idx + 1];
				if(next){ segments.push([curr, next]); }
			}

			output.segments.push(segments);
		}

		output.str = str;
		return output;
	},
	fromSegments(obj){
		for(let [key, segments] of obj.segments.entries()){
			let arr = [];
			key = '{' + key + '}';

			for(let [idx, segment] of segments.entries()){
				if(!idx){ arr.push(segment[0]); }
				arr.push(segment[1]);
			}

			obj.str = obj.str.replace(key, JSON.stringify(arr));
		}

		return JSON.parse(obj.str);
	},
	rotateDegree(degree, rotate){
		let deg = (degree + rotate) % 360;

		if(deg < 0){ deg = 360 + deg; }

		return deg;
	},
	checkGtype(type, rings){
		let gtype = type;
		let len = rings.length;

		switch(len){
			case 1:
				if(gtype === 'pointbuffer'){
					gtype = 'pointbuffer';
				} else {
					gtype = 'point';
				}
			break;
			case 2:
				if(type === 'polygon'){
					gtype = 'linestring';
				} else {
					gtype = type;
				}
			break;
			default:
				gtype = type;
			break;
		}

		return gtype;
	}
};

module.exports = GisUtils;
