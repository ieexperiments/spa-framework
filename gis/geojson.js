/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class GeoJSON{
	constructor(geojson = false){
		this._features = [];
		this._geometries = [];

		if(!geojson){ return; }

		switch(geojson.type){
			case 'Point':
			case 'LineString':
			case 'Polygon':
			case 'MultiPoint':
			case 'MultiLineString':
			case 'MultiPolygon':
				this._geometries = [geojson];
				this._features = [{type: 'Feature', geometry: geojson}];
			break;
			case 'Feature':
				this.setFeatures([geojson]);
			break;
			case 'FeatureCollection':
				this.setFeatures(geojson.features);
			break;
			case 'GeometryCollection':
				this._geometries = geojson.geometries;
			break;
		}
	}
	point(coordinates, properties = {}){
		return this.feature('Point', coordinates, properties);
	}
	polygon(coordinates, properties = {}){
		return this.feature('Polygon', coordinates, properties);
	}
	linestring(coordinates, properties = {}){
		return this.feature('LineString', coordinates, properties);
	}
	multipoint(coordinates, properties = {}){
		return this.feature('MultiPoint', coordinates, properties);
	}
	multipolygon(coordinates, properties = {}){
		return this.feature('MultiPolygon', coordinates, properties);
	}
	multilinestring(coordinates, properties = {}){
		return this.feature('MultiLineString', coordinates, properties);
	}
	feature(type, coordinates, properties = {}){
		let geometry = {type: type, coordinates: coordinates};
		let feature = {type: 'Feature', properties: properties, geometry: geometry};
		this._geometries.push(geometry);
		this._features.push(feature);

		return feature;
	}
	setFeatures(features){
		let arr1 = [];
		let arr2 = [];

		for(let feature of features){
			arr1.push(feature);
			arr2.push(feature.geometry);
		}

		this._features = arr1;
		this._geometries = arr2;
	}
	addFeatures(features){
		let arr1 = [];
		let arr2 = [];

		for(let feature of features){
			arr1.push(feature);
			arr2.push(feature.geometry);
		}

		this._features = this._features.concat(arr1);
		this._geometries = this._geometries.concat(arr2);
	}
	getFeatures(prop, val){
		let output = [];

		for(let feature of this._features){
			if(feature.properties[prop] && feature.properties[prop] === val){
				output.push(feature);
			}
		}

		return output;
	}
	geometryCollection(){
			return {type: 'GeometryCollection', geometries: this._geometries};
	}
	featureCollection(){
		return {type: 'FeatureCollection', features: this._features};
	}
	get features(){
		return this._features;
	}
	get geometries(){
		return this._geometries;
	}
	set features(features){
			this.setFeatures(features);
	}
	set geometries(geometries){
			this._geometries = geometries;
	}
}

module.exports = GeoJSON;
