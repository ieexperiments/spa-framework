/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const uri = 'http://aviationweather.gov/adds/dataserver_current/httpparam?datasource=metars&requesttype=retrieve&format=csv&hoursBeforeNow=1.25&stationString=';

const Metars = {
	process(){
		let parms = ['a','b','c','d','e','f','g','h','i','j','ka','kb','kc','kd','ke','kf','kg','kh','ki','kj','kk','kl','km','kn','ko','kp','kq','kr','ks','kt','ku','kv','kw','kx','ky','kz','k0','k1','k2','k3','k4','k5','k6','k7','k8','k9','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
		let batch = [];

		this.data = [];

		for(let parm of parms){
			batch.push({key: parm, path: uri + parm + '*'});
		}

		Utils.asyncBatch({module: this, method: 'download', async: 'parallel', payload: batch}, function(err, result){
			batch = fm.fileList({
				path: './temp',
				recursive: false,
				extIncludes: ['metar']
			});

			Utils.asyncBatch({module: this, method: 'readData', async: 'series', payload: batch}, function(err, result){
				this.load();
			}.bind(this));
		}.bind(this));
	},
	load(){
		let sql = 'insert or replace into metars values (' + this.binds.join(',') + ')';

		sqlite.bulk({db: 'weather', sql: sql, binds: this.data}, function(){
			console.log('loaded');
		});
	},
	readData(path, callback){
		fm.readCSV({
			path: path,
			onClose: function(data){
				for(let line of data){
					this.processLine(line);
				}
				fm.unlink(path);
				callback();
			}.bind(this)
		});
	},
	processLine(arr){
		let obj = {};

		if(arr[0] === 'raw_text' && !this.mapping){
			this.mapping = [];
			this.binds = [];
			for(let [idx, value] of arr.entries()){
				this.mapping.push({field: value, sample: null, len: 0});
				this.binds.push('$' + (idx + 1));
			}
		}

		if(arr.length === 1 || arr[0] === 'raw_text'){ return; }

		for(let [idx, value] of arr.entries()){
			obj['$' + (idx + 1)] = value;
			let len = value.length;
			if(this.mapping[idx].len < len){
				this.mapping[idx].len = len;
				this.mapping[idx].sample = value;
			}
		}

		this.data.push(obj);
	},
	download(obj, callback){
		let req = request(obj.path);
		let path = './temp/metars_' + obj.key + '.metar';

		req.pipe(fs.createWriteStream(path));

		req.on('end', function(response, body){
			callback(null, null);
		}.bind(this));
	}
};

module.exports = Metars;
