/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const uri = 'https://aviationweather.gov/adds/dataserver_current/current/';
const xml2js = require('xml2js');

const sources = {
	metars: {table: 'metars', file: 'metars.cache.xml'},
	tafs: {table: 'tafs', file: 'tafs.cache.xml'},
	pireps: {table: 'pireps', file: 'aircraftreports.cache.xml'}
};

class ADDS{
	constructor(source){
		this.conf = sources[source];
		this.data = [];
		this.mapping = [];
		this.binds = [];
		this.first = true;

		let src = uri + this.conf.file;
		let dest = './temp/' + this.conf.file;

		fm.download(src, dest, function(){
			this.readData(dest, function(){
				this.load();
			}.bind(this));
		}.bind(this));
	}
	load(){
		let sql = 'insert or replace into {table} values (' + this.binds.join(',') + ')';

		sql = sql.replace('{table}', this.conf.table);

		console.log(this.mapping);

		return;

		sqlite.bulk({db: 'weather', sql: sql, binds: this.data}, function(){
			console.log('loaded');
		});
	}
	readData(path, callback){
		let data = fm.readFile(path, true);
		let options = {trim: true};
		let parser = new xml2js.Parser(options);

		parser.parseString(data, function(err, result){
			console.log(err);
			console.log(result);
			callback();
		});

		return;
	}
	processLine(arr){
		let obj = {};

		if(arr.length === 1){ return; }

		if(this.first){
			this.first = false;
			for(let [idx, value] of arr.entries()){
				value = value || 'field';
				this.mapping.push({field: value, sample: null, len: 0});
				this.binds.push('$' + (idx + 1));
			}
			return;
		}
console.log(arr.length);

		for(let [idx, value] of arr.entries()){
			obj['$' + (idx + 1)] = value;
			let len = value.length;

			if(this.mapping[idx].len < len){
				this.mapping[idx].len = len;
				this.mapping[idx].sample = value;
			}
		}

		this.data.push(obj);
	}
}

module.exports = ADDS;
