/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const processManager = require('../shared/process-manager');

class Server{
	constructor(){
		global.pm = new processManager({
			main: this,
			appServer: false,
			modules: {
				AADS: './lib/adds',
				fm: '../shared/file-manager',
				request: 'request',
				SQLite: 'shared/sqlite'
			}
		});

		global.sqlite = new SQLite(['weather']);
		new AADS('metars');
		//new AADS('tafs');
		//new AADS('pireps');
	}
}

new Server();
