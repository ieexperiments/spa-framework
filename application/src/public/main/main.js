/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Main{
	constructor(){
		this.setModel();
		this.setView();
		this.setShortcuts();
	}
	authenticate(){
		let username = this.model.username;
		let password = this.model.password;

		if(!this.model.agree){
			this.alert.show({type: 'warning', message: 'You must agree to the Terms of Use.'});
			return;
		}

		if(!username || !password){
			this.alert.show({type: 'warning', message: 'Username and password are required.'});
			return;
		}

		this.spinner.start(true);

		Utils.ajax({
			path: 'auth',
			route: 'authentication',
			data: {username: username, password: password},
			success: function(data){
				this.checkAuthCode(data.code);
			}.bind(this),
			error: function(err){
				this.alert.show({type: 'warning', message: 'Unknown authentication error.'});
			}.bind(this)
		});
	}
	checkAuthCode(code){
		if(!code){
			location.reload();
			return;
		}

		this.spinner.stop(true);

		switch(code){
			case 1:
				this.alert.show({type: 'error', message: 'Invalid username or password.'});
			break;
			default:
				this.alert.show({type: 'error', message: 'An unknown error has occured.'});
			break;
		}
	}
	setModel(){
		this.model = kendo.observable({
			username: '',
			password: '',
			showTou: Conf.termsOfUse.show,
			agree: Conf.termsOfUse.show ? false : true,
			authenticate: function(){ this.authenticate(); }.bind(this)
		});
	}
	setShortcuts(){
		shortcutKeys.addProfile('authentication', [
			{key: 'enter', bind: this, func: 'handleShortcut'},
			{key: 'space', bind: this, func: 'handleShortcut'}
		]);

		shortcutKeys.activate('authentication');
	}
	handleShortcut(obj){
		if(obj.evt.target.type === 'checkbox'){ return; }

		obj.evt.target.blur();

		switch(obj.key){
			case 'enter':
			case 'space':
				this.authenticate();
			break;
		}
	}
	setView(){
		let tou = Conf.termsOfUse;
		let visible = tou.show ? 'visible' : 'none';
		let content = [
			{type: 'spinner', opts: {key: 'spinner', animateIn: true, animateOut: true, active: false}},
			{type: 'panelset', opts: {flex: 'auto', justify: 'center', align: 'center', items: [
				{items: [
					{style: 'auth-logo'},
					{justify: 'center', padding: '0 10px', items:[
						{text: Conf.title, style: 'auth-title'},
						{text: Conf.description, style: 'auth-desc'}
					]},
				]},
				{fontSize: '1.3em', marginTop: 20, content: [
					{type: 'inputText', opts: {key: 'username', placeholder: 'Username', width: 200}},
					{type: 'inputPassword', opts: {key: 'password', placeholder: 'Password', width: 200}},
					{type: 'inputButton', opts: {text: 'Login', primary: true, enabled: 'agree', click: 'authenticate'}}
				]},
				{fontSize: '1.3em', marginTop: 10, visible: 'showTou', content: [
					{type: 'inputCheckbox', opts: {key: 'agree', desc: tou.agreeText}}
				]},
				{content: [
					{type: 'notification', opts: {key: 'alert', single: true}}
				]},
				{items: [
					{key: 'tou', align: 'center', maxWidth: 700}
				]}
			]}
		}];

		this.view = new View({
			model: this.model,
			container: 'body',
			content: content
		});

		this.alert = this.view.notifications.alert;
		this.spinner = this.view.spinners.spinner;

		this.setTermsOfUse();
		kendo.bind('body', this.model);
	}
	setTermsOfUse(){
		let terms = Conf.termsOfUse.terms;
		let panel = this.view.panels.tou;

		for(let item of terms){
			Dom.el('div', {text: item.text, class: 'auth-tou'}, panel);
		}
	}
}

$(document).ready(function(){
	const main = new Main();
});
