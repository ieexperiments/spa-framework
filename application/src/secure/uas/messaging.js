/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class UASMessaging{
	constructor(layers){
		this.layers = layers;
	}
	stream(msg){
		this.refreshLayers();
		this.dialog(msg);
	}
	refreshLayers(){
		Utils.ajax({
			route: 'getUserSchedules',
			success: function(data){
				this.layers.setData(data);
			}.bind(this)
		});
	}
	dialog(msg){
		let index = {
			pending: 'Pending',
			approved: 'Approved',
			denied: 'Denied',
			conditional: 'Conditional'
		};
		let id = 'FAA-UAS-' + msg.id.substr(0, 8);
		let status = index[msg.status];
		let dialog = Dom.el('div', {}, 'body');
		let line = 'UAS Schedule: {1} status was updated to {2}'.replace('{1}', id).replace('{2}', status);
		let content = this.getContent(msg, line);

		dialog.kendoDialog({
			title: 'UAS Schedule Alert',
			closable: false,
			modal: true,
			content: content,
			actions: [
				{text: 'View on map', action: function(){ this.layers.gotoFeature(msg.id); }.bind(this)},
				{text: 'View schedules', action: function(){ uasSchedules.show(); }.bind(this)},
				{text: 'Close', primary: true}
			],
			close: function(){}
		});
	}
	getContent(msg, line){
		console.log(msg);
		if(!msg.warnings){ return line; }

		let w = msg.warnings;
		let airspace = w.classb.concat(w.classc).concat(w.classd);
		let warnings = w.obstructions.length;
		let wrapper = Dom.el('div', {});

		Dom.el('div', {text: line}, wrapper);

		if(airspace.length){
			Dom.el('div', {margin: '10px 0 10px 0', bold: true, text: 'Airspace Conflicts'}, wrapper);

			for(let item of airspace){
				Dom.el('div', {text: item.NAME}, wrapper);
			}
		}

		if(warnings){
			Dom.el('div', {margin: '10px 0 0 0', bold: true, text: 'Warning: ' + warnings + ' obstructions are in this area.'}, wrapper);
		}

		return wrapper.html();
	}
}
