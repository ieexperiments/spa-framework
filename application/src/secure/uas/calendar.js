/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class UasCalendar{
	constructor(){
		this.visible = false;
	}
	show(){
		mainBar.register(this);
		this.getData();
	}
	hide(){
		mainBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	setModel(){
		this.model = kendo.observable({});
	}
	setView(){
		let options = {
			key: 'uas',
			date: new Date(),
			views: ['day', { type: 'week', selected: true }, 'workWeek', 'month'],
			dataSource: []
		};
		let content = [
			{type: 'panelset', opts: {key: 'container', flex: 'auto', items: [
				{border: true, text: 'UAS Calendar', style: 'k-header header'},
				{border: true, flex: 'auto', content: [
					{type: 'scheduler', opts: options}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			content: content,
			container: this.container
		});

		//this.grid = this.view.grids.schedules;
		this.scheduler = this.view.schedulers.uas;
		kendo.bind(this.view.panels.container, this.model);
	}
	getData(){
		Utils.ajax({
			route: 'uasCalendar',
			success: function(arr){
				let data = new kendo.data.SchedulerDataSource({data: arr});
				this.scheduler.setDataSource(data);
			}.bind(this)
		});
	}
}
