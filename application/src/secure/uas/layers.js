/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class UASLayers{
	constructor(editor){
		this.add();
		this.setData(userInfo.uas);
	}
	gotoFeature(id){
		let feature = this.geojson.getFeatures('id', id)[0];

		if(!feature){ return; }
		console.log(feature);
		let geom = Topology.reader.read(feature).geometry;
		geom.getEnvelope();
		let bbox = geom.envelope;

		mapAPI.fitBounds([[bbox.minx, bbox.miny],[bbox.maxx, bbox.maxy]]);
	}
	addFeature(feature){
		this.geojson.addFeatures(feature);
		mapAPI.setSourceData('uas', this.geojson.featureCollection());
	}
	setData(arr){
		this.geojson = new Geojson();
		let index = {
			pending: 1,
			approved: 2,
			denied: 3,
			conditional: 4
		};

		if(!arr || !arr.length){ return;}

		for(let item of arr){
			let feature = JSON.parse(item.geojson);
			feature.properties.status = index[item.status];
			feature.properties.id = item.id;
			this.geojson.addFeatures(feature);
		}

		mapAPI.setSourceData('uas', this.geojson.featureCollection());
	}
	add(){
		let layers = [
			{
				id: 'uasFill',
				type: 'fill',
				source: 'uas',
				fillColor: {property: 'status', stops: [[1, '#ef8b2c'], [2, '#a2b86c'], [3, '#c02e1d'], [4, '#0f5b78']]},
				fillOpacity: 0.2,
				filter: ['all', ['==', '$type', 'Polygon']],
			},
			{
				id: 'uasLine',
				type: 'line',
				source: 'uas',
				lineColor: {property: 'status', stops: [[1, '#ef8b2c'], [2, '#a2b86c'], [3, '#c02e1d'], [4, '#0f5b78']]},
				lineJoin: 'round',
				lineCap: 'round',
				lineWidth: 2
			},
			{
				id: 'uasPoint',
				type: 'circle',
				source: 'uas',
				circleColor: {property: 'status', stops: [[1, '#ef8b2c'], [2, '#a2b86c'], [3, '#c02e1d'], [4, '#0f5b78']]},
				filter: ['all', ['==', '$type', 'Point']]
			}
		];

		mapAPI.addSource('uas');

		for(let layer of layers){
			mapAPI.addLayer(layer);
		}
	}
}
