/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class UasSchedules{
	constructor(){
		this.visible = false;
	}
	show(admin){
		this.title = admin ? 'All Scheduled UAS Activity' : 'My Scheduled UAS Activity';
		this.route = admin ? 'uasAllGrid' : 'uasUserGrid';
		this.admin = admin;
		topBar.register(this);
		main.home();
		mapAPI.setLayout('1.0.0.0.a');
		mapAPI.disableEditor();
		this.fields = ['schedule.frequency','schedule.days','schedule.alt','schedule.altType','schedule.date1','schedule.date2','schedule.date3'];
		this.controls = ['schedule.status', 'schedule.comments'];
	}
	hide(){
		topBar.unregister();
	}
	toggle(admin){
		if(this.visible && admin === this.admin){
			this.hide();
		} else {
			this.show(admin);
		}
	}
	setModel(opts){
		this.model = kendo.observable({
			schedule: {
				frequency: 'single',
				days: [false, false, false, false, false, false, false],
				alt: [0, 400],
				altType: 'agl'
			},
			readonly: true,
			show1: true,
			show2: false,
			show3: false,
			changeFreq: function(){ this.changeFreq(); }.bind(this),
			changeStatus: function(){ this.changeStatus(); }.bind(this),
			viewOnMap: function(){ this.viewOnMap(); }.bind(this)
		});
	}
	toolbarClick(evt){
		evt.target.blur();
		switch(evt.id){
			case 'save':
				this.save();
			break;
			case 'new':
				this.newAccount();
			break;
			case 'reset':
				this.reset();
			break;
		}
	}
	changeStatus(){
		let status = this.model.schedule.status;


		if(status === 'conditional'){
			this.view.enable(this.fields);
			console.log(this.model.status);
		} else {
			this.view.disable(this.fields);
			this.reset();
			this.model.set('schedule.status', status);
		}

		kendo.bind(this.view.panels.container, this.model);
	}
	viewOnMap(){
		let geojson = this.model.get('schedule.geojson');
		if(!geojson){ return; }
		let geom = Topology.reader.read(JSON.parse(geojson)).geometry;
		geom.getEnvelope();
		let bbox = geom.envelope;

		mapAPI.fitBounds([[bbox.minx, bbox.miny],[bbox.maxx, bbox.maxy]]);
	}
	changeFreq(){
		let single = this.model.schedule.frequency === 'single' ? true : false;

		this.model.set('show1', single ? true : false);
		this.model.set('show2', single ? false : true);
	}
	enable(){
		this.view.enable(this.controls);
		this.model.set('show3', true);
		kendo.bind(this.view.panels.container, this.model);
	}
	save(){
		let data = this.view.data().schedule;

		this.backup = {
			frequency: 'single',
			days: [false, false, false, false, false, false, false],
			alt: [0, 400],
			altType: 'agl'
		};

		this.view.disable(this.fields.concat(this.controls));
		this.model.set('show3', false);
		this.reset();

		data.isAdmin = this.admin;

		Utils.ajax({
			route: 'uasUpdate',
			data: data,
			success: function(data){
				this.grid.read();
			}.bind(this)
		});
	}
	reset(){
		if(!this.backup){ return; }
		this.model.set('schedule', this.backup);
		this.changeFreq();
	}
	setView(opts){
		let toolbar = {
			key: 'toolbar',
			margin: '0 10px',
			click: function(evt){ this.toolbarClick(evt); }.bind(this),
			toggle: function(evt){ this.toolbarClick(evt); }.bind(this),
			items: [
				{type: 'button', text: 'Save', spriteCssClass: 'icon-floppy-disk', id: 'save'},
				{type: 'button', text: 'Reset', spriteCssClass: 'icon-undo', id: 'reset'}
			]
		};
		let freq = [
			{name: 'One Time', value: 'single'},
			{name: 'Recurring', value: 'multi'}
		];
		let days = [
			{name: 'Monday', value: 'mon'},
			{name: 'Tuesday', value: 'tue'},
			{name: 'Wednesday', value: 'wed'},
			{name: 'Thursday', value: 'thu'},
			{name: 'Friday', value: 'fri'},
			{name: 'Saturday', value: 'sat'},
			{name: 'Sunday', value: 'sun'}
		];
		let alts = [
			{name: 'AGL', value: 'agl'},
			{name: 'MSL', value: 'msl'}
		];
		let status = [
			{name: 'Pending', value: 'pending'},
			{name: 'Acknowleged', value: 'approved'},
			{name: 'Conditional', value: 'conditional'},
			{name: 'Denied', value: 'denied'}
		];
		let content = [
			//{type: 'spinner', opts: {key: 'spinner', active: true}},
				{type: 'panelset', opts: {key: 'container', flex: 'auto', items: [
				{border: true, text: this.title, style: 'k-header header'},
				{border: true, align: 'center', bind: 'visible:show3', content: [
					{type: 'toolbar', opts: toolbar},
					{type: 'notification', opts: {key: 'notifier', single: true, autoHideAfter: 0}}
				]},
				{border: false, items: [
					{border: false, items: [
						{border: false, padding: '0 5px', content: [
							{type: 'inputText', opts: {label: 'User', enabled: false, key: 'schedule.user'}},
							{type: 'inputPhone', opts: {label: 'Phone', width: 130, enabled: false, key: 'schedule.phone'}},
							{type: 'inputText', opts: {label: 'UAS Certification', enabled: false, width: 130, key: 'schedule.uas_cert'}}
						]},
						{border: false, padding: '0 5px', content: [
							{type: 'inputRadiobuttonGroup', opts: {label: 'Frequency', items: freq, enabled: false, key: 'schedule.frequency', change: 'changeFreq'}}
						]},
						{border: false, padding: '0 5px', bind: 'visible:show1', content: [
							{type: 'inputDateTimeRange', opts: {key: 'schedule.date1', enabled: false, label: 'Start and End'}}
						]},
						{border: false, padding: '0 5px', bind: 'visible:show2', content: [
							{type: 'inputDateRange', opts: {key: 'schedule.date2', enabled: false, label: 'Date Range'}}
						]},
						{border: false, padding: '0 5px', bind: 'visible:show2', content: [
							{type: 'inputTimeRange', opts: {key: 'schedule.date3', enabled: false, label: 'Time Range'}}
						]},
						{border: false, padding: '0 5px', bind: 'visible:show2', content: [
							{type: 'inputCheckboxGroup', opts: {label: 'Days of Week', enabled: false, items: days, key: 'schedule.days'}}
						]},
						{border: false, padding: '0 5px', content: [
							{type: 'inputNumericRange', opts: {key: 'schedule.alt', enabled: false, width: 80, label: 'Altitude', min: 0, max: 400, step: 5, minDiff: 5, format: '###', decimals: 0}},
							{type: 'inputRadiobuttonGroup', opts: {items: alts, enabled: false, key: 'schedule.altType'}}
						]}
					]},
					{border: true, flex: 'auto', items: [
						{border: false, padding: '0 5px', content: [
							{type: 'inputDropdown', opts: {label: 'Status', enabled: false, width: 250, key: 'schedule.status', change: 'changeStatus', dataSource: status}},
							{type: 'inputButton', opts: {text: 'View on Map', primary: true, click: 'viewOnMap'}}
						]},
						{border: false, padding: '0 5px', content: [
							{type: 'inputTextarea', opts: {label: 'Comments', enabled: false, minHeight: 100, key: 'schedule.comments'}}
						]}
					]}
				]},
				{border: true, text: 'Schedules', style: 'k-header section'},
				{border: true, height: 200, key: 'grid', content: [
					{type: 'grid', opts: this.gridOptions()}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			content: content,
			container: this.container
		});

		this.notifier = this.view.notifications.notifier;
		this.grid = this.view.grids.schedules;
		kendo.bind(this.view.panels.container, this.model);
	}
	gridData(options){
		this.gridOpts = options;

		Utils.ajax({
			route: this.route,
			data: options.data,
			success: function(data){
				this.processData(data);

				options.success({
					data: data.results,
					total: data.total
				});
			}.bind(this),
			error: function(err){
				options.error();
			}
		});
	}
	processData(data){
		let arr = [];

		for(let item of data.results){
			item.uuid = item.id;
			item.load_dt = new Date(item.load_dt);
			item.start1 = item.start1 ? new Date(item.start1) : item.start1;
			item.start2 = item.start2 ? new Date(item.start2) : item.start2;
			item.start3 = item.start3 ? new Date(item.start3) : item.start3;
			item.end1 = item.end1 ? new Date(item.end1) : item.end1;
			item.end2 = item.end2 ? new Date(item.end2) : item.end2;
			item.end3 = item.end3 ? new Date(item.end3) : item.end3;
			item.days = item.days.split(',');
			item.start = item.start1 || item.start2;
			item.end = item.end1 || item.end2;
			item.alt = [item.alt1, item.alt2];
			item.date1 = [item.start1, item.end1];
			item.date2 = [item.start2, item.end2];
			item.date3 = [item.start3, item.end3];

			for(let [idx, str] of item.days.entries()){
				item.days[idx] = str === 'true' ? true : false;
			}
		}
	}
	gridSelect(obj){
		this.backup = obj.dataItem.toJSON();
		this.model.set('schedule',  this.backup);
		this.changeFreq();
		this.enable();
		//Utils.modelBulkSet(this.model, this.backup);
	}
	gridOptions(){
		let options = {
			key: 'schedules',
			autoBind: true,
			autoFit: false,
			selectable: 'row',
			scrollable: true,
			reorderable: true,
			editable: false,
			sortable: {mode: 'multiple', allowUnsort: true},
			transport: function(options){ this.gridData(options); }.bind(this),
			paging: {
				size: 100,
				dropdown: true,
				prevNext: true,
				numeric: true,
				buttonCount: false,
				input: true,
				refresh: true,
				info: true
			},
			columns: [
				{title: 'Created', type: 'date', field: 'load_dt', width: 100},
				{title: 'Status', type: 'string', field: 'status', width: 100},
				{title: 'User', type: 'string', field: 'user', width: 100},
				{title: 'Phone', type: 'string', field: 'phone', width: 100},
				{title: 'UAS Cert', type: 'string', field: 'uas_cert', width: 100},
				{title: 'Start Date', type: 'date', field: 'start', width: 200},
				{title: 'End Date', type: 'date', field: 'end', width: 200},
				{title: 'Low Alt', type: 'string', field: 'alt1', width: 200},
				{title: 'High Alt', type: 'string', field: 'alt2', width: 140},
				{title: 'Alt Type', type: 'string', field: 'alt_type', width: 80}
			],
			onSelect: function(obj){ this.gridSelect(obj); }.bind(this)
		};

		return options;
	}
}
