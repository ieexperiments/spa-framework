/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class UasScheduler{
	constructor(){
		this.visible = false;
		this.layers = new UASLayers();
		this.messaging = new UASMessaging(this.layers);
	}
	show(){
		this.minDate = moment().add(1, 'hours').minutes(0).seconds(0).toDate();
		this.maxDate = moment().add(90, 'days').minutes(0).seconds(0).toDate();
		topBar.register(this);
		this.setInstructions();
		main.home();
		mapAPI.setLayout('1.0.0.0.a');
		mapAPI.disableEditor();

		let date1 = moment().add(1, 'day').hours(9).minutes(0);
		let date2 = date1.clone().add(1, 'hour');

		date1 = date1.toDate();
		date2 = date2.toDate();

		this.model.set('date1', [date1, date2]);
		this.model.set('date2', [date1, date2]);
		this.model.set('date3', [date1, date2]);
	}
	hide(){
		topBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	setModel(){
		let time1 = moment().add(1, 'day').hours(0).minutes(0);
		let time2 = time1.clone().add(1, 'hour');

		this.model = kendo.observable({
			uuid: Utils.uuid(),
			user: userInfo.account.fullName,
			phone: userInfo.account.phone,
			cert: userInfo.account.uas_cert,
			freq: 'single',
			days: [false, false, false, false, false, false, false],
			alt: [0, 400],
			altType: 'agl',
			date1: [time1.toDate(), time2.toDate()],
			date2: [time1.toDate(), time2.toDate()],
			date3: [time1.toDate(), time2.toDate()],
			show1: true,
			show2: false,
			show3: true,
			step1: true,
			step2: false,
			tb1: true,
			tb2: false,
			changeFreq: function(){ this.changeFreq(); }.bind(this),
			gtype: 'pointbuffer',
			radius: 400,
			units: 'ft',
			showBuffer: true,
			changeGtype: function(){ this.changeGtype(); }.bind(this)
		});

		this.backup = this.model.toJSON();
	}
	toolbarClick(evt){
		let action = evt.id;
		evt.target.blur();
		let valid = action === 'next' ? this.validate() : true;
		if(!valid){ return; }

		this.hideNotifications();

		switch(action){
			case 'next':
			case 'prev':
				this.model.set('step1', action === 'next' ? false : true);
				this.model.set('step2', action === 'next' ? true : false);
			break;
			case 'reset':
				this.reset();
			break;
			case 'close':
				this.hide();
			break;
			case 'draw':
				this.draw();
			break;
			case 'redo':
				this.redo();
			break;
		}
	}
	hideNotifications(){
		this.notifier1.hide();
		this.notifier2.hide();
		this.notifier3.hide();
	}
	validate(){
		let data = this.model.toJSON();
		let fields = [];
		if(!data.user){ fields.push('User'); }
		if(!data.phone){ fields.push('Phone'); }
		if(!data.cert){ fields.push('UAS Certification'); }
		if(Utils.isNullOrUndefined(data.alt[0]) || Utils.isNullOrUndefined(data.alt[0])){ fields.push('Altitudes'); }

		if(data.freq === 'single'){
			if(!data.date1[0] || !data.date1[1]){ fields.push('Start and End'); }
		} else {
			if(!data.date2[0] || !data.date2[1]){ fields.push('Date Range'); }
			if(!data.date3[0] || !data.date3[1]){ fields.push('Time Range'); }
			let checked = false;
			for(let bool of data.days){
				checked = bool || checked;
			}
			if(!checked){ fields.push('Days of Week'); }
		}

		if(fields.length){
			let msg = 'The following fields are required: ' + fields.join(', ');
			this.notifier1.show({type: 'warning', message: msg});
			this.notifier3.show({type: 'warning', message: msg});
		}
		return fields.length ? false : true;
	}
	draw(){
		let data = this.view.data();

		mapAPI.draw({
			type: data.gtype,
			single: true,
			radius: data.radius,
			units: data.units,
			measure: false,
			onComplete: function(geom){
				this.complete(geom);
			}.bind(this)
		});
	}
	complete(geom){
		if(!geom){
			let msg = 'The geometry is invalid. Please try again.';
			this.notifier2.show({type: 'warning', message: msg});
			return;
		}

		this.geom = geom.features[0];
		this.geom.properties.status = 'pending';
		this.geom.properties.id = this.model.get('uuid');
		this.layers.addFeature(this.geom);
		this.jsts = Topology.read(this.geom);
		this.checkDates();
	}
	redo(){
		if(!this.validate()){ return; }
		this.checkDates();
	}
	submit(){
		let data = this.view.data();
		data.geom = this.geom;
		data.bbox = Topology.bbox(this.geom);
		data.uuid = Utils.uuid();

		Utils.ajax({
			route: 'uasSubmit',
			data: data,
			success: function(features){
				this.layers.setData(features);
				this.hide();
				this.dialog(data);
			}.bind(this)
		});
	}
	dialog(data){
		let id = 'FAA-UAS-' + data.uuid.substr(0, 8);
		let dialog = Dom.el('div', {}, 'body');
		let line = 'UAS Schedule: {1} has been received by the FAA'.replace('{1}', id);

		dialog.kendoDialog({
			title: 'UAS Schedule Received',
			closable: false,
			modal: true,
			content: line,
			actions: [
				{text: 'View schedules', action: function(){ uasSchedules.show(); }.bind(this)},
				{text: 'Close', primary: true}
			],
			close: function(){}
		});
	}
	checkDates(){
		let data = this.model.toJSON();
		let daytime = false;

		if(data.freq === 'single'){
			let date1 = moment(data.date1[0]);
			let date2 = moment(data.date1[1]);
			daytime = this.checkDaytime(date1, date2);
		}

		if(daytime.invalid){
			console.log('test');
			Utils.modelBulkSet(this.model, {step1: true, step2: false, tb1: false, tb2: true});
			let msg = 'Daylight Operations Only. Times must be between ' + daytime.times + '.';
			this.notifier3.show({type: 'warning', message: msg});
		} else {
			this.submit();
		}
	}
	checkDaytime(dt1, dt2){
			let point = this.jsts.geometry.getCentroid().getCoordinates()[0];
			let daytime = SunCalc.getTimes(dt1, point.y, point.x);
			let min = moment(daytime.sunrise).add(30, 'minutes');
			let max = moment(daytime.sunset).subtract(30, 'minutes');
			let bool1 = moment(dt1).isBetween(min, max);
			let bool2 = moment(dt2).isBetween(min, max);
			let invalid = (bool1 && bool2) ? false : true;

			return {invalid: invalid, times: (min.format('LT') + ' and ' + max.format('LT'))};
	}
	reset(){
		Utils.modelBulkSet(this.model, this.backup);
	}
	changeGtype(){
		let gtype = this.model.gtype;
		this.setInstructions();
		this.model.set('show3', (gtype === 'pointbuffer' || gtype === 'linebuffer') ? true : false);
	}
	changeFreq(){
		let single = this.model.freq === 'single' ? true : false;

		this.model.set('show1', single ? true : false);
		this.model.set('show2', single ? false : true);
	}
	setInstructions(){
		let panel = this.view.panels.instructions;
		let nav = [
				'Navigate to the location of the activity on the map.',
				'Click the "Begin Drawing" button."'
		];
		let gtypes = {
			pointbuffer: [
				'Set the radius and units for the circle.',
				'Click on the map at the center point.'
			],
			polygon: [
				'Click on the map to begin drawing.',
				'Move mouse to next point.',
				'Click on the map to start a new segment.',
				'Double click on the map to finish.'
			],
			linestring: [
				'Click on the map to begin drawing.',
				'Move mouse to next point.',
				'Click on the map to start a new segment.',
				'Double click on the map to finish.'
			],
			linebuffer: [
				'Set the radius and units for the circle.',
				'Click on the map to begin drawing.',
				'Move mouse to next point.',
				'Click on the map to start a new segment.',
				'Double click on the map to finish.'
			]
		};

		panel.empty();

		let lines = nav.concat(gtypes[this.model.gtype]);
		for(let [idx, line] of lines.entries()){
			Dom.el('div', {text: (idx + 1) + '. ' + line, lineHeight: '1.5em'}, panel);
		}
	}
	setView(){
		let toolbar1 = {
			key: 'toolbar1',
			margin: '0 10px',
			click: function(evt){ this.toolbarClick(evt); }.bind(this),
			toggle: function(evt){ this.toolbarClick(evt); }.bind(this),
			items: [
				{type: 'button', text: 'Set Location', spriteCssClass: 'icon-pin-map-down-7', id: 'next'},
				{type: 'button', text: 'Reset', spriteCssClass: 'icon-undo', id: 'reset'},
				{type: 'button', text: 'Close', spriteCssClass: 'icon-door-out-7', id: 'close'}
			]
		};
		let toolbar2 = {
			key: 'toolbar2',
			margin: '0 10px',
			click: function(evt){ this.toolbarClick(evt); }.bind(this),
			toggle: function(evt){ this.toolbarClick(evt); }.bind(this),
			items: [
				{type: 'button', text: 'Begin Drawing', spriteCssClass: 'icon-edit', id: 'draw'},
				{type: 'button', text: 'Review Schedule', spriteCssClass: 'icon-calendar3', id: 'prev'},
				{type: 'button', text: 'Close', spriteCssClass: 'icon-door-out-7', id: 'close'}
			]
		};
		let toolbar3 = {
			key: 'toolbar2',
			margin: '0 10px',
			click: function(evt){ this.toolbarClick(evt); }.bind(this),
			toggle: function(evt){ this.toolbarClick(evt); }.bind(this),
			items: [
				{type: 'button', text: 'Resubmit', spriteCssClass: 'icon-redo', id: 'redo'},
				{type: 'button', text: 'Close', spriteCssClass: 'icon-door-out-7', id: 'close'}
			]
		};
		let freq = [
			{name: 'One Time', value: 'single'},
			{name: 'Recurring', value: 'multi'}
		];
		let days = [
			{name: 'Monday', value: 'mon'},
			{name: 'Tuesday', value: 'tue'},
			{name: 'Wednesday', value: 'wed'},
			{name: 'Thursday', value: 'thu'},
			{name: 'Friday', value: 'fri'},
			{name: 'Saturday', value: 'sat'},
			{name: 'Sunday', value: 'sun'}
		];
		let alts = [
			{name: 'AGL', value: 'agl'},
			{name: 'MSL', value: 'msl'}
		];
		let units = [
			{name: 'Miles', value: 'mi'},
			{name: 'Nautical Miles', value: 'nm'},
			{name: 'Kilometers', value: 'km'},
			{name: 'Feet', value: 'ft'},
			{name: 'Meters', value: 'm'}
		];
		let geoms = [
			{name: 'Circle', value: 'pointbuffer'},
			{name: 'Polygon', value: 'polygon'},
			{name: 'Linestring', value: 'linestring'},
			{name: 'Buffered Linestring', value: 'linebuffer'}
		];
		let content = [
			{type: 'panelset', opts: {key: 'container1', bind: 'visible:step1', flex: 'auto', items: [
				{border: true, text: 'Schedule UAS Activity', style: 'k-header header'},
				{border: true, align: 'center', bind: 'visible:tb1', content: [
					{type: 'toolbar', opts: toolbar1},
					{type: 'notification', opts: {key: 'notifier1', single: true, autoHideAfter: 0}}
				]},
				{border: true, align: 'center', bind: 'visible:tb2', content: [
					{type: 'toolbar', opts: toolbar3},
					{type: 'notification', opts: {key: 'notifier3', single: true, autoHideAfter: 0}}
				]},
				{border: false, padding: '0 5px', content: [
					{type: 'inputText', opts: {label: 'User', key: 'user'}},
					{type: 'inputPhone', opts: {label: 'Phone', width: 130, key: 'phone'}},
					{type: 'inputText', opts: {label: 'UAS Certification', width: 130, key: 'cert'}}
				]},
				{border: false, padding: '0 5px', content: [
					{type: 'inputRadiobuttonGroup', opts: {label: 'Frequency', items: freq, key: 'freq', change: 'changeFreq'}}
				]},
				{border: false, padding: '0 5px', bind: 'visible:show1', content: [
					{type: 'inputDateTimeRange', opts: {key: 'date1', label: 'Start and End', min: this.minDate, max: this.maxDate}}
				]},
				{border: false, padding: '0 5px', bind: 'visible:show2', content: [
					{type: 'inputDateRange', opts: {key: 'date2', label: 'Date Range', min: this.minDate, max: this.maxDate}}
				]},
				{border: false, padding: '0 5px', bind: 'visible:show2', content: [
					{type: 'inputTimeRange', opts: {key: 'date3', label: 'Time Range', min: this.minDate, max: this.maxDate}}
				]},
				{border: false, padding: '0 5px', bind: 'visible:show2', content: [
					{type: 'inputCheckboxGroup', opts: {label: 'Days of Week', items: days, key: 'days'}}
				]},
				{border: false, padding: '0 5px', content: [
					{type: 'inputNumericRange', opts: {key: 'alt', width: 80, label: 'Altitude', min: 0, max: 400, step: 5, minDiff: 5, format: '###', decimals: 0}},
					{type: 'inputRadiobuttonGroup', opts: {items: alts, key: 'altType'}}
				]}
			]}},
			{type: 'panelset', opts: {key: 'container2', bind: 'visible:step2', flex: 'auto', items: [
				{border: true, text: 'Set UAS Activity Location', style: 'k-header header'},
				{border: true, align: 'center', content: [
					{type: 'toolbar', opts: toolbar2},
					{type: 'notification', opts: {key: 'notifier2', single: true, autoHideAfter: 0}}
				]},
				{border: false, padding: '0 5px', content: [
					{type: 'inputRadiobuttonGroup', opts: {label: 'Geometry Type', items: geoms, key: 'gtype', change: 'changeGtype'}}
				]},
				{border: false, padding: '0 5px', bind: 'visible:show3', content: [
					{type: 'inputNumeric', opts: {key: 'radius', width: 80, label: 'Buffer Radius', min: 0.1, max: 9999, step: 0.1, format: '####'}},
					{type: 'inputDropdown', opts: {label: 'Units', width: 150, key: 'units', dataSource: units}},
				]},
				{border: true, text: 'Drawing Instructions', style: 'k-header section'},
				{border: false, padding: 10, items:[
					{border: false, key: 'instructions'}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			container: this.container,
			content: content
		});
		this.notifier1 = this.view.notifications.notifier1;
		this.notifier2 = this.view.notifications.notifier2;
		this.notifier3 = this.view.notifications.notifier3;

		kendo.bind(this.view.panels.container1, this.model);
		kendo.bind(this.view.panels.container2, this.model);
	}
}
