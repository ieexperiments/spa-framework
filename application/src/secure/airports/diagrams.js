/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class AirportDiagrams{
	constructor(){
		this.visible = false;
	}
	show(){
		rightBar.register(this);
	}
	hide(){
		rightBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	setModel(){
		this.model = kendo.observable({});
	}
	setView(){
		let search = {
			key: 'global',
			placeholder: 'Search Airports',
			ajax: 'arptDiagramSearch',
			height: 500,
			width: 800,
			onSelect: function(result){ this.loadDiagram(result); }.bind(this)
		};
		let content = [
			{type: 'panelset', opts: {key: 'container', flex: 'auto', items: [
				{border: true, text: 'Airport Diagrams', style: 'k-header header'},
				{padding: 10, border: true, content: [
					{type: 'autocomplete', opts: search}
				]},
				{flex: 'auto', content: [
					{type: 'iframe', opts: {key: 'diagram'}}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			content: content,
			container: this.container
		});

		this.iframe = this.view.iframes.diagram;

		kendo.bind(this.view.panels.container, this.model);
	}
	loadDiagram(id){
		let url = 'ajax?route=arptDiagramGet&faa=' + id;

		this.iframe.setSrc(url);
	}
}
