/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class PirepLayers{
	constructor(){
		//this.geojson = new Geojson();
		mapAPI.addSource('pirep');
		this.createLayers();
		this.listener();
	}
	createLayers(){
		let layers = [
			{
				id: 'pirep-1',
				type: 'symbol',
				source: 'pirep',
				minzoom: 0,
				maxzoom: 22,
				textFont: ['pirep'],
				symbolPlacement: 'point',
				textField: '{chr}',
				textSize: 20,
				textAllowOverlap: true,
				textIgnorePlacement: true,
				textAnchor: 'center',
				textJustify: 'center',
				textColor: '#a2b86c',
				textHaloColor: 'rgba(0, 0, 0, 0.3)',
				textHaloWidth: 1,
				textOffset: [0, 0.2],
				filter: [
					'all',
					['==', '$type', 'Point'],
					['==', 'severity', 1]
				]
			},
			{
				id: 'pirep-2',
				type: 'symbol',
				source: 'pirep',
				minzoom: 0,
				maxzoom: 22,
				textFont: ['pirep'],
				symbolPlacement: 'point',
				textField: '{chr}',
				textSize: 20,
				textAllowOverlap: true,
				textIgnorePlacement: true,
				textAnchor: 'center',
				textJustify: 'center',
				textColor: '#ef8b2c',
				textHaloColor: 'rgba(0, 0, 0, 0.3)',
				textHaloWidth: 1,
				textOffset: [0, 0.2],
				filter: [
					'all',
					['==', '$type', 'Point'],
					['==', 'severity', 2]
				]
			},
			{
				id: 'pirep-3',
				type: 'symbol',
				source: 'pirep',
				minzoom: 0,
				maxzoom: 22,
				textFont: ['pirep'],
				symbolPlacement: 'point',
				textField: '{chr}',
				textSize: 20,
				textAllowOverlap: true,
				textIgnorePlacement: true,
				textAnchor: 'center',
				textJustify: 'center',
				textColor: '#c02e1d',
				textHaloColor: 'rgba(0, 0, 0, 0.3)',
				textHaloWidth: 1,
				textOffset: [0, 0.2],
				filter: [
					'all',
					['==', '$type', 'Point'],
					['==', 'severity', 3]
				]
			},
			{
				id: 'pirep-4',
				type: 'symbol',
				source: 'pirep',
				minzoom: 0,
				maxzoom: 22,
				textFont: ['DINOffcProRegularArialUnicodeMSRegular'],
				symbolPlacement: 'point',
				textField: '{alt}',
				textSize: 16,
				textAllowOverlap: true,
				textIgnorePlacement: true,
				textAnchor: 'center',
				textJustify: 'center',
				textColor: '#a2b86c',
				textHaloColor: 'rgba(0, 0, 0, 0.3)',
				textHaloWidth: 1,
				textOffset: [0, 1.2],
				filter: [
					'all',
					['==', '$type', 'Point'],
					['==', 'severity', 1]
				]
			},
			{
				id: 'pirep-5',
				type: 'symbol',
				source: 'pirep',
				minzoom: 0,
				maxzoom: 22,
				textFont: ['DINOffcProRegularArialUnicodeMSRegular'],
				symbolPlacement: 'point',
				textField: '{alt}',
				textSize: 16,
				textAllowOverlap: true,
				textIgnorePlacement: true,
				textAnchor: 'center',
				textJustify: 'center',
				textColor: '#ef8b2c',
				textHaloColor: 'rgba(0, 0, 0, 0.3)',
				textHaloWidth: 1,
				textOffset: [0, 1.2],
				filter: [
					'all',
					['==', '$type', 'Point'],
					['==', 'severity', 2]
				]
			},
			{
				id: 'pirep-6',
				type: 'symbol',
				source: 'pirep',
				minzoom: 0,
				maxzoom: 22,
				textFont: ['DINOffcProRegularArialUnicodeMSRegular'],
				symbolPlacement: 'point',
				textField: '{alt}',
				textSize: 16,
				textAllowOverlap: true,
				textIgnorePlacement: true,
				textAnchor: 'center',
				textJustify: 'center',
				textColor: '#c02e1d',
				textHaloColor: 'rgba(0, 0, 0, 0.3)',
				textHaloWidth: 1,
				textOffset: [0, 1.2],
				filter: [
					'all',
					['==', '$type', 'Point'],
					['==', 'severity', 3]
				]
			},
		];

		for(let layer of layers){
			mapAPI.addLayer(layer);
		}
	}
	setSource(geojson){
		this.geojson = geojson;
		mapAPI.setSourceData('pirep', geojson.featureCollection());
	}
	listener(){
		mapAPI.on('themechange', function(evt){
			this.setSource(this.geojson);
		}.bind(this));
	}
}
