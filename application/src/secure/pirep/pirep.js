/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Pirep{
	constructor(editor){
		this.visible = false;
		this.layers = new PirepLayers();
		this.getData();
	}
	show(){
		topBar.register(this);
		main.home();
		mapAPI.setLayout('1.0.0.0.a');
		mapAPI.disableEditor();

	}
	hide(){
		topBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	setModel(){
		this.model = kendo.observable({
			reported: new Date(),
			type: 'turbulence',
			severity: 'light',
			alt: 0,
			lat: undefined,
			lon: undefined,
			setFromMap: function(){ this.setPoint(); }.bind(this)
		});

		this.backup = this.model.toJSON();
	}
	toolbarClick(evt){
		let action = evt.id;
		evt.target.blur();
		//let valid = action === 'next' ? this.validate() : true;
		//if(!valid){ return; }

		//this.hideNotifications();

		switch(action){
			case 'submit':
				this.submit();
			break;
			case 'reset':
				this.reset();
			break;
			case 'close':
				this.hide();
			break;
		}
	}
	reset(){
		Utils.modelBulkSet(this.model, this.backup);
	}
	submit(){
		let data = this.view.data();
		data.uuid = Utils.uuid();

		Utils.ajax({
			route: 'pirepSubmit',
			data: data,
			success: function(resp){
				this.getData();
				//this.hide();
				this.reset();
				this.dialog(data);
			}.bind(this)
		});
	}
	dialog(data){
		let id = 'FAA-PIREP-' + data.uuid.substr(0, 8);
		let dialog = Dom.el('div', {}, 'body');
		let line = 'PIREP: {1} has been received by the FAA'.replace('{1}', id);

		dialog.kendoDialog({
			title: 'PIREP Received',
			closable: false,
			modal: true,
			content: line,
			actions: [
				{text: 'Close', primary: true}
			],
			close: function(){}
		});
	}
	setPoint(){
		mapAPI.draw({
			type: 'point',
			single: true,
			measure: false,
			onComplete: function(geom){
				let point = geom.features[0].geometry.coordinates;
				this.model.set('lon', point[0]);
				this.model.set('lat', point[1]);
			}.bind(this)
		});
	}
	setView(){
		let toolbar = {
			key: 'toolbar',
			margin: '0 10px',
			click: function(evt){ this.toolbarClick(evt); }.bind(this),
			toggle: function(evt){ this.toolbarClick(evt); }.bind(this),
			items: [
				{type: 'button', text: 'Submit', spriteCssClass: 'icon-pin-map-down-7', id: 'submit'},
				{type: 'button', text: 'Reset', spriteCssClass: 'icon-undo', id: 'reset'},
				{type: 'button', text: 'Close', spriteCssClass: 'icon-door-out-7', id: 'close'}
			]
		};
		let types = [
			{name: 'Turbulence', value: 'turbulence'},
			{name: 'Ice', value: 'ice'}
		];
		let severity = [
			{name: 'Light', value: 'light'},
			{name: 'Moderate', value: 'moderate'},
			{name: 'Severe', value: 'severe'}
		];
		let content = [
			{type: 'panelset', opts: {key: 'container', flex: 'auto', items: [
				{border: true, text: 'PIREP', style: 'k-header header'},
				{border: true, align: 'center', content: [
					{type: 'toolbar', opts: toolbar},
					{type: 'notification', opts: {key: 'notifier', single: true, autoHideAfter: 0}}
				]},
				{border: false, padding: '0 5px', content: [
					{type: 'inputDateTime', opts: {key: 'reported', label: 'Reported'}},
					{type: 'inputDropdown', opts: {label: 'Report Type', width: 150, key: 'type', dataSource: types}},
					{type: 'inputDropdown', opts: {label: 'Severity', width: 150, key: 'severity', dataSource: severity}}
				]},
				{border: false, padding: '0 5px', content: [
					{type: 'inputNumeric', opts: {label: 'Flight Level', key: 'alt', width: 80, min: 0, max: 999, format: '###'}},
					{type: 'inputNumeric', opts: {label: 'Latitude', key: 'lat', min: -90, max: 90, decimals: 6, format: 'n6'}},
					{type: 'inputNumeric', opts: {label: 'Longitude', key: 'lon', min: -180, max: 180, decimals: 6, format: 'n6'}},
					{type: 'inputButton', opts: {text: 'Set from Map Click', primary: true, click: 'setFromMap'}}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			content: content,
			container: this.container
		});

		kendo.bind(this.view.panels.container, this.model);
	}
	getData(){
		Utils.ajax({
			route: 'pirepAll',
			success: function(data){
				this.processData(data);
			}.bind(this)
		});
	}
	processData(data){
		let geojson = new Geojson();

		let index = {
			'light': 1,
			'moderate': 2,
			'severe': 3
		};

		for(let item of data){
			let point = [item.longitude, item.latitude];
			let properties = {
				chr: this.getSymbol(item),
				severity: index[item.severity],
				alt: item.altitude
			};

			geojson.point(point, properties);
		}

		this.layers.setSource(geojson);
	}
	getSymbol(item){
		let key = item.type + ':' + item.severity;
		let symbol = false;

		switch(key){
			case 'ice:light':
				symbol = 'B';
			break;
			case 'ice:moderate':
				symbol = 'C';
			break;
			case 'ice:severe':
				symbol = 'D';
			break;
			case 'turbulence:light':
				symbol = 'E';
			break;
			case 'turbulence:moderate':
				symbol = 'F';
			break;
			case 'turbulence:severe':
				symbol = 'G';
			break;
		}

		return symbol;
	}
}
