/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Navigation = {
	main: [
		{type: 'menu', items: [
			{text: 'Home', spriteCssClass: 'icon-home', action: 'home'},
			{text: 'File Manager', spriteCssClass: 'icon-file-folder-7', action: 'fileManager'},
			{text: 'Logout', spriteCssClass: 'icon-switch', action: 'logout'},
			//{text: 'My Profile', spriteCssClass: 'icon-profile', action: 'myProfile'},
			{text: 'Application Theme', spriteCssClass: 'icon-bucket-1', items: [
				{text: 'Black', action: 'theme:black'},
				{text: 'Blue Opal', action: 'theme:blueopal'},
				{text: 'Bootstrap', action: 'theme:bootstrap'},
				{text: 'Default', action: 'theme:default'},
				{text: 'Fiori', action: 'theme:fiori'},
				{text: 'Flat', action: 'theme:flat'},
				{text: 'High Contrast', action: 'theme:highcontrast'},
				{text: 'Material', action: 'theme:material'},
				{text: 'Material Black', action: 'theme:materialblack'},
				{text: 'Metro', action: 'theme:metro'},
				{text: 'Metro Black', action: 'theme:metroblack'},
				{text: 'Moonlight', action: 'theme:moonlight'},
				{text: 'Nova', action: 'theme:nova'},
				{text: 'Office 365', action: 'theme:office365'},
				{text: 'Silver', action: 'theme:silver'},
				{text: 'Uniform', action: 'theme:uniform'}
			]}
		]},
		{type: 'section', text: 'Airports'},
		{type: 'menu', items: [
			{text: 'Diagrams', spriteCssClass: 'icon-file-pdf', action: 'airportDiagrams'},
			{text: 'PIREP', spriteCssClass: 'icon-map-draw-message-7', action: 'pirepAdd'}
		]},
		{type: 'section', text: 'UAS'},
		{type: 'menu', items: [
			{text: 'Schedule Activity', spriteCssClass: 'icon-pin-map-plus-7', action: 'uasScheduler'},
			{text: 'My Schedules', spriteCssClass: 'icon-calendar3', action: 'uasMySchedules'},
			{text: 'Approver Schedules', spriteCssClass: 'icon-calendar3', action: 'uasAllSchedules'},
			{text: 'Activity Calendar', spriteCssClass: 'icon-calendar-7', action: 'uasCalendar'}
		]},
		{type: 'section', text: 'Tracker'},
		{type: 'menu', items: [
			{text: 'Toggle Feed', spriteCssClass: 'icon-traffic-light-7', action: 'trackerToggle'},
			{text: 'Filter Tracks', spriteCssClass: 'icon-filter', action: 'trackerFilters'},
			{text: 'Clear Selected', spriteCssClass: 'icon-airplane', action: 'trackClearDatablocks'},
			{text: 'Settings', spriteCssClass: 'icon-cogs', action: 'trackerSettings'}
		]},
		{type: 'section', text: 'Map'},
		{type: 'menu', items: [
			{text: 'Layers', spriteCssClass: 'icon-layer-7', action: 'mapLayers'},
			{text: 'Screenshot', spriteCssClass: 'icon-camera', action: 'mapScreenshot'},
			{text: 'Measure', spriteCssClass: 'icon-ruler-1', action: 'mapMeasure'},
			{text: 'Drawing', spriteCssClass: 'icon-edit', action: 'mapEditor'},
			{text: '3D Map', spriteCssClass: 'icon-globe', action: 'map3d'},
			{text: 'Map Theme', spriteCssClass: 'icon-bucket-1', items: [
				{text: 'Basic', action: 'mapTheme:basic'},
				{text: 'Bright', action: 'mapTheme:bright'},
				{text: 'Dark', action: 'mapTheme:dark'},
				{text: 'Emerald', action: 'mapTheme:emerald'},
				{text: 'Light', action: 'mapTheme:light'},
				{text: 'Streets', action: 'mapTheme:streets'}
			]},
			{text: 'Layout', spriteCssClass: 'icon-map-1011a', items: [
				{text: 'Full', spriteCssClass: 'icon-map-1000a', action: 'mapLayout:1.0.0.0.a'},
				{text: 'Preset 1', spriteCssClass: 'icon-map-1010a', action: 'mapLayout:1.0.1.0.a'},
				{text: 'Preset 2', spriteCssClass: 'icon-map-1100a', action: 'mapLayout:1.1.0.0.a'},
				{text: 'Preset 3', spriteCssClass: 'icon-map-1111a', action: 'mapLayout:1.1.1.1.a'},
				{text: 'Preset 4', spriteCssClass: 'icon-map-1011a', action: 'mapLayout:1.0.1.1.a'},
				{text: 'Preset 5', spriteCssClass: 'icon-map-1110a', action: 'mapLayout:1.1.1.0.a'},
				{text: 'Preset 6', spriteCssClass: 'icon-map-1011b', action: 'mapLayout:1.0.1.1.b'},
				{text: 'Preset 7', spriteCssClass: 'icon-map-1110b', action: 'mapLayout:1.1.1.0.b'}
			]}
		]},
		{type: 'section', text: 'System Administration', roles: ['admin']},
		{type: 'menu', roles: ['admin'], items: [
			{text: 'Account Management', spriteCssClass: 'icon-users', action: 'accountAdmin'},
			//{text: 'Password Policy', spriteCssClass: 'icon-key', action: 'passwordPolicy'}
		]}
	]
};
