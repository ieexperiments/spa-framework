/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class SideBar{
	constructor(panel){
		this.panel = panel;
		this.listener();
	}
	register(obj){
		if(this.instance){ this.unregister(); }
		this.instance = obj;
		this.panel.empty();
		main.model.set('panes.sidebar', true);
		obj.container = this.panel;
		obj.setModel();
		obj.setView();
		obj.visible = true;
		mapAPI.resize();
	}
	unregister(){
		this.panel.empty();
		main.model.set('panes.sidebar', false);
		this.instance.visible = false;
		this.instance.view = undefined;
		this.instance.model = undefined;
		this.instance = undefined;
		mapAPI.resize();
	}
	listener(){
		mapAPI.on('select', function(evt){
			if(this.instance && this.instance.mapSwitch){
				this.instance.mapSwitch(evt.instance);
			}
		}.bind(this));
	}
}
