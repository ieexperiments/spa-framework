/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Main{
	constructor(){
		window.main = this;
		this.setModel();
		this.setView();
		this.libVersions();

		Utils.ajax({
			route: 'clientData',
			success: function(data){
				this.startup(data);
			}.bind(this)
		});
	}
	startup(data){
		window.userInfo = data;
		window.mapAPI = new Map();
		this.socket();

		this.model.set('username', data.account.fullName + ' (' + data.account.username + ')');
	}
	mapReady(){
		this.spinner.stop();
		window.topBar = new TopBar(this.view.panels.topbar);
		window.mainBar = new MainBar(this.view.panels.main);
		window.sideBar = new SideBar(this.view.panels.sidebar);
		window.rightBar = new RightBar(this.view.panels.rightbar);
		window.tracker = new Tracker();
		window.map3d = new Map3d();
		window.accountAdmin = new AccountAdmin();
		window.uasScheduler = new UasScheduler();
		window.uasSchedules = new UasSchedules();
		window.uasCalendar = new UasCalendar();
		window.fileManager = new FileManager();
		window.airportDiagrams = new AirportDiagrams();
		window.pirep = new Pirep();
		window.sua = new SUA();
		/*
		mapAPI.editor({
			type: 'pointbuffer',
			single: false,
			radius: 100,
			units: 'mi',
			segments: 3,
			measure: false,
			onComplete: function(geom){},
			onPoint: function(point){}
		});
		*/
		mapAPI.on('mousemove', function(evt){
			this.setLocation(evt.coord);
		}.bind(this));
	}
	setLocation(coord){
		let location = Coordinate.deg2dms(coord);
		this.model.set('location', location.lon + ' ' + location.lat);
	}
	setModel(){
		this.model = kendo.observable({
			username: 'Username',
			location: 'Map Location',
			panes: {
				header: true,
				topbar: false,
				leftbar: true,
				rightbar: false,
				bottombar: false,
				sidebar: false
			}
		});
	}
	setView(){
		let tou = Conf.termsOfUse;
		let visible = tou.show ? 'visible' : 'none';
		let search = {
			key: 'global',
			fontSize: '1.2em',
			placeholder: 'Search',
			ajax: 'globalSearch',
			height: 500,
			width: 500,
			groupKey: 'group',
			onSelect: function(result){ mapAPI.flyTo(result, 11); }
		};
		let content = [
			{type: 'spinner', opts: {key: 'spinner', active: true}},
			{type: 'panelset', opts: {flex: 'auto', items: [
				{visible: 'panes.header', items: [
					{style: 'app-logo'},
					{justify: 'center', padding: '0 10px', items:[
						{key: 'headerTitle', text: Conf.title, style: 'app-title'},
						{key: 'headerDesc', text: Conf.description, style: 'app-desc'}
					]},
					{key: 'headerCenter', justify: 'center', align: 'end', margin: '0 20px 0 0', flex: 'auto', content: [
						{type: 'autocomplete', opts: search}
					]},
					{justify: 'center', items:[
						{key: 'username', bind: 'text:username', style: 'app-header-text'},
						{key: 'timestamp', style: 'app-header-text', content: [
							{type: 'clock', opts: {key: 'timestamp'}}
						]},
						{key: 'headerLocation', bind: 'text:location', style: 'app-header-text'}
					]}
				]},
				{border: true, flex: 'auto', items: [
					{key: 'leftbar', border: true, visible: 'panes.leftbar', content: [
						{type: 'panelmenu', opts: {config: Navigation.main}}
					]},
					{key: 'sidebar', border: true, visible: 'panes.sidebar'},
					{border: true, flex: 'auto', items:[
						{key: 'topbar', border: true, text: 'top', visible: 'panes.topbar'},
						{key: 'main', border: true, flex: 'auto'},
						{key: 'bottombar', border: true, visible: 'panes.bottombar'}
					]},
					{key: 'rightbar', border: true, visible: 'panes.rightbar', }
				], content: [
					{type: 'notification', opts: {key: 'notifier', style: 'notification-top-right', autoHideAfter: 25000, single: true}}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			container: 'body',
			content: content
		});

		this.notifier = this.view.notifications.notifier;
		this.spinner = this.view.spinners.spinner;
		this.setLocation([0, 0]);
		kendo.bind('body', this.model);
	}
	home(){
		mainBar.unregister(true);
		mapAPI.show();
	}
	logout(){
		this.spinner.start();

		Utils.ajax({
			route: 'logout',
			success: function(data){
				location.reload();
			},
			error: function(err){
				location.reload();
			}
		});
	}
	setTheme(theme){
		let path1 = 'lib/kendo/styles/kendo.{theme}.min.css';
		let path2 = 'lib/kendo/styles/kendo.{theme}.mobile.min.css';

		$('link')[1].href = path1.replace('{theme}', theme);
		$('link')[2].href = path2.replace('{theme}', theme);
	}
	socket(){
		window.ws = new Websocket({
			onMessage: function(msg){
				msg = JSON.parse(msg);

				switch(msg.route){
					case 'uas':
						uasScheduler.messaging.stream(msg);
					break;
				}
			}
		});
	}
	libVersions(){
		let versions = {
			jquery: jQuery.fn.jquery,
			mapbox: mapboxgl.version,
			jsts: jsts.version,
			moment: moment.version,
			kendo: kendo.version
		};

		console.log(versions);
	}
}

$(document).ready(function(){ new Main(); });
/*
$(window).on('unload', function(){
	setTimeout(function(){
		console.log('test');
		return true;
	}, 15000);
});
*/
