/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MainBar{
	constructor(panel){
		this.panel = panel;
		this.listener();
	}
	register(obj, map = false){
		if(this.instance){ this.unregister(true); }
		this.instance = obj;
		this.panel.empty();
		obj.container = this.panel;
		obj.setModel();
		obj.setView();
		obj.visible = true;
		mapAPI.resize();
	}
	unregister(replacement = false){
		if(!this.instance){ return; }
		this.panel.empty();
		this.instance.visible = false;
		this.instance.view = undefined;
		this.instance.model = undefined;
		this.instance = undefined;
		if(!replacement){ main.home(); }
		mapAPI.resize();
	}
	listener(){
		mapAPI.on('select', function(evt){
			if(this.instance && this.instance.mapSwitch){
				this.instance.mapSwitch(evt.instance);
			}
		}.bind(this));
	}
}
