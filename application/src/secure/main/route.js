/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Route = function(item){
	let data = item.dataItem;

	if(!data.action){ return; }

	let split = data.action.split(':');
	let type = split[0];
	let subtype = split[1];

	switch(type){
		case 'home':
			main.home();
		break;
		case 'logout':
			main.logout();
		break;
		case 'accountAdmin':
			accountAdmin.toggle();
		break;
		case 'adminRoles':
			new AdminRoles();
		break;
		case 'theme':
			main.setTheme(subtype);
		break;
		case 'myProfile':
			new UserProfile();
		break;
		case 'passwordPolicy':
			new PasswordPolicy();
		break;
		case 'addAccount':
			new AddAccount();
		break;
		case 'mapLayers':
			mapAPI.toggleLayerControl();
		break;
		case 'mapEditor':
			mapAPI.editor();
		break;
		case 'mapTheme':
			mapAPI.setTheme(subtype);
		break;
		case 'mapLayout':
			mapAPI.setLayout(subtype);
		break;
		case 'mapScreenshot':
			mapAPI.screenshot();
		break;
		case 'mapMeasure':
			mapAPI.measure();
		break;
		case 'map3d':
			map3d.toggle();
		break;
		case 'fileManager':
			fileManager.show();
		break;
		case 'trackerToggle':
			tracker.toggle();
		break;
		case 'trackerFilters':
			tracker.toggleFilters();
		break;
		case 'trackerSettings':
			tracker.toggleSettings();
		break;
		case 'trackClearDatablocks':
			tracker.clearDatablocks();
		break;
		case 'uasScheduler':
			uasScheduler.toggle();
		break;
		case 'uasMySchedules':
			uasSchedules.toggle();
		break;
		case 'uasAllSchedules':
			uasSchedules.toggle(true);
		break;
		case 'uasCalendar':
			uasCalendar.toggle();
		break;
		case 'airportDiagrams':
			airportDiagrams.toggle();
		break;
		case 'pirepAdd':
			pirep.toggle();
		break;
	}
};
