/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class AccountAdmin{
	constructor(){
		this.container = main.view.panels.main;
	}
	show(){
		mainBar.register(this);
	}
	hide(){
		mainBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	setModel(opts){
		this.model = kendo.observable({
			account: {acctId: Utils.uuid(), active: true, roles: []},
			filters: {},
			reloadGrid: function(){ this.gridData(this.gridOpts); },
			passwordGen: function(){ this.passwordGen(); },
			clearFilters: function(){ this.clearFilters(); },
			exportAccounts: function(){ this.exportAccounts(); }
		});
	}
	toolbarClick(evt){
		console.log(evt);
		evt.target.blur();
		switch(evt.id){
			case 'save':
				this.save();
			break;
			case 'new':
				this.newAccount();
			break;
			case 'reset':
				this.reset();
			break;
		}
	}
	save(){
		let data = this.model.account.toJSON();
		let invalid = this.validate(data);

		if(invalid){
			this.notifier.show({type: 'warning', message: invalid.msg});
			return;
		}

		Utils.ajax({
			route: 'accountMerge',
			data: data,
			success: function(data){
				this.notifier.show({type: 'success', message: 'Account has been saved.'});
				this.gridData(this.gridOpts);
				this.newAccount();
			}.bind(this),
			error: function(err){ console.log(err); }
		});
	}
	validate(data){
		if(!data.first_nm){ return {msg: 'First Name is required.'}; }
		if(!data.last_nm){ return {msg: 'Last Name is required.'}; }
		if(!data.title){ return {msg: 'Title is required.'}; }
		if(!data.username){ return {msg: 'Username is required.'}; }
		if(!data.email){ return {msg: 'Email is required.'}; }
		if(!Utils.validateEmail(data.email)){ return {msg: 'A valid email is required.'}; }
		if(!data.phone){ return {msg: 'Phone number is required.'}; }
		if(!data.department){ return {msg: 'Department is required.'}; }
		if(!data.roles.length){ return {msg: 'At least one user role is required.'}; }

		return false;
	}
	newAccount(){
		let account = {id: Utils.uuid(), active: true, roles: ['user']};
		this.model.set('account', account);
		this.backup = account;
	}
	reset(){
		if(!this.backup){ return; }
		this.model.set('account', this.backup);
	}
	setView(opts){
		let toolbar = {
			key: 'toolbar',
			margin: '0 10px',
			click: function(evt){ this.toolbarClick(evt); }.bind(this),
			toggle: function(evt){ this.toolbarClick(evt); }.bind(this),
			items: [
				{type: 'button', text: 'Save', spriteCssClass: 'icon-floppy-disk', id: 'save'},
				{type: 'button', text: 'New User', spriteCssClass: 'icon-user-plus', id: 'new'},
				{type: 'button', text: 'Reset', spriteCssClass: 'icon-undo', id: 'reset'}
			]
		};
		let roles = [
			{name: 'System Administator', value: 'admin'},
			{name: 'User', value: 'user'},
			{name: 'Manager', value: 'manager'}
		];
		let content = [
			//{type: 'spinner', opts: {key: 'spinner', active: true}},
				{type: 'panelset', opts: {key: 'wrapper', flex: 'auto', items: [
				{border: true, text: 'Account Management', style: 'k-header header'},
				{border: true, align: 'center', content: [
					{type: 'toolbar', opts: toolbar},
					{type: 'notification', opts: {key: 'notifier', single: true}}
				]},
				{border: true, items: [
					{border: true, padding: 5, content: [
						{type: 'inputHeader', opts: {type: 'section', text: 'User Info'}},
						{type: 'inputText', opts: {label: 'First Name', width: 250, key: 'account.first_nm'}},
						{type: 'inputText', opts: {label: 'Last Name', width: 250, key: 'account.last_nm'}},
						{type: 'inputText', opts: {label: 'Title', width: 250, key: 'account.title'}},
						{type: 'inputText', opts: {label: 'Username', width: 250, key: 'account.username'}},
						{type: 'inputEmail', opts: {label: 'Email', width: 250, key: 'account.email'}},
						{type: 'inputPhone', opts: {label: 'Phone', width: 250, key: 'account.phone'}}
					]},
					{border: true, padding: 5, content: [
						{type: 'inputHeader', opts: {text: 'Address'}},
						{type: 'inputText', opts: {label: 'Address 1', width: 250, key: 'account.addr1'}},
						{type: 'inputText', opts: {label: 'Address 2', width: 250, key: 'account.addr2'}},
						{type: 'inputText', opts: {label: 'City', width: 250, key: 'account.city'}},
						{type: 'inputDropdown', opts: {label: 'State', width: 250, key: 'account.state', dataSource: Data.states}},
						{type: 'inputText', opts: {label: 'Zip', width: 250, key: 'account.zip'}}
					]},
					{border: false, padding: 5, content: [
						{type: 'inputHeader', opts: {text: 'Settings'}},
						{type: 'panelset', opts: {dir: 'row', content: [
							{type: 'inputText', opts: {label: 'Password', key: 'account.password'}},
							{type: 'inputButton', opts: {text: 'Generate', primary: true, click: 'passwordGen'}}
						]}},
						{type: 'inputDate', opts: {label: 'Account Expiration', min: new Date(), key: 'account.expires'}},
						{type: 'panelset', opts: {dir: 'row', content: [
							{type: 'inputCheckbox', opts: {label: 'Account Flags', desc: 'Active', key: 'account.active'}},
							{type: 'inputCheckbox', opts: {desc: 'Locked', key: 'account.locked'}}
						]}},
						{type: 'inputMultiselect', opts: {label: 'Security Roles', dataSource: roles, key: 'account.roles'}},
					]}
				]},
				{border: true, text: 'Account List', style: 'k-header section'},
				{border: true, padding: 5, content: [
					{type: 'inputAutocomplete', opts: {label: 'Username', width: 250, change: 'reloadGrid', ajax: function(opts){ this.userLookup(opts); }, key: 'filters.user'}},
					{type: 'inputButton', opts: {text: 'Clear Filters', primary: true, click: 'clearFilters'}},
					{type: 'inputButton', opts: {text: 'Export Accounts', primary: false, click: 'exportAccounts'}}
				]},
				{border: true, flex: 'auto', key: 'grid', content: [
					{type: 'grid', opts: this.gridOptions()}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			content: content,
			container: this.container
		});
		this.notifier = this.view.notifications.notifier;
		kendo.bind(this.container, this.model);
	}
	gridData(options){
		this.gridOpts = options;

		options.data.filters = this.model.filters.toJSON();

		Utils.ajax({
			route: 'adminGrid',
			data: options.data,
			success: function(data){
				data.results.forEach(function(item){
					item.phone = Utils.formatPhone(item.phone);
					item.acctId = item.id;
					item.rating = 0;
				});

				options.success({
					data: data.results,
					total: data.total
				});
			},
			error: function(err){
				console.log(err);
			}
		});
	}
	getData(){
		Utils.ajax({
			route: 'adminData',
			success: function(data){
				this.roles = data.roles;
				this.depts = data.depts;
				this.setView();
			},
			error: function(err){
				console.log(err);
			}
		});
	}
	gridSelect(obj){
		this.backup = obj.dataItem.toJSON();
		this.model.set('account',  this.backup);
		//Utils.modelBulkSet(this.model, this.backup);
	}
	gridOptions(){
		let options = {
			key: 'accounts',
			autoBind: true,
			autoFit: false,
			selectable: 'row',
			scrollable: true,
			reorderable: true,
			editable: false,
			sortable: {mode: 'multiple', allowUnsort: true},
			transport: function(options){ this.gridData(options); }.bind(this),
			paging: {
				size: 100,
				dropdown: true,
				prevNext: true,
				numeric: true,
				buttonCount: false,
				input: true,
				refresh: true,
				info: true
			},
			columns: [
				{title: 'Username', type: 'string', field: 'username', width: 100},
				{title: 'First Name', type: 'string', field: 'first_nm', width: 100},
				{title: 'Last Name', type: 'string', field: 'last_nm', width: 100},
				{title: 'Department', type: 'string', field: 'dept_nm', width: 200},
				{title: 'Title', type: 'string', field: 'title', width: 200},
				{title: 'Email', type: 'string', field: 'email', width: 200},
				{title: 'Phone', type: 'string', field: 'phone', width: 140},
				{title: 'Active', type: 'boolean', field: 'active', width: 80},
				{title: 'Admin', type: 'boolean', field: 'admin', width: 80}
			],
			onSelect: function(obj){ this.gridSelect(obj); }.bind(this)
		};

		return options;
	}
}
