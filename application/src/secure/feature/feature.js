/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class FeatureSet{
	constructor(){
		this.setModel();
	}
	setModel(){
		this.model = {
			count: 0,
			features: {},
			nodes: {},
			segments: {}
		};
	}
	empty(){
		this.setModel();
	}
	count(){
		return this.model.count;
	}
	getFeature(id){
		return this.model.features[id];
	}
	importFeature(feature, props = {}, opts = {}){
		let type = feature.type.toLowerCase();

		this.addFeature(type, props, opts);

		for(let [idx, ring] of feature.coordinates.entries()){
			for(let point of ring){
				this.addPoint(point, idx);
			}
		}

		this.active.valid = true;
		this.endFeature();
	}
	addFeature(type, props = {}, opts = {}){
		let id = Utils.uuid();

		type = type.toLowerCase();
		props.id = id;

		this.model.count++;
		this.model.features[id] = {
			id: id,
			type: type,
			rings: [],
			holes: [],
			radius: opts.radius,
			units: opts.units,
			buffer: opts.buffer,
			properties: props
		};

		this.active = this.model.features[id];
	}
	endFeature(){
		if(!this.active){ return; }
		if(!this.active.geojson){ this.geojson(); }
		this.setProperty('mapStyle', 2, this.active.id);

		if(this.active.type === 'polygon'){
			if(!this.active.valid || this.active.rings.length < 3){
				this.removeFeature(this.active.id);
			}
		}

		this.active = false;
	}
	cancelFeature(){
		if(!this.active){ return; }
		this.removeFeature(this.active.id);
		this.active = false;
	}
	removeFeature(id){
		delete this.model.features[id];

		this.model.count--;
	}
	addPoint(point, hole = 0){
		let id = Utils.uuid();
		let hash = point.join(':');

		if(this.hash && this.hash === hash){ return; }

		this.hash = hash;
		this.model.nodes[id] = point;
		if(hole){
			let idx = hole - 1;
			if(!this.active.holes[idx]){ this.active.holes.push([]); }
			this.active.holes[idx].push(id);
		} else {
			this.active.rings.push(id);
		}
	}
	updateMeasures(key, value){
		if(!this.active){ return; }

		this.active[key] = value;

		if(key === 'buffer'){
			this.updateGeomType(key, value);
		}
	}
	updateGeomType(key, value){
		let obj = this.active;

		if(obj.type === 'linestring' || obj.type === 'linebuffer'){
			obj.type = value ? 'linebuffer' : 'linestring';
		} else if(obj.type === 'point' || obj.type === 'pointbuffer'){
			obj.type = value ? 'pointbuffer' : 'point';
		}
	}
	setProperty(key, value, id){
		let feature = id ? this.model.features[id] : this.active;
		if(!feature){ return; }
		let geojson = Utils.isArray(feature.geojson) ? feature.geojson : [feature.geojson];

		feature.properties[key] = value;

		for(let obj of geojson){
			if(!obj){ return; }
			obj.properties[key] = value;
		}
	}
	geojson(opts = {}){
		let geojson = new Geojson();

		if(!this.model.count){
			return geojson.featureCollection();
		}

		for(let [key, item] of Object.entries(this.model.features)){
			let active = (item.id === this.active.id);

			if(active || !item.geojson){
				let rings = this.getPoints(item.rings);
				let holes = [];

				for(let hole of item.holes){
					holes.push(this.getPoints(hole));
				}

				if(opts.append){
					let hash = opts.append.join(':');
					let dupe = (this.hash && this.hash === hash);
					if(!dupe){ rings.push(opts.append);	}
				}

				this.processFeature(geojson, item, rings, holes, opts);
			} else {
				geojson.addFeatures(item.geojson);
			}
		}

		return geojson.featureCollection();
	}
	processFeature(geojson, item, rings, holes, opts){
		let gtype = GisUtils.checkGtype(item.type, rings);
		let feature = false;

		console.log(gtype, rings);

		switch(gtype){
			case 'point':
				feature = geojson.point(rings[0], item.properties);
			break;
			case 'linestring':
				feature = geojson.linestring(rings, item.properties);
			break;
			case 'polygon':
				let geom = [rings];
				for(let hole of holes){
					geom.push(hole);
				}
				feature = geojson.polygon(geom, item.properties);
			break;
			case 'circle':
				feature = geojson.circle(rings, undefined, undefined, item.properties);
			break;
			case 'linebuffer':
				feature = geojson.linebuffer(rings, item.radius, item.units, item.properties);
			break;
			case 'pointbuffer':
				feature = geojson.circle(rings, item.radius, item.units, item.properties);
			break;
		}

		if(gtype === 'linestring'){
			let dist = GisUtils.lineLength(rings, item.units);
			this.distance = Utils.precision(dist.distance, 2) + ' ' + dist.units;
		}

		let valid = feature.properties.valid;
		feature.properties.mapStyle = valid ? 1 : 4;
		item.geojson = feature;
		item.valid = valid;
	}
	getPoints(arr){
		if(!arr || !arr.length){ return []; }

		let output = [];

		for(let item of arr){
			let point = this.model.nodes[item];
			output.push(point);
		}

		return output;
	}
}
