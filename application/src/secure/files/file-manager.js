/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class FileManager{
	constructor(){
		this.visible = false;
	}
	show(){
		mainBar.register(this);
	}
	hide(){
		mainBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	setModel(){
		this.model = kendo.observable({});
	}
	setView(){
		let content = [
			{type: 'panelset', opts: {key: 'container', flex: 'auto', items: [
				{border: true, text: 'File Manager', style: 'k-header header'},
				{border: false, flex: 'auto', content: [
					{type: 'filetree', opts: {fs: 'mainFileSytem', root: 'Application Files'}}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			content: content,
			container: this.container
		});

		kendo.bind(this.view.panels.container, this.model);
	}
}
