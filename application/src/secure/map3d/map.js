/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Map3d{
	constructor(){
		this.visible = false;
	}
	show(){
		this.bbox = mapAPI.bbox();
		mainBar.register(this);
		this.buildMap();
	}
	hide(){
		mainBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	setModel(){
		this.model = kendo.observable({});
	}
	setView(){
		let content = [
			{type: 'panelset', opts: {key: 'container', flex: 'auto'}}
		];

		this.view = new View({
			model: this.model,
			content: content,
			container: this.container
		});

		kendo.bind(this.view.panels.container, this.model);
	}
	buildMap(){
		//let empty = Dom.el('div', {}, 'body').outerHTML();
		let options = {
			infoBox: false,
			homeButton: false,
			fullscreenButton: false,
			scene3DOnly: true,
			vrButton: false,
			animation: false,
			timeline: false,
			geocoder: false,
			//creditContainer:
		};
		let bbox = this.bbox;
		let container = this.view.panels.container;
		let wrapper = Dom.el('div', {id: 'wrapper', height: '100%', width: '100%'}, container);
		let viewer = new Cesium.Viewer('wrapper', options);
		let cesiumTerrainProviderMeshes = new Cesium.CesiumTerrainProvider({
			url : 'https://assets.agi.com/stk-terrain/world',
			requestWaterMask : true,
			requestVertexNormals : true
		});
		viewer.terrainProvider = cesiumTerrainProviderMeshes;
		viewer.scene.globe.enableLighting = false;
		viewer.scene.globe.depthTestAgainstTerrain = true;
		viewer.camera.setView({
			destination : Cesium.Rectangle.fromDegrees(bbox.minx, bbox.miny, bbox.maxx, bbox.maxy),
		});

		this.viewer = viewer;
		this.scene = viewer.scene;
		this.canvas = viewer.canvas;
		this.camera = viewer.camera;
		this.setLayers();

		/*
		scene.screenSpaceCameraController.enableRotate = false;
scene.screenSpaceCameraController.enableTranslate = false;
scene.screenSpaceCameraController.enableZoom = false;
scene.screenSpaceCameraController.enableTilt = false;
scene.screenSpaceCameraController.enableLook = false;
*/
	}
	setLayers(){
		let dataSource = new Cesium.GeoJsonDataSource();
		let classb = dataSource.load('map/geojson/classb.json', {
			fill: Cesium.Color.fromCssColorString('rgba(255, 0, 255, 0.2)'),
		});

		dataSource = new Cesium.GeoJsonDataSource();
		let classc = dataSource.load('map/geojson/classc.json', {
			fill: Cesium.Color.fromCssColorString('rgba(255, 0, 0, 0.1)'),
		});

		dataSource = new Cesium.GeoJsonDataSource();
		let classd = dataSource.load('map/geojson/classd.json', {
			fill: Cesium.Color.fromCssColorString('rgba(0, 0, 255, 0.1)'),
		});

		Cesium.when(classb, function(dataSource){ this.extrude(dataSource, 0, 20000); }.bind(this));
		Cesium.when(classc, function(dataSource){ this.extrude(dataSource, 20000, 20000); }.bind(this));
		Cesium.when(classd, function(dataSource){ this.extrude(dataSource, 40000, 20000); }.bind(this));

	}
	extrude(dataSource, elevation, height){
		this.viewer.dataSources.add(dataSource);

		let entities = dataSource.entities.values;

		for(let entity of dataSource.entities.values){
			let low = entity.properties.LOWALT;
			let high = entity.properties.HIGHALT;

			low = (low === 'SFC' ? 0 : Number(low)) * 5;
			high = Number(high) * 5;

			entity.polygon.height = low;
			entity.polygon.extrudedHeight = high;
			entity.polygon.outline = false;
		}
	}
}
