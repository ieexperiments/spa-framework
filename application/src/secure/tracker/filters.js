/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class TrackFilters{
	constructor(){
		this.visible = false;
	}
	show(){
		this.instance = mapAPI.getSelected();
		sideBar.register(this);
		this.setValues(this.instance.trackFilters);
	}
	hide(){
		sideBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	mapSwitch(instance){
		if(!this.visible){ return; }

		this.instance = instance;
		this.setValues(this.instance.trackFilters);
	}
	setValues(values){
		values = values || this.getDefaults();

		for(let [key, value] of Object.entries(values)){
			this.model.set(key, value);
		}
	}
	getDefaults(){
		return {
			speed: [0, 2000],
			alt: [0, 999],
			dept: undefined,
			dest: undefined,
			comm: true,
			ga: true
		};
	}
	apply(){
		let data = this.view.data();

		data.dept = data.dept ? data.dept.toUpperCase() : undefined;
		data.dest = data.dest ? data.dest.toUpperCase() : undefined;
		this.instance.trackFilters = data;
	}
	setModel(){
		this.model = kendo.observable({
			speed: [0, 2000],
			alt: [0, 999],
			dept: undefined,
			dest: undefined,
			comm: true,
			ga: true,
			setDefaults: function(){ this.setValues(); }.bind(this),
			apply: function(){ this.apply(); }.bind(this),
			hide: function(){ this.hide(); }.bind(this)
		});
	}
	setView(){
		let content = [
			{type: 'panelset', opts: {key: 'container', dir: 'column', items: [
				{text: 'Track Filters', style: 'k-header panel-section'},
				{content: [
					{type: 'inputNumericRange', opts: {key: 'speed', width: 80, label: 'Speed', min: 0, max: 2000, format: '####', decimals: 0}},
					{type: 'inputNumericRange', opts: {key: 'alt', width: 80, label: 'Altitude', min: 0, max: 999, format: '###', decimals: 0}}
				]},
				{items: [
					{content: [
						{type: 'inputText', opts: {key: 'dept', label: 'Dept', width: 80, maxlength: 4, uppercase: true}},
						{type: 'inputText', opts: {key: 'dest', label: 'Dest', width: 80, maxlength: 4, uppercase: true}}
					]}
				]},
				{items: [
					{content: [
						{type: 'inputCheckbox', opts: {key: 'comm', label: 'Type', desc: 'Commerical'}},
						{type: 'inputCheckbox', opts: {key: 'ga', desc: 'GA'}},
					]}
				]},
				{align: 'center', items: [
					{content: [
						{type: 'inputButton', opts: {text: 'Apply', primary: true, click: 'apply'}},
						{type: 'inputButton', opts: {text: 'Defaults', click: 'setDefaults'}},
						{type: 'inputButton', opts: {text: 'Close', click: 'hide'}},
					]}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			container: main.view.panels.sidebar,
			content: content
		});

		kendo.bind(this.view.panels.container, this.model);
	}
}
