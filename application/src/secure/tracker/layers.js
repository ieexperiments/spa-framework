/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class TrackLayers{
	constructor(){
		mapAPI.addSource('tracks');
		this.setLayers();
	}
	setLayers(comm = '#ff0000', ga = '#0000ff'){
		let layers = [
			{
				id: 'track-tail',
				type: 'line',
				source: 'tracks',
				lineColor: 'rgba(128, 0, 0, 1)',
				lineWidth: 2,
				minzoom: 7,
				maxzoom: 22,
				filter: [
					'all',
					['==', '$type', 'LineString']
				]
			},
			{
				id: 'trackpp-1',
				type: 'fill',
				source: 'tracks',
				fillColor: 'rgba(0, 0, 0, 0.2)',
				filter: ['all', ['==', '$type', 'Polygon']]
			},
			{
				id: 'trackpp-2',
				type: 'line',
				source: 'tracks',
				lineColor: 'rgba(128, 0, 0, 0.7)',
				lineWidth: 1,
				lineDasharray: [5,5],
				filter: ['all', ['==', '$type', 'Polygon']]
			},
			{
				id: 'track-1',
				type: 'symbol',
				source: 'tracks',
				minzoom: 0,
				maxzoom: 22,
				textFont: ['airplanes'],
				symbolPlacement: 'point',
				textField: '{chr}',
				textSize: {base: 1, stops: [[1, 2], [10, 28]]},
				textAllowOverlap: true,
				textIgnorePlacement: true,
				//textLineHeight: 1,
				//textPadding: 0,
				textAnchor: 'center',
				textJustify: 'center',
				textColor: comm,
				textHaloColor: 'rgba(0, 0, 0, 0.5)',
				textHaloWidth: 0.25,
				textHaloBlur: 0,
				textOffset: [0, 0.2],
				filter: [
					'all',
					['==', '$type', 'Point'],
					['==', 'type', 1]
				]
			},
			{
				id: 'track-2',
				type: 'symbol',
				source: 'tracks',
				minzoom: 0,
				maxzoom: 22,
				textFont: ['airplanes'],
				symbolPlacement: 'point',
				textField: '{chr}',
				textSize: {base: 1, stops: [[1, 2], [10, 28]]},
				textAllowOverlap: true,
				textIgnorePlacement: true,
				//textLineHeight: 1,
				//textPadding: 0,
				textAnchor: 'center',
				textJustify: 'center',
				textColor: ga,
				textHaloColor: 'rgba(0, 0, 0, 0.5)',
				textHaloWidth: 0.25,
				textHaloBlur: 0,
				textOffset: [0, 0.2],
				filter: [
					'all',
					['==', '$type', 'Point'],
					['==', 'type', 2]
				]
			}
		];

		for(let layer of layers){
			mapAPI.removeLayer(layer.id);
			mapAPI.addLayer(layer);
		}
	}
}
