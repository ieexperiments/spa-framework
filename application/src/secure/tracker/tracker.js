/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Tracker{
	constructor(opts){
		this.tracks = {};
		this.layers = new TrackLayers();
		this.parser = new TrackParser(this.tracks);
		this.renderer = new TrackRenderer(this.tracks);
		this.events = new TrackEvents(this.tracks);
		this.filters = new TrackFilters();
		this.settings = new TrackSettings();
	}
	start(){
		this.worker = new WebWorker('worker-tracker', function(msg){
			if(!this.running){ return; }
			this.parser.socket(msg);

			if(msg.type === 'trackMerge'){
				this.renderer.render();
			}

		}.bind(this));

		this.running = true;
	}
	stop(){
		this.running = false;
		this.worker.terminate();

		for(let [key, track] of Object.entries(this.tracks)){
			delete this.tracks[key];
			mapAPI.removeDatablock(key);
		}

		this.renderer.render();
	}
	clearDatablocks(){
		for(let [key, track] of Object.entries(this.tracks)){
			if(track.active){
				track.active = false;
				mapAPI.removeDatablock(track.tid);
			}
		}

		this.refresh();
	}
	refresh(){
		if(!this.running){ return; }

		this.renderer.render();
	}
	toggle(){
		if(this.running){
			this.stop();
		} else {
			this.start();
		}
	}
	toggleFilters(){
		this.filters.toggle();
	}
	toggleSettings(){
		this.settings.toggle();
	}
}
