/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class TrackSettings{
	constructor(){
		this.visible = false;
	}
	show(){
		sideBar.register(this);
		this.setValues(this.current);
	}
	hide(){
		sideBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	getDefaults(){
		return {angle: 30, minutes: 5, comm: '#ff0000', ga: '#0000ff'};
	}
	setValues(values){
		values = values || this.getDefaults();

		for(let [key, value] of Object.entries(values)){
			this.model.set(key, value);
		}
	}
	apply(){
		let data = this.view.data();

		this.current = data;

		tracker.renderer.minutes = data.minutes;
		tracker.renderer.angle = data.angle;
		tracker.layers.setLayers(data.comm, data.ga);
	}
	setModel(){
		this.model = kendo.observable({
			angle: 30,
			minutes: 5,
			comm: '#ff0000',
			ga: '#0000ff',
			setDefaults: function(){ this.setValues(); }.bind(this),
			apply: function(){ this.apply(); }.bind(this),
			hide: function(){ this.hide(); }.bind(this)
		});
	}
	setView(){
		let content = [
			{type: 'panelset', opts: {key: 'container', dir: 'column', items: [
				{text: 'Track Settings', style: 'k-header panel-section'},
				{content: [
					{type: 'inputNumeric', opts: {key: 'angle', width: 80, label: 'Projected Angle', min: 0, max: 360, format: '####', decimals: 0}},
					{type: 'inputNumeric', opts: {key: 'minutes', width: 80, label: 'Projected Minutes', min: 0, max: 999, format: '####', decimals: 0}},
					{type: 'inputColor', opts: {key: 'comm', label: 'Commercial Color'}},
					{type: 'inputColor', opts: {key: 'ga', label: 'GA Color'}},
				]},
				{align: 'center', items: [
					{content: [
						{type: 'inputButton', opts: {text: 'Apply', primary: true, click: 'apply'}},
						{type: 'inputButton', opts: {text: 'Defaults', click: 'setDefaults'}},
						{type: 'inputButton', opts: {text: 'Close', click: 'hide'}},
					]}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			container: main.view.panels.sidebar,
			content: content
		});

		kendo.bind(this.view.panels.container, this.model);
	}
}
