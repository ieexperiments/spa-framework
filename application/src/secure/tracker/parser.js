/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class TrackParser{
	constructor(tracks){
		this.tracks = tracks;
		this.symbols = {};
	}
	socket(msg){
		switch(msg.type){
			case 'trackHeader':
				this.setTracks(msg.data);
			break;
			case 'trackMerge':
				this.merge(msg.data);
			break;
			case 'trackRemove':
				this.remove(msg.data);
			break;
		}
	}
	setTracks(arr){
		for(let item of arr){
			this.tracks[item.tid] = $.extend(this.tracks[item.tid] || {}, item);
		}
	}
	remove(arr){
		for(let item of arr){
			delete this.tracks[item.tid];
			mapAPI.removeDatablock(item.tid);
		}
	}
	merge(arr){
		for(let item of arr){
			let track = this.tracks[item.tid];
			if(!track){ return; }
			$.extend(track, item);
			this.getSymbol(track);
			this.appendTail(track);
		}
	}
	appendTail(track){
		track.tail = track.tail || [];
		track.tail.push(track.point);

		if(track.tail.length > 60){
			track.tail = track.tail.slice(track.tail.length - 60);
		}
	}
	getSymbol(track){
		if(!track.point){ return; }

		let hash = track.type + ':' + track.heading;

		if(this.symbols[hash]){
			track.chr = this.symbols[hash];
			return;
		}

		let idx = ((track.type - 1) * 360) + (59648 + track.heading);

		track.chr = String.fromCharCode(idx);
		this.symbols[hash] = track.chr;
	}
}
