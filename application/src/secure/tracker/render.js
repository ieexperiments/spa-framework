/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class TrackRenderer{
	constructor(tracks){
		this.tracks = tracks;
		this.minutes = 5;
		this.angle = 30;
	}
	render(){
		let maps = mapAPI.getActive();

		for(let instance of maps){
			let geojson = new Geojson();

			for(let [key, track] of Object.entries(this.tracks)){
				let valid = this.checkFilters(instance, track);

				if(valid){
					geojson.point(track.point, {tid: track.tid, chr: track.chr, type: track.type});
					this.tail(geojson, instance, track);
				}

				if(track.active && track.active.idx === instance.mapIdx){
					if(valid){
						instance.datablock.setBlock(track);
						this.predicted(track, geojson);
					} else {
						instance.datablock.hide(track.tid);
					}
				}
			}
			if(!instance.moving){
				instance.map.getSource('tracks').setData(geojson.featureCollection());
			}
		}
	}
	predicted(track, geojson){
		let vector = new Vector2d(track.point);
		let pp = vector.predictedPath(track.speed, track.heading, this.minutes, this.angle, 'nm');

		geojson.polygon([pp], {tid: track.tid, }, false);
	}
	tail(geojson, instance, track){
		if(!track.tail || track.tail.length < 2 || instance.stats.zoom < 7){ return; }

		geojson.linestring(track.tail, {tid: track.tid});
	}
	checkFilters(instance, track){
		if(!track.point){ return false; }

		let viewBuffer = instance.utils.inViewportBuffer(track.point);
		let filters = instance.trackFilters;

		if(!viewBuffer.buffer){ return false; }
		if(!filters){ return true; }
		if(!Utils.isBetween(track.alt, filters.alt[0], filters.alt[1])){ return false; }
		if(!Utils.isBetween(track.speed, filters.speed[0], filters.speed[1])){ return false; }
		if(filters.dept && (track.dept !== filters.dept)){ return false; }
		if(filters.dest && (track.dest !== filters.dest)){ return false; }
		if(!filters.comm && track.type === 1){ return false; }
		if(!filters.ga && track.type === 2){ return false; }

		return true;
	}
}


/*
render(){
	let geojson = new GeoJSON();

	for(let [key, obj] of Object.entries(this.tracks)){
		if(obj.point){
			geojson.point(obj.point, {tid: obj.tid, chr: obj.chr, type: obj.type});
		}
	}
	mapAPI.setSourceData('tracks', geojson.featureCollection());
}
*/
