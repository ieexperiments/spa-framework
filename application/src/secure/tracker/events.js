/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class TrackEvents{
	constructor(tracks){
		this.tracks = tracks;
		this.listeners();
	}
	eventHandler(evt){
		let instance = evt.instance;

		switch(evt.type){
			case 'mousedown':
				this.mousedown(evt, instance);
			break;
			case 'mousemove':
				this.mousemove(evt, instance);
			break;
			case 'moveend':
				tracker.refresh();
		}
	}
	mousedown(evt, instance){

		if(instance.stats.zoom <= 6){ return; }

		let bbox = this.bbox(evt.point);
		let features = instance.map.queryRenderedFeatures(bbox, {layers: ['track-1','track-2']});

		if(!features.length){ return; }

		let properties = features[0].properties;
		this.toggleTrack(instance, properties.tid);
	}
	mousemove(evt, instance){

	}
	toggleTrack(instance, tid){
		let track = this.tracks[tid];

		if(!track){ return; }

		track.active = track.active ? false : {idx: instance.mapIdx};

		if(track.active){
			instance.datablock.setBlock(track);
		} else {
			instance.datablock.remove(track.tid);
		}
	}
	bbox(point, tol = 5){
		return [[point.x - tol, point.y - tol], [point.x + tol, point.y + tol]];
	}
	listeners(){
		mapAPI.on('mousemove', function(evt){ this.eventHandler(evt); }.bind(this));
		mapAPI.on('mousedown', function(evt){ this.eventHandler(evt); }.bind(this));
		mapAPI.on('moveend', function(evt){ this.eventHandler(evt); }.bind(this));
		//mapAPI.on('mouseup', function(evt){ this.eventHandler(evt); }.bind(this));
		//mapAPI.on('click', function(evt){ this.eventHandler(evt); }.bind(this));
		//mapAPI.on('dblclick', function(evt){ this.eventHandler(evt); }.bind(this));
	}
}
