/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class SUA{
	constructor(){
		this.getSchedules();
	}
	processSchedules(data){
		let filter1 = ['!in','NM'];
		let filter2 = ['in','NM'];
		let layers = ['suaaf','suaal','suadf','suadl','suamf','suaml','suapf','suapl','suarf','suarl','suatf','suatl','suawf','suawl'];

		if(!data.length){ return; }

		for(let item of data){
			filter1.push(item.nm);
			filter2.push(item.nm);
		}

		for(let layer of layers){
			mapAPI.setFilter(layer, filter1);
			mapAPI.setFilter(layer + 'a', filter2);
		}
	}
	getSchedules(){
		Utils.ajax({
			route: 'suaSchedules',
			success: function(data){
				this.processSchedules(data);
			}.bind(this)
		});
	}
}
