/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const GisUtils = {
	densify: function(data, distance = 50, units = 'km'){
		let obj = this.toSegments(data);

		for(let [key, segments] of obj.segments.entries()){
			let arr = [];

			for(let segment of segments){
				let p1 = segment[0];
				let p2 = segment[1];
				let min = p1[0] < p2[0] ? p1[0] : p2[0];
				let max = p1[0] > p2[0] ? p1[0] : p2[0];
				let vector = new Vector2d(p1);
				let length = vector.distance(p2, units);
				let prev = false;

				if(length >= distance){
					let count = Math.ceil(length / distance);

					for(let offset = 1; offset < count; offset++){
						let point = vector.intermediatePoint(p2, offset/count);
						if(point[0] < min){ point[0] += 360; }
						if(point[0] > max){ point[0] -= 360; }
						arr.push([prev || p1, point]);
						prev = point;
					}
					arr.push([prev, p2]);
				} else {
					arr.push(segment);
				}
			}

			obj.segments[key] = arr;
		}
		let out = this.fromSegments(obj);

		return out;
	},
	lineLength(data, units){
		let obj = this.toSegments(data);
		let dist = 0;

		for(let segment of obj.segments[0]){
			let p1 = segment[0];
			let p2 = segment[1];
			let vector = new Vector2d(p1);

			dist += vector.distance(p2, units);
		}

		return {distance: dist, units: units};
	},
	toSegments(obj){
		let str = JSON.stringify(obj);
		let match = str.match(/(\[\[-?\d(.(?!\]\]))*\d\]\])/g);
		let output = {segments: []};

		for(let [key, value] of match.entries()){
			let arr = JSON.parse(value);
			let segments = [];
			str = str.replace(value, '{' + key + '}');

			for(let [idx, curr] of arr.entries()){
				let next = arr[idx + 1];
				if(next){ segments.push([curr, next]); }
			}

			output.segments.push(segments);
		}

		output.str = str;
		return output;
	},
	fromSegments(obj){
		for(let [key, segments] of obj.segments.entries()){
			let arr = [];
			key = '{' + key + '}';

			for(let [idx, segment] of segments.entries()){
				if(!idx){ arr.push(segment[0]); }
				arr.push(segment[1]);
			}

			obj.str = obj.str.replace(key, JSON.stringify(arr));
		}

		return JSON.parse(obj.str);
	},
	rotateDegree(degree, rotate){
		let deg = (degree + rotate) % 360;

		if(deg < 0){ deg = 360 + deg; }

		return deg;
	},
	checkGtype(type, rings){
		let gtype = type;
		let len = rings.length;

		switch(len){
			case 1:
				if(gtype === 'pointbuffer'){
					gtype = 'pointbuffer';
				} else {
					gtype = 'point';
				}
			break;
			case 2:
				if(type === 'polygon'){
					gtype = 'linestring';
				} else {
					gtype = type;
				}
			break;
			default:
				gtype = type;
			break;
		}

		return gtype;
	}
};
