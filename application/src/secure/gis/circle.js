/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Circle = {
	create(coordinate, radius = false, units = 'km', density = 2){
		let arr = [];
		let center = this.getCenter(coordinate, radius, units);

		while(center.bearing >= 0){
			let vector = new Vector2d(center.point);
			let point = vector.pointAtBearing(center.bearing, center.radius, center.units);
			arr.push(point);
			center.bearing -= density;
		}

		return arr;
	},
	getCenter(coordinate, radius, units){
		let center = {bearing: 360, units: units};
		let line = Utils.isArray(coordinate[0]);

		center.point = line ? coordinate[0] : coordinate;

		if(!radius && line){
			let vector = new Vector2d(center.point);
			center.radius = vector.distance(coordinate[1], units);
		} else {
			center.radius = radius;
		}

		return center;
	}
};
