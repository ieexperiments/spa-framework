/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const jstsFactory = new jsts.geom.GeometryFactory(new jsts.geom.PrecisionModel(), 4326);

const Topology = {
	union(geom1, geom2){
		geom1 = this.getGeometry(geom1);
		geom2 = this.getGeometry(geom2);

		return geom1.union(geom2);
	},
	difference(geom1, geom2){
		geom1 = this.getGeometry(geom1);
		geom2 = this.getGeometry(geom2);

		return geom1.difference(geom2);

	},
	intersect(geom1, geom2){
		geom1 = this.getGeometry(geom1);
		geom2 = this.getGeometry(geom2);

		return geom1.intersection(geom2);
	},
	isValid(obj){
		let geom = this.getGeometry(obj);

		return geom.isValid();
	},
	getGeometry(obj){
		let geom = false;

		if(obj instanceof jsts.geom.Geometry){
			geom = obj;
		} else if(obj.geometry && obj.geometry instanceof jsts.geom.Geometry){
			geom = obj.geometry;
		} else {
			geom = this.read(obj).geometry;
		}

		return geom ? geom.norm() : geom;
	},
	bbox: function(obj){
		let geom = this.getGeometry(obj);
		geom.getEnvelope();
		return geom.envelope;
	},
	read(geojson){
		let geom = false;

		try{
			geom = this.reader.read(geojson);
		} catch(err){
			console.log(err);
		}

		return geom;
	},
	write(geometry){
		return this.writer.write(geometry);
	},
	reader: new jsts.io.GeoJSONReader(jstsFactory),
	writer: new jsts.io.GeoJSONWriter(jstsFactory),
};
