/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Geojson{
	constructor(geojson = false){
		this._features = [];
		this._geometries = [];

		if(!geojson){ return; }

		switch(geojson.type){
			case 'Point':
			case 'LineString':
			case 'Polygon':
			case 'MultiPoint':
			case 'MultiLineString':
			case 'MultiPolygon':
				this._geometries = [geojson];
				this._features = [{type: 'Feature', geometry: geojson}];
			break;
			case 'Feature':
				this.setFeatures([geojson]);
			break;
			case 'FeatureCollection':
				this.setFeatures(geojson.features);
			break;
			case 'GeometryCollection':
				this._geometries = geojson.geometries;
			break;
		}
	}
	point(coordinates, properties = {}){
		return this.feature('Point', coordinates, properties);
	}
	polygon(coordinates, properties = {}, geodesic = true){
		let a = coordinates[0][0];
		let b = Utils.arrayLast(coordinates[0]);
		if(a.join(':') !== b.join(':')){ coordinates[0].push(a); }
		if(geodesic){ coordinates = GisUtils.densify(coordinates); }
		return this.feature('Polygon', coordinates, properties);
	}
	linestring(coordinates, properties = {}, geodesic = true){
		if(geodesic){ coordinates = GisUtils.densify(coordinates); }
		return this.feature('LineString', coordinates, properties);
	}
	multipoint(coordinates, properties = {}){
		return this.feature('MultiPoint', coordinates, properties);
	}
	multipolygon(coordinates, properties = {}, geodesic = true){
		if(geodesic){ coordinates = GisUtils.densify(coordinates); }
		return this.feature('MultiPolygon', coordinates, properties);
	}
	multilinestring(coordinates, properties = {}, geodesic = true){
		if(geodesic){ coordinates = GisUtils.densify(coordinates); }
		return this.feature('MultiLineString', coordinates, properties);
	}
	circle(coordinates, radius, units, properties = {}){
		let circle = Circle.create(coordinates, radius, units);
		return this.polygon([circle], properties, false);
	}
	linebuffer(coordinates, radius, units, properties = {}){
		let buffer = BufferedLine.create(coordinates, radius, units);
		return this.polygon(buffer, properties, false);
	}
	feature(type, coordinates, properties = {}){
		let geometry = {type: type, coordinates: coordinates};
		let feature = {type: 'Feature', properties: properties, geometry: geometry};

		feature.properties.valid = 1;

		if(type === 'Polygon'){
			feature.properties.valid = Topology.isValid(feature) ? 1 : 0;
		}

		this._geometries.push(geometry);
		this._features.push(feature);

		return feature;
	}
	setFeatures(features){
		let arr1 = [];
		let arr2 = [];

		for(let feature of features){
			arr1.push(feature);
			arr2.push(feature.geometry);
		}

		this._features = arr1;
		this._geometries = arr2;
	}
	addFeatures(features){
		let arr1 = [];
		let arr2 = [];

		features = Utils.isArray(features) ? features : [features];

		for(let feature of features){
			arr1.push(feature);
			arr2.push(feature.geometry);
		}

		this._features = this._features.concat(arr1);
		this._geometries = this._geometries.concat(arr2);
	}
	getFeatures(prop, val){
		let output = [];

		for(let feature of this._features){
			if(feature.properties[prop] && feature.properties[prop] === val){
				output.push(feature);
			}
		}

		return output;
	}
	geometryCollection(){
			return {type: 'GeometryCollection', geometries: this._geometries};
	}
	featureCollection(){
		return {type: 'FeatureCollection', features: this._features};
	}
	dataURL(){
		let url = 'data:application/vnd.geo+json,';

		url += JSON.stringify(this.featureCollection());

		return url;
	}
	get features(){
		return this._features;
	}
	get geometries(){
		return this._geometries;
	}
	set features(features){
			this.setFeatures(features);
	}
	set geometries(geometries){
			this._geometries = geometries;
	}
}
