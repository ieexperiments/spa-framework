/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Vector2d{
	constructor(coord){
		this.lon = coord[0];
		this.lat = coord[1];
	}
	distance(coord, units = 'km'){
		const point = this.getPoint(coord);
		const R = 6371e3;
		const φ1 = this.toRadians(this.lat);
		const λ1 = this.toRadians(this.lon);
		const φ2 = this.toRadians(point.lat);
		const λ2 = this.toRadians(point.lon);
		const Δφ = φ2 - φ1;
		const Δλ = λ2 - λ1;
		const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		const d = R * c;

		return UnitConversion.distance(d, 'm', units);
	}
	bearing(coord){
		const point = this.getPoint(coord);
		const φ1 = this.toRadians(this.lat);
		const φ2 = this.toRadians(point.lat);
		const Δλ = this.toRadians(point.lon - this.lon);
		const y = Math.sin(Δλ) * Math.cos(φ2);
		const x = Math.cos(φ1)* Math.sin(φ2) - Math.sin(φ1) * Math.cos(φ2) * Math.cos(Δλ);
		const θ = Math.atan2(y, x);

		return (this.toDegrees(θ) + 360) % 360;
	}
	bearingReverse(coord){
		const point = this.getPoint(coord);

		return (point.bearing(this) + 180 ) % 360;
	}
	midpoint(coord){
		const point = this.getPoint(coord);
		const φ1 = this.toRadians(this.lat);
		const λ1 = this.toRadians(this.lon);
		const φ2 = this.toRadians(point.lat);
		const Δλ = this.toRadians(point.lon - this.lon);
		const Bx = Math.cos(φ2) * Math.cos(Δλ);
		const By = Math.cos(φ2) * Math.sin(Δλ);
		const x = Math.sqrt((Math.cos(φ1) + Bx) * (Math.cos(φ1) + Bx) + By * By);
		const y = Math.sin(φ1) + Math.sin(φ2);
		const φ3 = Math.atan2(y, x);
		const λ3 = λ1 + Math.atan2(By, Math.cos(φ1) + Bx);

		return [(this.toDegrees(λ3) + 540) % 360 - 180, this.toDegrees(φ3)];
	}
	intermediatePoint(coord, fraction){
		const point = this.getPoint(coord);
		const φ1 = this.toRadians(this.lat);
		const λ1 = this.toRadians(this.lon);
		const φ2 = this.toRadians(point.lat);
		const λ2 = this.toRadians(point.lon);
		const sinφ1 = Math.sin(φ1), cosφ1 = Math.cos(φ1), sinλ1 = Math.sin(λ1), cosλ1 = Math.cos(λ1);
		const sinφ2 = Math.sin(φ2), cosφ2 = Math.cos(φ2), sinλ2 = Math.sin(λ2), cosλ2 = Math.cos(λ2);
		const Δφ = φ2 - φ1;
		const Δλ = λ2 - λ1;
		const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
		const δ = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		const A = Math.sin((1 - fraction) * δ) / Math.sin(δ);
		const B = Math.sin(fraction * δ) / Math.sin(δ);
		const x = A * cosφ1 * cosλ1 + B * cosφ2 * cosλ2;
		const y = A * cosφ1 * sinλ1 + B * cosφ2 * sinλ2;
		const z = A * sinφ1 + B * sinφ2;
		const φ3 = Math.atan2(z, Math.sqrt(x * x + y * y));
		const λ3 = Math.atan2(y, x);

		return [(this.toDegrees(λ3) + 540) % 360 - 180, this.toDegrees(φ3)];
	}
	pointAtBearing(bearing, dist, units){
		const meters = UnitConversion.distance(dist, units, 'm');
		const δ = meters / 6371e3;
		const θ = this.toRadians(Number(bearing));
		const φ1 = this.toRadians(this.lat);
		const λ1 = this.toRadians(this.lon);
		const sinφ1 = Math.sin(φ1);
		const cosφ1 = Math.cos(φ1);
		const sinδ = Math.sin(δ);
		const cosδ = Math.cos(δ);
		const sinθ = Math.sin(θ);
		const cosθ = Math.cos(θ);
		const sinφ2 = sinφ1 * cosδ + cosφ1 * sinδ * cosθ;
		const φ2 = Math.asin(sinφ2);
		const y = sinθ * sinδ * cosφ1;
		const x = cosδ - sinφ1 * sinφ2;
		const λ2 = λ1 + Math.atan2(y, x);

		return [(this.toDegrees(λ2) + 540) % 360 - 180, this.toDegrees(φ2)];
	}
	intersection(coord1, bearing1, coord2, bearing2){
		const p1 = this.getPoint(coord1);
		const p2 = this.getPoint(coord2);
		const φ1 = this.toRadians(p1.lat);
		const λ1 = this.toRadians(p1.lon);
		const φ2 = this.toRadians(p2.lat);
		const λ2 = this.toRadians(p2.lon);
		const θ13 = this.toRadians(Number(bearing1));
		const θ23 = this.toRadians(Number(bearing2));
		const Δφ = φ2-φ1;
		const Δλ = λ2-λ1;
		const δ12 = 2 * Math.asin(Math.sqrt(Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2)));

		if(δ12 === 0){ return null; }

		let θa = Math.acos((Math.sin(φ2) - Math.sin(φ1) * Math.cos(δ12)) / (Math.sin(δ12) * Math.cos(φ1)));

		if(isNaN(θa)){ θa = 0; }

		const θb = Math.acos((Math.sin(φ1) - Math.sin(φ2) * Math.cos(δ12)) / (Math.sin(δ12) * Math.cos(φ2)));
		const θ12 = Math.sin(λ2 - λ1) > 0 ? θa : 2 * Math.PI - θa;
		const θ21 = Math.sin(λ2 - λ1) > 0 ? 2 * Math.PI - θb : θb;
		const α1 = (θ13 - θ12 + Math.PI) % (2 * Math.PI) - Math.PI;
		const α2 = (θ21 - θ23 + Math.PI) % (2 * Math.PI) - Math.PI;

		if (Math.sin(α1) === 0 && Math.sin(α2) === 0){ return null; }
		if (Math.sin(α1) * Math.sin(α2) < 0){ return null; }

		const α3 = Math.acos(-Math.cos(α1) * Math.cos(α2) + Math.sin(α1) * Math.sin(α2) * Math.cos(δ12));
		const δ13 = Math.atan2(Math.sin(δ12) * Math.sin(α1) * Math.sin(α2), Math.cos(α2) + Math.cos(α1) * Math.cos(α3));
		const φ3 = Math.asin(Math.sin(φ1) * Math.cos(δ13) + Math.cos(φ1) * Math.sin(δ13) * Math.cos(θ13));
		const Δλ13 = Math.atan2(Math.sin(θ13) * Math.sin(δ13) * Math.cos(φ1), Math.cos(δ13) - Math.sin(φ1) * Math.sin(φ3));
		const λ3 = λ1 + Δλ13;

		return [(this.toDegrees(λ3) + 540) % 360 - 180, this.toDegrees(φ3)];
	}
	crossTrackDistance(coord1, coord2) {
		const p1 = this.getPoint(coord1);
		const p2 = this.getPoint(coord2);
		const radius = 6371e3;
		const δ13 = p1.distance(this) / radius;
		const θ13 = this.toRadians(p1.bearing(this));
		const θ12 = this.toRadians(p1.bearing(p2));
		const dxt = Math.asin( Math.sin(δ13) * Math.sin(θ13 - θ12)) * radius;

		return dxt;
	}
	predictedPath(speed, heading, mins, degs, units){
		let half = degs / 2;
		let start = heading - half + 0.5;
		let end = heading + half - 0.5;
		let origin = [this.lon, this.lat];
		let dist = (speed * (mins / 60));
		let coords = [origin];

		while(start <= end){
			coords.push(this.pointAtBearing(start, dist, units));
			start++;
		}

		coords.push(origin);
		return (coords);
	}
	intercept(coord, speed1, speed2, heading, units){
		const point = this.getPoint(coord);
		const dx = point.lon - this.lon;
		const dy = point.lat - this.lat;
		const cos = Math.cos(this.toRadians(heading));
		const sin = Math.sin(this.toRadians(heading));
		const dist = (dx * dx) + (dy * dy);
		const a = 2 * speed2 * (sin * dx + cos * dy);
		const sqr = Math.sqrt((a * a) - (4 * dist * ((speed2 * speed2) - (speed1 * speed1))));
		const x1 = 2 * dist / ((a * -1) + sqr);
		const x2 = 2 * dist / ((a * -1) - sqr);
		const xMin = Math.min(x1, x2);
		const xMax = Math.max(x1, x2);
		const x = xMin > 0 ? xMin : xMax;

		if(x < 0 || isNaN(x1)){ return false; }

		const intercept = [point.lon + speed2 * (x * sin), point.lat + speed2 * (x * cos)];
		const db = this.distanceBearing(intercept, units, true);
		const obj = {point: intercept, distance: db.distance, bearing: db.bearing, time: ((db.distance / speed1) * 60).toFixed(1)};

		return obj;
	}
	maxLatitude(bearing) {
		const θ = this.toRadians(Number(bearing));
		const φ = this.toRadians(this.lat);
		const φMax = Math.acos(Math.abs(Math.sin(θ) * Math.cos(φ)));

		return this.toDegrees(φMax);
	}
	equals(point){
		if(this.lat !== point.lat){ return false; }
    if(this.lon !== point.lon){ return false; }

    return true;
	}
	getPoint(point){
		return (point instanceof Vector2d) ? point : new Vector2d(point);
	}
	toRadians(num){
		return num * Math.PI / 180;
	}
	toDegrees(num){
		return num * 180 / Math.PI;
	}
}
