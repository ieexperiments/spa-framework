/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const BufferedLine = {
	create(line, radius, units){
		if(line.length < 2){ return Circle.create(line, radius, units); }
		let geoms = this.buildGeometries(line, radius, units);
		return this.union(geoms);
	},
	union(geoms){
		let geometry = false;

		for(let [idx, circle] of geoms.circles.entries()){
			let jsts = this.getGeom(circle);
			geometry = idx ? Topology.union(geometry, jsts) : jsts;

			if(geoms.polygons[idx]){
				jsts = this.getGeom(geoms.polygons[idx]);
				geometry = Topology.union(geometry, jsts);
			}
		}

		let geojson = Topology.write(geometry);
		return geojson.coordinates;
	},
	getGeom(coordinates){
		let feature = {type: 'Feature', geometry: {type: 'Polygon', coordinates: [coordinates]}};
		return Topology.read(feature);
	},
	buildGeometries(line, radius, units){
		let out = {polygons: [], circles: []};

		for(let [idx, curr] of line.entries()){
			let next = line[idx + 1];

			let circle = Circle.create(curr, radius, units);
			out.circles.push(circle);

			if(next){
				let polygon = this.getPolygon([curr, next], radius, units);
				out.polygons.push(polygon);
			}
		}

		return out;
	},
	getPolygon(line, radius, units){
		let path = GisUtils.densify(line);
		let obj = GisUtils.toSegments(path);
		let arr1 = [];
		let arr2 = [];

		for(let [idx, segment] of obj.segments[0].entries()){
			let p1 = segment[0];
			let p2 = segment[1];
			let vector = new Vector2d(p1);
			let bearing = vector.bearing(p2);
			let deg1 = GisUtils.rotateDegree(bearing, 90);
			let deg2 = GisUtils.rotateDegree(bearing, -90);

			if(!idx){
				arr1.push(vector.pointAtBearing(deg1, radius, units));
				arr2.push(vector.pointAtBearing(deg2, radius, units));
			}

			vector = new Vector2d(p2);
			arr1.push(vector.pointAtBearing(deg1, radius, units));
			arr2.push(vector.pointAtBearing(deg2, radius, units));
		}

		let polygon = arr1.concat(arr2.reverse());
		polygon.push(polygon[0]);
		return polygon;
	}
};
