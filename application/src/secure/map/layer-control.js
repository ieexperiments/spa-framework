/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapLayerControl{
	constructor(instance){
		this.instance = instance;
		this.map = instance.map;
		this.clone();
	}
	show(){
		sideBar.register(this);
	}
	hide(){
		sideBar.unregister();
	}
	toggle(){
		if(this.visible){
			this.hide();
		} else {
			this.show();
		}
	}
	mapSwitch(instance){
		if(!this.visible){ return; }
		if(instance.mapIdx === this.instance.mapIdx){ return; }

		instance.layerControl.toggle();
	}
	setModel(){
		this.model = kendo.observable({});
	}
	setView(){
		let dataSource = this.treeData();
		let treeOpts = {
			dataSource: this.treeData(),
			autoSort: true,
			checkChildren: true,
			checkboxes: true,
			onSelect: function(obj){ this.onSelect(obj); }.bind(this),
			onExpCol: function(obj){ this.onExpCol(obj); }.bind(this)
		};
		let content = [
			{type: 'panelset', opts: {key: 'container', flex: 'auto', dir: 'column', items: [
				{text: 'Map Layers', style: 'k-header panel-section'},
				{flex: 'auto', content: [
					{type: 'treeview', opts: treeOpts}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			container: main.view.panels.sidebar,
			content: content
		});
	}
	setVisible(node, dataItem){
		let layer = this.layers[node.key];
		let def = Data.mapLayers[node.key];
		let checked = node.checked;
		let visibility = checked ? 'visible' : 'none';

		layer.visible = checked;

		for(let id of def.layers){
			if(this.validLayers[id]){
				this.map.setLayoutProperty(id, 'visibility', visibility);
			} else {
				console.log('invalid', id);
			}
			this.checkRaster(dataItem, def, layer);
		}
	}
	checkRaster(node, def, layer){
		if(!def.raster){ return; }
		if(!node.checked){
			this.raster = undefined;
			return;
		}

		if(this.raster){
			this.map.setLayoutProperty(this.raster.def.layers[0], 'visibility', 'none');
			this.raster.layer.visible = false;
			this.raster.node.set('checked', false);
		}

		this.raster = {node: node, def: def, layer: layer};
	}
	onSelect(obj){
		let data = obj.data;

		if(data.items){
			for(let node of data.items){
				this.setVisible(node, obj.dataItem);
			}
		} else {
			this.setVisible(data, obj.dataItem);
		}
	}
	onExpCol(obj){
		this.folders[obj.data.key].expanded = obj.data.expanded;
	}
	setValidLayers(){
		let layers = this.map.getStyle().layers;
		this.validLayers = {};

		for(let layer of layers){
			this.validLayers[layer.id] = true;
		}
	}
	themeChange(){
		this.setValidLayers();

		for(let [key, item] of Object.entries(this.layers)){
			for(let id of item.layers){
				if(this.validLayers[id]){
					this.map.setLayoutProperty(id, 'visibility', item.visible ? 'visible' : 'none');
				}
			}
		}
	}
	treeData(){
		let dataSource = [];

		for(let [key, folder] of Object.entries(this.folders)){
			folder.items = [];
			folder.key = key;
		}

		for(let [key, layer] of Object.entries(this.layers)){
			this.folders[layer.folder].items.push({text: layer.text, checked: layer.visible, key: key});
		}
		for(let [key, folder] of Object.entries(this.folders)){
			dataSource.push(folder);
		}

		return dataSource;
	}
	clone(){
		this.folders = {};
		this.layers = {};

		for(let [key, folder] of Object.entries(Data.mapFolders)){
			this.folders[key] = Object.assign({}, folder);
		}

		for(let [key, layer] of Object.entries(Data.mapLayers)){
			this.layers[key] = Object.assign({}, layer);
		}
	}
}
