/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapSources{
	constructor(instance){
		this.instance = instance;
		this.map = instance.map;
		this.registry = [];
	}
	add(id, item = {type: 'geojson'}, register = true){
		if(item.type === 'geojson' && !item.data){
			item.data = {type: 'FeatureCollection', features: []};
		}

		if(register){
			this.registry.push({id: id, data: item});
		}

		this.map.addSource(id, item);
	}
	remove(id){
		let key = false;

		for(let [idx, source] of Object.entries(this.registry)){
			if(source.id === id){ key = idx; }
		}

		if(!key){ return; }

		this.registry.splice(key, 1);
		try{ this.map.removeSource(id); } catch(err){ console.log(err);}
	}
	reload(){
		for(let [key, source] of Object.entries(this.registry)){
			this.add(source.id, source.data, false);
		}
	}
}
