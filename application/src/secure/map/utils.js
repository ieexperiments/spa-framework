/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapUtils{
	constructor(instance){
		this.instance = instance;
	}
	inViewport(coord){
		let bbox = this.instance.stats.viewport;

		return (Utils.isBetween(coord[0], bbox.minx, bbox.maxx) && Utils.isBetween(coord[1], bbox.miny, bbox.maxy));
	}
	inBuffer(coord){
		let bbox = this.instance.stats.buffer;

		return (Utils.isBetween(coord[0], bbox.minx, bbox.maxx) && Utils.isBetween(coord[1], bbox.miny, bbox.maxy));
	}
	inViewportBuffer(coord){
		return {
			viewport: this.inViewport(coord),
			buffer: this.inBuffer(coord)
		};
	}
}
