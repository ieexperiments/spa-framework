/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapEditorSelector{
	constructor(editor){
		this.editor = editor;
		this.model = editor.model;
		this.map = editor.map;
	}
	getFeatures(evt){
		let point = evt.point;
		let bbox = [[point.x - 5, point.y - 5], [point.x + 5, point.y + 5]];

		let features = this.map.queryRenderedFeatures(point, {layers: ['editorFill']});
		features = features.concat(this.map.queryRenderedFeatures(bbox, {layers: ['editorLine']}));

		this.processFeatures(features);
		//this.model.set('selected', this.selected.list.length);
		this.editor.updateState();
	}
	processFeatures(features){
		let count = features.length;
		let first = false;
		let id = false;

		if(!count){ return; }

		if(count === 1){
			id = features[0].properties.id;
			first = true;

			if(Utils.arrayExists(this.model.selected, id)){
				this.deselect(id);
			} else {
				this.select(id);
			}
		} else {
			for(let feature of features){
				id = feature.properties.id;

				if(!first && !Utils.arrayExists(this.model.selected, id)){
					first = true;
					this.select(id);
				}
			}
		}

		if(!first){
			for(let feature of features){
				id = feature.properties.id;
				this.deselect(id);
			}
		}
	}
	select(id){
		this.model.selected.push(id);
		this.editor.featureSet.setProperty('mapStyle', 3, id);
	}
	deselect(id){
		Utils.arrayRemove(this.model.selected, id);
		this.editor.featureSet.setProperty('mapStyle', 2, id);
	}
	remove(){
		for(let id of this.model.selected){
			this.editor.featureSet.removeFeature(id);
		}

		this.editor.setMode('draw');
	}
	eventHandle(evt){
		this.point = evt.coord;

		switch(evt.type){
			case 'mousedown':
				this.getFeatures(evt);
			break;
		}
	}
}
