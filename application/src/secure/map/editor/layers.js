/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapEditorLayers{
	constructor(editor){
		this.editor = editor;
		this.model = editor.model;
		this.map = editor.map;
		this.instance = editor.instance;
	}
	setData(geojson, popup){
		if(!geojson || !geojson.features){ return; }
		//if(!geojson.features.length){ return; }

		if(popup && popup.data){
			this.instance.datablock.setPopup(this.editor.popup, popup.point, popup.data);
		}

		if(this.model.single){
			geojson.features[0].properties.mapStyle = 2;
		}

		this.map.getSource('editor').setData(geojson);
	}
	add(){
		this.layers = [
			{
				id: 'editorFill',
				type: 'fill',
				source: 'editor',
				fillColor: {property: 'mapStyle', stops: [[1, '#a2b86c'], [2, '#1395ba'], [3, '#0d3c55'], [4, '#c02e1d']]},
				fillOpacity: 0.2,
				filter: ['all', ['==', '$type', 'Polygon'], ['!=', 'edit', 1], ['==', 'valid', 1]],
			},
			{
				id: 'editorLine',
				type: 'line',
				source: 'editor',
				lineColor: {property: 'mapStyle', stops: [[1, '#a2b86c'], [2, '#1395ba'], [3, '#0d3c55'], [4, '#c02e1d']]},
				lineJoin: 'round',
				lineCap: 'round',
				lineWidth: 3,
				lineBlur: 1,
				filter: ['all', ['!=', 'edit', 1]],
			},
			{
				id: 'editorPoint',
				type: 'circle',
				source: 'editor',
				circleColor: {property: 'mapStyle', stops: [[1, '#a2b86c'], [2, '#1395ba'], [3, '#0d3c55'], [4, '#c02e1d']]},
				filter: ['all', ['==', '$type', 'Point'], ['!=', 'edit', 1]]
			}/*,
			{
				id: 'editorLabel',
				type: 'symbol',
				source: 'editor',
				textField: '{distance}',
				textFont: ['DINOffcProRegularArialUnicodeMSRegular'],
				symbolSpacing: 200,
				textOffset: [0, 0.5],
				textPadding: 2,
				textLeterSpacing: 0.05,
				symbolPlacement: 'line',
				textMaxAngle: 100,
				textColor: '#ff0000',
				textAllowOverlap: true,
				textIgnorePlacement: true,
				textOptional: true,
				filter: ['all', ['==', '$type', 'LineString']]
			}*/
		];

		this.instance.sources.add('editor');

		for(let layer of this.layers){
			this.instance.layers.add(layer);
		}
	}
	remove(){
		for(let layer of this.layers){
			this.instance.layers.remove(layer.id);
		}

		this.instance.sources.remove('editor');
	}
}
