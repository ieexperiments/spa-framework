/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapEditorOperations{
	constructor(editor){
		this.editor = editor;
		this.model = editor.model;
	}
	perform(action){
		let selected = this.model.selected;
		let feature1 = this.editor.featureSet.getFeature(selected[0]);
		let feature2 = this.editor.featureSet.getFeature(selected[1]);

		let obj = {
			id1: feature1.id,
			id2: feature2.id,
			type1: feature1.type,
			type2: feature2.type,
			geom1: Topology.read(feature1.geojson).geometry,
			geom2: Topology.read(feature2.geojson).geometry
		};

		switch(action){
			case 'union':
				this.union(obj);
			break;
			case 'diff':
				this.diff(obj);
			break;
			case 'intersect':
				this.intersect(obj);
			break;
			case 'symdiff':
				this.symdiff(obj);
			break;
			case 'bisect':
				this.bisect(obj);
			break;
		}
	}
	complete(obj, features){
		let featureSet = this.editor.featureSet;

		features = Utils.isArray(features) ? features : [features];
		featureSet.removeFeature(obj.id1);
		featureSet.removeFeature(obj.id2);

		for(let geom of features){
			//geom = geom.union(geom);
			let geojson = Topology.write(geom);
			featureSet.importFeature(geojson, {}, this.editor.model);
		}

		this.editor.setMode('draw');
	}
	union(obj){
		let geom = Topology.union(obj.geom1, obj.geom2);
		this.complete(obj, geom.geometries || geom);
	}
	diff(obj){
		let geom = Topology.difference(obj.geom1, obj.geom2);
		this.complete(obj, geom.geometries || geom);
	}
	intersect(obj){
		let geom = Topology.intersect(obj.geom1, obj.geom2);
		this.complete(obj, geom);
	}
	symdiff(obj){
		let geom1 = Topology.difference(obj.geom1, obj.geom2);
		let geom2 = Topology.difference(obj.geom2, obj.geom1);
		this.complete(obj, [geom1, geom2]);
	}
	bisect(obj){
		let line = obj.type1 === 'linestring' ? obj.geom1 : obj.geom2;
		let poly = obj.type1 === 'linestring' ? obj.geom2 : obj.geom1;
		line = line.buffer(0.00001);
		let geom = Topology.difference(poly, line);
		this.complete(obj, geom.geometries);
	}
}
