/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapEditor{
	constructor(instance){
		this.instance = instance;
		this.map = instance.map;
		this.setModel();
		this.featureSet = new FeatureSet();
		this.layers = new MapEditorLayers(this);
		this.designer = new MapEditorDesigner(this);
		this.toolbar = new MapEditorToolbar(this);
		this.selector = new MapEditorSelector(this);
		this.operations = new MapEditorOperations(this);
	}
	enable(opts, layout){
		if(this.active){ this.disable(); }

		this.modelMerge(opts);

		if(layout){
			mapAPI.setLayout('1.0.0.0.a');
			this.toolbar.show();
		}

		this.featureSet.empty();
		this.instance.events.drawing = true;
		this.map.doubleClickZoom.disable();
		this.map.dragPan.disable();
		this.instance.canvasContainer.addClass('cursor-crosshair');
		this.active = true;
		this.layout = layout;
		this.layers.add();
		this.popup = this.instance.datablock.createPopup();
		this.updateState();
		this.designer.start();
	}
	disable(){
		if(!this.active){ return; }
		if(this.layout){ mapAPI.setLayout(this.layout); }
		this.instance.events.drawing = false;
		this.map.doubleClickZoom.enable();
		this.map.dragPan.enable();
		this.instance.canvasContainer.removeClass('cursor-crosshair');
		this.toolbar.hide();
		this.active = false;
		this.layout = undefined;
		this.layers.remove();
		this.featureSet.cancelFeature();
		if(this.featureSet.model.count){
			this.model.onComplete(this.featureSet.geojson());
		} else {
			this.model.onComplete(false);
		}
		this.featureSet.empty();
		this.popup.remove();
	}
	eventHandle(evt){
		let mode = this.model.mode;

		switch(mode){
			case 'draw':
				this.designer.eventHandle(evt);
			break;
			case 'select':
				this.selector.eventHandle(evt);
			break;
		}
	}
	setGetGeomType(){
		let curr = this.model.type;
		let buffer = this.model.buffer;
		let bufferToggle = false;

		if(curr === 'linestring' || curr === 'linebuffer'){
			this.model.set('type', buffer ? 'linebuffer' : 'linestring');
			bufferToggle = true;
		} else if(curr === 'point' || curr === 'pointbuffer'){
			this.model.set('type', buffer ? 'pointbuffer' : 'point');
			bufferToggle = true;
		}

		this.model.set('bufferToggle', bufferToggle);

		return this.model.type;
	}
	updateState(){
		this.model.set('features', this.featureSet.count());
		this.toolbar.checkState();
		this.designer.render();
	}
	setMode(mode, type = this.model.type){
		this.featureSet.cancelFeature();
		this.model.set('mode', mode);

		if(mode === 'draw'){
			this.model.set('type', type);
			this.model.set('selected', []);
			this.model.set('count', this.featureSet.count());
			this.updateState();
			this.designer.start();
		} else {
			this.updateState();
		}
	}
	modelMerge(opts){
		opts = Object.assign({
			onComplete: function(){},
			onPoint: function(){},
			features: 0,
			selected: [],
			measure: false,
			buffer: false,
			single: false,
			segments: false,
			units: 'nm',
			radius: 5,
			type: 'polygon'
		}, opts);

		for(let [key, value] of Object.entries(opts)){
			this.model.set(key, value);
		}

		if(opts.type === 'linebuffer' || opts.type === 'pointbuffer'){
			this.model.set('buffer', true);
		}
	}
	setModel(){
		this.model = kendo.observable({
			mode: 'draw',
			features: 0,
			selected: [],
			type: 'polygon',
			radius: 5,
			units: 'nm',
			buffer: false,
			measure: false,
			bufferToggle: false,
			updateMeasures: function(key, value){
				this.featureSet.updateMeasures(key, value);
			}.bind(this)
		});
	}
}
