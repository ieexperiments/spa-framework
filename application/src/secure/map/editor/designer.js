/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapEditorDesigner{
	constructor(editor){
		this.editor = editor;
		this.model = editor.model;
		this.featureSet = editor.featureSet;
	}
	start(){
		this.origin = false;
		this.segments = 0;
		this.gtype = this.editor.setGetGeomType();
		this.featureSet.addFeature(this.gtype, {}, this.model);
	}
	end(){
		this.featureSet.endFeature();
		this.editor.updateState();

		if(this.model.single){
			this.editor.disable();
		} else {
			this.start();
		}
	}
	mousedown(evt){
		this.model.onPoint(evt.coord);

		if(this.gtype === 'point' || this.gtype === 'pointbuffer'){
			this.featureSet.addPoint(evt.coord);
			this.render();
			this.end();
			return;
		}

		if(this.gtype === 'circle' && this.origin){
			let hash1 = this.origin.join(':');
			let hash2 = evt.coord.join(':');

			console.log(hash1, hash2);

			if(hash1 === hash2){
				console.log('dump');
				this.featureSet.cancelFeature();
				this.render();
				this.start();
			} else {
				console.log('keep');
				this.featureSet.addPoint(evt.coord);
				this.end();
			}

			return;
		}

		if(!this.origin){
			console.log('setting origin');
			this.origin = evt.coord;
		} else {
			this.segments++;
		}

		this.featureSet.addPoint(evt.coord);
		this.render();

		if(this.model.segments && this.model.segments === this.segments){
			this.end();
		}
	}
	mousemove(evt){
		if(!this.origin){ return; }

		this.render({append: evt.coord});
	}
	dblclick(evt){
		console.log('dble');
		if(this.gtype === 'circle'){ return; }
		this.end();
	}
	render(opts){
		let geojson = this.featureSet.geojson(opts);
		let popup = false;

		if(this.model.measure){
			popup = {point: this.point, data: this.featureSet.distance};
		}

		this.editor.layers.setData(geojson, popup);
	}
	eventHandle(evt){
		this.point = evt.coord;

		switch(evt.type){
			case 'mousedown':
				this.mousedown(evt);
			break;
			case 'mousemove':
				this.mousemove(evt);
			break;
			case 'dblclick':
				this.dblclick(evt);
			break;
		}
	}
}
