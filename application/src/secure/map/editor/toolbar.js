/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapEditorToolbar{
	constructor(editor){
		this.editor = editor;
		this.model = editor.model;
	}
	show(){
		let container = mapAPI.view.panels.header;
		container.empty();
		container.show();
		this.setView(container);
		this.editor.map.resize();
	}
	hide(){
		let container = mapAPI.view.panels.header;
		container.empty();
		container.hide();
		this.view = false;
		this.editor.map.resize();
	}
	checkState(){
		let mode = this.model.mode;
		let counts = {
			features: this.model.features,
			selected: this.model.selected.length
		};

		if(this.model.mode === 'draw'){
			this.toggle(this.model.type, true);
		}

		this.enable('remove', (counts.selected ? true : false));
		this.enable('select', (counts.features ? true : false));
		this.enable('save', (counts.features ? true : false));
		this.disable(['union','diff','intersect','symdiff','bisect']);

		if(counts.selected === 2){
			this.checkSelected();
		}
	}
	checkSelected(){
		let selected = this.model.selected;
		let feature1 = this.editor.featureSet.getFeature(selected[0]);
		let feature2 = this.editor.featureSet.getFeature(selected[1]);
		let key = feature1.type + ':' + feature2.type;

		switch(key){
			case 'polygon:polygon':
			case 'polygon:circle':
			case 'polygon:linebuffer':
			case 'polygon:pointbuffer':

			case 'circle:polygon':
			case 'circle:circle':
			case 'circle:linebuffer':
			case 'circle:pointbuffer':

			case 'linebuffer:polygon':
			case 'linebuffer:circle':
			case 'linebuffer:linebuffer':
			case 'linebuffer:pointbuffer':

			case 'pointbuffer:polygon':
			case 'pointbuffer:circle':
			case 'pointbuffer:linebuffer':
			case 'pointbuffer:pointbuffer':
				this.checkRelate(feature1, feature2);
			break;
			case 'polygon:linestring':
			case 'polygon:linestring':
			case 'polygon:linestring':
			case 'polygon:linestring':

			case 'circle:linestring':
			case 'circle:linestring':
			case 'circle:linestring':
			case 'circle:linestring':

			case 'linebuffer:linestring':
			case 'linebuffer:linestring':
			case 'linebuffer:linestring':
			case 'linebuffer:linestring':

			case 'pointbuffer:linestring':
			case 'pointbuffer:linestring':
			case 'pointbuffer:linestring':
			case 'pointbuffer:linestring':

			case 'linestring:polygon':
			case 'linestring:circle':
			case 'linestring:linebuffer':
			case 'linestring:pointbuffer':
				this.checkLinePoly(key, feature1, feature2);
			break;
		}
	}
	checkRelate(feature1, feature2){
		let geom1 = Topology.read(feature1.geojson).geometry;
		let geom2 = Topology.read(feature2.geojson).geometry;
		let relate = geom1.relate(geom2);
		let overlap = relate.isIntersects();

		/*
		console.log('overlap', relate.isOverlaps());
		console.log('touch', relate.isTouches());
		console.log('crosses', relate.isCrosses());
		console.log('intersect', relate.isIntersects());
		console.log('disjoint', relate.isDisjoint());
		console.log(relate);
		*/
		
		if(overlap){
			this.enable(['union','diff','intersect','symdiff']);
		}
	}
	checkLinePoly(key, feature1, feature2){
		let types = key.split(':');
		let geom1 = key[0] === 'linestring' ? feature1 : feature2;
		let geom2 = key[0] === 'linestring' ? feature2 : feature1;
		let line = Topology.read(geom1.geojson).geometry;
		let poly = Topology.read(geom2.geojson).geometry;
		let pt1 = line.getStartPoint();
		let pt2 = line.getEndPoint();
		let rel1 = pt1.within(poly);
		let rel2 = pt2.within(poly);

		if(!rel1 && !rel2){
			this.enable('bisect');
		}
	}
	click(evt){
		let action = evt.id;
		evt.target.blur();

		switch(action){
			case 'save':
			case 'close':
				this.editor.disable();
			break;
			case 'polygon':
			case 'linestring':
			case 'circle':
			case 'point':
			case 'buffer':
			case 'pointbuffer':
				this.editor.setMode('draw', action);
				this.toggle('select', false);
			break;
			case 'select':
				this.editor.setMode('select', action);
			break;
			case 'remove':
				this.editor.selector.remove();
			break;
			case 'union':
			case 'diff':
			case 'intersect':
			case 'symdiff':
			case 'bisect':
				this.editor.operations.perform(action);
			break;
		}
	}
	enable(arr, bool = true){
		if(!this.view){ return; }
		arr = Utils.isArray(arr) ? arr : [arr];

		for(let id of arr){
			let selector = '#' + id;
			this.toolbar1.enable(selector, bool);
			this.toolbar2.enable(selector, bool);
		}
	}
	disable(arr){
		this.enable(arr, false);
	}
	toggle(id, bool = false){
		if(!id || !this.view){ return; }

		id = id === 'linebuffer' ? 'linestring' : id;
		id = id === 'pointbuffer' ? 'point' : id;
		let selector = '#' + id;

		this.toolbar1.toggle(selector, bool);
		this.toolbar2.toggle(selector, bool);
	}
	onChange(evt){
		let key = evt.sender.element.attr('id');
		let value = evt.sender.value() || evt.checked;
		let gtype = this.model.type;
		this.model.updateMeasures(key, value);

		if(key === 'buffer'){
			this.editor.setGetGeomType();
		}
	}
	setView(container){
		let change = function(evt){ this.onChange(evt); }.bind(this);
		let toolbar1 = {
			key: 'toolbar1',
			margin: '0 10px',
			click: function(evt){ this.click(evt); }.bind(this),
			toggle: function(evt){ this.click(evt); }.bind(this),
			items: [
				{type: 'buttonGroup', buttons: [
					{type: 'button', id: 'polygon', text: 'Polygon', group: 'toggle', enable: true, togglable: true, spriteCssClass: 'icon-distort-1'},
					{type: 'button', id: 'linestring', text: 'Line', group: 'toggle', enable: true, togglable: true, spriteCssClass: 'icon-line-7'},
					{type: 'button', id: 'circle', text: 'Circle', group: 'toggle', enable: true, togglable: true, spriteCssClass: 'icon-oval-1'},
					{type: 'button', id: 'point', text: 'Point', group: 'toggle', enable: true, togglable: true, spriteCssClass: 'icon-pin-map-down-7'}
				]},
				{type: 'separator'},
				{type: 'buttonGroup', buttons: [
					{type: 'button', id: 'select', text: 'Select', group: 'toggle', enable: false, togglable: true, spriteCssClass: 'icon-select'},
					{type: 'button', id: 'remove', text: 'Delete', group: 'toggle', enable: false, togglable: false, spriteCssClass: 'icon-bin2'},
					{type: 'button', id: 'save', text: 'Save', group: 'toggle', enable: true, togglable: false, spriteCssClass: 'icon-diskette-7'},
					{type: 'button', id: 'close', text: 'Close', group: 'toggle', enable: true, togglable: false, spriteCssClass: 'icon-door-out-7'}
				]},
				{type: 'separator'},
				{type: 'numeric', id: 'radius', label: 'Radius', bind: 'value:radius,enabled:bufferToggle', change: change, min: 0, max: 999, width: 70, format: '###'},
				{type: 'dropdown', id: 'units', label: 'Units', bind: 'value:units,enabled:bufferToggle', change: change, width: 130, dataSource:[
					{name: 'Nautical Miles', value: 'nm'},
					{name: 'Miles', value: 'mi'},
					{name: 'Kilometers', value: 'km'}
				]},
				{type: 'switch', id: 'buffer', label: 'Buffer', bind: 'checked:buffer,enabled:bufferToggle', change: change}
			]
		};
		let toolbar2 = {
			key: 'toolbar2',
			margin: '0 10px',
			click: function(evt){ this.click(evt); }.bind(this),
			toggle: function(evt){ this.click(evt); }.bind(this),
			items: [
				{type: 'buttonGroup', buttons: [
					{type: 'button', id: 'union', text: 'Union', group: 'ops', enable: false, togglable: false, spriteCssClass: 'icon-union'},
					{type: 'button', id: 'intersect', text: 'Intersect', group: 'ops', enable: false, togglable: false, spriteCssClass: 'icon-intersection'},
					{type: 'button', id: 'diff', text: 'Difference', group: 'ops', enable: false, togglable: false, spriteCssClass: 'icon-difference'},
					{type: 'button', id: 'symdiff', text: 'Symetrical Difference', group: 'ops', enable: false, togglable: false, spriteCssClass: 'icon-sym-diff'},
					{type: 'button', id: 'bisect', text: 'Bisect', group: 'ops', enable: false, togglable: false, spriteCssClass: 'icon-bisect'}
				]}
			]
		};
		let content = [
			{type: 'panelset', opts: {dir: 'column', flex: 'auto', content: [
				{type: 'toolbar', opts: toolbar1},
				{type: 'toolbar', opts: toolbar2},
			]}}
		];

		delete this.model.toolbars;

		this.view = new View({
			model: this.model,
			container: container,
			content: content
		});

		this.toolbar1 = this.view.toolbars.toolbar1;
		this.toolbar2 = this.view.toolbars.toolbar2;
		kendo.bind(container, this.model);
	}
}
