/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapDatablock{
	constructor(instance){
		this.instance = instance;
		this.map = instance.map;
		this.blocks = {};
		this.build();
		this.listeners();
	}
	setBlock(track){
		if(!this.blocks[track.tid]){ this.createBlock(track.tid); }

		let block = this.blocks[track.tid];
		let point = this.map.project(track.point);

		block.point = track.point;
		this.setPosition(block);

		let arr = [track.callsign];
		if(track.dept && track.dest){ arr.push(track.dept + ' to ' + track.dest); }
		if(track.acftType){ arr.push(track.acftType); }
		if(track.mode3){ arr.push(track.mode3); }
		arr.push(track.speed + ' ' + track.heading + ' ' + track.alt);

		block.el.text(arr.join('\n'));
		block.el.show();
	}
	setPosition(block){
		let point = this.map.project(block.point);
		point.y -= 10;
		point.x += 10;

		block.el.css({top: point.y + 'px', left: point.x + 'px'});
	}
	hide(id){
		if(!this.blocks[id]){ return; }

		this.blocks[id].el.hide();
	}
	remove(id){
		if(!this.blocks[id]){ return; }

		this.blocks[id].el.remove();

		delete this.blocks[id];
	}
	createBlock(id){
		this.blocks[id] = {
			el: Dom.el('block', {}, this.container)
		};
	}
	build(){
		this.container = Dom.el('datablocks', {}, this.map.getContainer());
	}
	onMove(){
		for(let [key, block] of Object.entries(this.blocks)){
			this.setPosition(block);
		}
	}
	createPopup(point, data){
		let el = Dom.el('block', {}, this.container);

		if(!point || !data){
			el.hide();
		} else {
			this.setPopup(el, point, data);
		}

		return el;
	}
	setPopup(el, point, data){
		el.text(data);
		el.show();

		this.setPosition({
			el: el,
			point: point
		});
	}
	listeners(){
		this.instance.events.on('move', function(evt){
			this.onMove();
		}.bind(this));
	}
}
