/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapLayers{
	constructor(instance){
		this.instance = instance;
		this.map = instance.map;
		this.registry = [];
		this.setMapping();
	}
	add(item, register = true){
		let layer = {layout: {}, paint: {}};

		for(let [key, value] of Object.entries(item)){
			let def = this.mapping[key];

			if(def){
				if(def.member){
					layer[def.member][def.property] = value;
				} else {
					layer[def.property] = value;
				}
			}
		}

		if(register){
			this.registry.push({id: item.id, data: item});
		}
		this.map.addLayer(layer);
	}
	remove(id){
		let key = false;

		for(let [idx, source] of Object.entries(this.registry)){
			if(source.id === id){ key = idx; }
		}

		if(!key){ return; }

		this.registry.splice(key, 1);
		try{ this.map.removeLayer(id); } catch(err){}
	}
	reload(){
		for(let [key, layer] of Object.entries(this.registry)){
			this.add(layer.data, false);
		}
	}
	setMapping(){
		this.mapping = {
			id: {property: 'id'},
			type: {property: 'type'},
			metadata: {property: 'metadata'},
			ref: {property: 'ref'},
			source: {property: 'source'},
			layer: {property: 'source-layer'},
			minzoom: {property: 'minzoom'},
			maxzoom: {property: 'maxzoom'},
			filter: {property: 'filter'},
			visibility: {member: 'layout', property: 'visibility'},
			lineCap: {member: 'layout', property: 'line-cap'},
			lineJoin: {member: 'layout', property: 'line-join'},
			lineMiterLimit: {member: 'layout', property: 'line-miter-limit'},
			lineRoundLimit: {member: 'layout', property: 'line-round-limit'},
			symbolPlacement: {member: 'layout', property: 'symbol-placement'},
			symbolSpacing: {member: 'layout', property: 'symbol-spacing'},
			symbolAvoidEdges: {member: 'layout', property: 'symbol-avoid-edges'},
			iconAllowOverlap: {member: 'layout', property: 'icon-allow-overlap'},
			iconIgnorePlacement: {member: 'layout', property: 'icon-ignore-placement'},
			iconOptional: {member: 'layout', property: 'icon-optional'},
			iconRotationAlignment: {member: 'layout', property: 'icon-rotation-alignment'},
			iconSize: {member: 'layout', property: 'icon-size'},
			iconImage: {member: 'layout', property: 'icon-image'},
			iconRotate: {member: 'layout', property: 'icon-rotate'},
			iconPadding: {member: 'layout', property: 'icon-padding'},
			iconKeepUpright: {member: 'layout', property: 'icon-keep-upright'},
			iconOffset: {member: 'layout', property: 'icon-offset'},
			textRotationAlignment: {member: 'layout', property: 'text-rotation-alignment'},
			textField: {member: 'layout', property: 'text-field'},
			textFont: {member: 'layout', property: 'text-font'},
			textSize: {member: 'layout', property: 'text-size'},
			textMaxWidth: {member: 'layout', property: 'text-max-width'},
			textLineHeight: {member: 'layout', property: 'text-line-height'},
			textLetterSpacing: {member: 'layout', property: 'text-letter-spacing'},
			textJustify: {member: 'layout', property: 'text-justify'},
			textAnchor: {member: 'layout', property: 'text-anchor'},
			textMaxAngle: {member: 'layout', property: 'text-max-angle'},
			textRotate: {member: 'layout', property: 'text-rotate'},
			textPadding: {member: 'layout', property: 'text-padding'},
			textKeepUpright: {member: 'layout', property: 'text-keep-upright'},
			textTransform: {member: 'layout', property: 'text-transform'},
			textOffset: {member: 'layout', property: 'text-offset'},
			textAllowOverlap: {member: 'layout', property: 'text-allow-overlap'},
			textIgnorePlacement: {member: 'layout', property: 'text-ignore-placement'},
			textOptional: {member: 'layout', property: 'text-optional'},
			backgroundColor: {member: 'paint', property: 'background-color'},
			backgroundPattern: {member: 'paint', property: 'background-pattern'},
			backgroundOpacity: {member: 'paint', property: 'background-opacity'},
			fillAntialias: {member: 'paint', property: 'fill-antialias'},
			fillOpacity: {member: 'paint', property: 'fill-opacity'},
			fillColor: {member: 'paint', property: 'fill-color'},
			fillOutlineColor: {member: 'paint', property: 'fill-outline-color'},
			fillTranslate: {member: 'paint', property: 'fill-translate'},
			fillTranslateAnchor: {member: 'paint', property: 'fill-translate-anchor'},
			fillPattern: {member: 'paint', property: 'fill-pattern'},
			lineOpacity: {member: 'paint', property: 'line-opacity'},
			lineColor: {member: 'paint', property: 'line-color'},
			lineTranslate: {member: 'paint', property: 'line-translate'},
			lineTranslateAnchor: {member: 'paint', property: 'line-translate-anchor'},
			lineWidth: {member: 'paint', property: 'line-width'},
			lineGapWidth: {member: 'paint', property: 'line-gap-width'},
			lineOffset: {member: 'paint', property: 'line-offset'},
			lineBlur: {member: 'paint', property: 'line-blur'},
			lineDasharray: {member: 'paint', property: 'line-dasharray'},
			linePattern: {member: 'paint', property: 'line-pattern'},
			iconOpacity: {member: 'paint', property: 'icon-opacity'},
			iconColor: {member: 'paint', property: 'icon-color'},
			iconHaloColor: {member: 'paint', property: 'icon-halo-color'},
			iconHaloWidth: {member: 'paint', property: 'icon-halo-width'},
			iconHaloBlur: {member: 'paint', property: 'icon-halo-blur'},
			iconTranslate: {member: 'paint', property: 'icon-translate'},
			iconTranslateAnchor: {member: 'paint', property: 'icon-translate-anchor'},
			textOpacity: {member: 'paint', property: 'text-opacity'},
			textColor: {member: 'paint', property: 'text-color'},
			textHaloColor: {member: 'paint', property: 'text-halo-color'},
			textHaloWidth: {member: 'paint', property: 'text-halo-width'},
			textHaloBlur: {member: 'paint', property: 'text-halo-blur'},
			textTranslate: {member: 'paint', property: 'text-translate'},
			textTranslateAnchor: {member: 'paint', property: 'text-translate-anchor'},
			rasterOpacity: {member: 'paint', property: 'raster-opacity'},
			rasterHueRotate: {member: 'paint', property: 'raster-hue-rotate'},
			rasterBrightnessMin: {member: 'paint', property: 'raster-brightness-min'},
			rasterBrightnessMax: {member: 'paint', property: 'raster-brightness-max'},
			rasterSaturation: {member: 'paint', property: 'raster-saturation'},
			rasterContrast: {member: 'paint', property: 'raster-contrast'},
			rasterFadeDuration: {member: 'paint', property: 'raster-fade-duration'},
			circleRadius: {member: 'paint', property: 'circle-radius'},
			circleColor: {member: 'paint', property: 'circle-color'},
			circleBlur: {member: 'paint', property: 'circle-blur'},
			circleOpacity: {member: 'paint', property: 'circle-opacity'},
			circleTranslate: {member: 'paint', property: 'circle-translate'},
			circleTranslateAnchor: {member: 'paint', property: 'circle-translate-anchor'}
		};
	}
}
