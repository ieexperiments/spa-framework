/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Map{
	constructor(opts){
		opts = Object.assign({
			layout: '1.0.0.0.a',
			container: main.view.panels.main,
			center: [-98.579416, 39.828328],
			zoom: 4,
			pitch: 0,
			bearing: 0,
			theme: 'streets'
		}, opts);

		if(!mapboxgl.supported()) {
			alert('Your browser does not support WebGL!');
			return;
		}

		mapboxgl.config.API_URL = 'map';
		mapboxgl.config.REQUIRE_ACCESS_TOKEN = false;

		this.options = opts;
		this.setModel(opts);
		this.setView(opts);
		this.build();
	}
	show(){
		this.options.container.empty();
		this.view.panels.wrapper.appendTo(this.options.container);
		kendo.bind(this.options.container, this.model);
		this.resize();
	}
	build(){
		let props = this.model.toJSON();
		let batch = [];
		this.maps = {0: {}, 1: {}, 2: {}, 3: {}};
		this.focus = Dom.el('mapfocus', {style: 'k-state-focused'});

		for(let [key, instance] of Object.entries(this.maps)){
			batch.push({key: key, instance: instance, props: props});
		}

		Utils.asyncBatch({module: this, method: 'createMap', payload: batch}, function(err, styles){
			this.ready();
		}.bind(this));
	}
	ready(){
		this.on('mousedown', function(evt){ this.selectMap(evt); }.bind(this));
		this.setLayout(this.options.layout);
		main.mapReady();
	}
	createMap(obj, callback){
		let target = this.view.panels['map' + obj.key][0];
		obj.instance.mapIdx = obj.key;

		obj.instance.map = new mapboxgl.Map({
			container: target,
			style: this.getThemePath(),
			pitch: obj.props.pitch,
			bearing: obj.props.bearing,
			zoom: obj.props.zoom,
			center: obj.props.center
		});

		obj.instance.canvasContainer = $(obj.instance.map.getCanvasContainer());
		obj.instance.map.addControl(new mapboxgl.NavigationControl());
		obj.instance.screenshot = new MapScreenshot(obj.instance.map);
		obj.instance.events = new MapEvents(obj.instance);
		obj.instance.sources = new MapSources(obj.instance);
		obj.instance.layers = new MapLayers(obj.instance);
		obj.instance.layerControl = new MapLayerControl(obj.instance);
		obj.instance.editor = new MapEditor(obj.instance);
		obj.instance.datablock = new MapDatablock(obj.instance);
		obj.instance.utils = new MapUtils(obj.instance);

		obj.instance.map.on('load', function(){
			obj.instance.events.setStats();
			obj.instance.layerControl.setValidLayers();
			callback(null);
		});
	}
	selectMap(evt){
		if(this.selected === evt.mapIdx){ return; }

		if(this.selected){
			let previous = this.getSelected();
			previous.selected = false;
		}

		this.selected = evt.mapIdx;
		this.focus.appendTo($(this.maps[evt.mapIdx].map.getContainer()));

		let instance = this.getSelected();

		instance.selected = true;
		instance.events.fire({type: 'select', instance: instance});
	}
	getSelected(){
		return this.maps[this.selected];
	}
	on(event, callback){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.events.on(event, callback);
		}
	}
	getThemePath(theme = this.model.theme){
		let path = 'map/styles/{theme}.json';

		return path.replace('{theme}', theme);
	}
	setLayout(config){
		let panel = this.view.panels.mapWrapper;
		let list = config.split('.');
		let style = 'map-panel-' + list.join('');
		let flex = list.pop();

		this.layout = config;

		for(let item of this.styles){
			panel.removeClass(item);
		}

		panel.addClass(style);
		this.selectMap({mapIdx: 0});
		this.resize();

		for(let [key, instance] of Object.entries(this.maps)){
			instance.visible = list[key] === '1' ? true : false;
		}
	}
	setFilter(layer, filter){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.map.setFilter(layer, filter);
		}
	}
	addSource(id, data){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.sources.add(id, data);
		}
	}
	addLayer(layer){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.layers.add(layer);
		}
	}
	removeLayer(id){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.layers.remove(id);
		}
	}
	setSourceData(id, data){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.map.getSource(id).setData(data);
		}
	}
	resize(){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.map.resize();
		}
	}
	removeDatablock(id){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.datablock.remove(id);
		}
	}
	getActive(){
		let output = [];

		for(let [key, instance] of Object.entries(this.maps)){
			if(instance.visible){
				output.push(instance);
			}
		}

		return output;
	}
	flyTo(point, zoom = 9){
		let map = this.getSelected().map;

		map.flyTo({center: point, zoom: zoom});
	}
	fitBounds(bounds){
		let map = this.getSelected().map;

		map.fitBounds(bounds);
	}
	toggleLayerControl(){
		this.getSelected().layerControl.toggle();
	}
	draw(opts){
		let instance = this.getSelected();

		instance.editor.enable(opts);
	}
	editor(opts = {}){
		let instance = this.maps[0];

		if(instance.editor.active){
			instance.editor.disable();
		} else {
			instance.editor.enable(opts, this.layout);
		}
	}
	bbox(){
		let instance = this.maps[0];

		return instance.stats.viewport;
	}
	disableEditor(){
		for(let [key, instance] of Object.entries(this.maps)){
			instance.editor.disable();
		}
	}
	screenshot(){
		let instance = this.getSelected();

		instance.screenshot.screenshot();
	}
	measure(){
		let instance = this.getSelected();

		instance.editor.enable({
			type: 'linestring',
			single: true,
			measure: true
		});
	}
	setTheme(theme){
		let instance = this.getSelected();

		instance.map.setStyle(this.getThemePath(theme));

		instance.map.once('data', function(evt){
			instance.sources.reload();
			instance.layers.reload();
			instance.layerControl.themeChange();
			instance.events.fire({type: 'themechange'});
		}.bind(this));
	}
	setModel(opts){
		this.model = kendo.observable({
			theme: opts.theme,
			center: opts.center,
			zoom: opts.zoom,
			pitch: opts.pitch,
			bearing: opts.bearing
		});
	}
	setView(opts){
		let content = [
			{type: 'panelset', opts: {key: 'wrapper', dir: 'row', flex: 'auto', items: [
				{flex: 'auto', items: [
					{key: 'header', visible: false, border: true},
					{flex: 'auto', items:[
						{flex: 'auto', key: 'mapWrapper', style: 'map-panel-1000a', items:[
							{key: 'maprow0', style: 'map-row', flex: 'auto', items:[
								{key: 'map0', style: 'map-panel-0 map-col k-content', flex: 'auto'},
								{key: 'map1', style: 'map-panel-1 map-col k-content', flex: 'auto'}
							]},
							{key: 'maprow1', id: 'maprow1', style: 'map-row map-panel-row', flex: 'auto', items:[
								{key: 'map2', style: 'map-panel-2 map-col k-content', flex: 'auto'},
								{key: 'map3', style: 'map-panel-3 map-col k-content', flex: 'auto'}
							]}
						]}
					]},
					{key: 'footer', visible: false, text: 'footer', border: true}
				]}
			]}
		}];

		this.styles = [
			'map-panel-1000a',
			'map-panel-1100a',
			'map-panel-1110a',
			'map-panel-1010a',
			'map-panel-1111a',
			'map-panel-1011a',
			'map-panel-1110b',
			'map-panel-1011b'
		];

		this.view = new View({
			model: this.model,
			container: opts.container,
			content: content
		});

		kendo.bind(opts.container, this.model);
	}
}
