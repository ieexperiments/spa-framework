/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapScreenshot{
	constructor(map){
		this.map = map;
	}
	screenshot(type = 'png'){
		this.map.once('render', function(evt){
			let mime = this.getMimeType(type);
			let name = 'screenshot-' + moment().format('YYYYMMDDHHmmss') + mime.ext;
			let canvas = this.map.getCanvas();
			let href = canvas.toDataURL(mime.format, 1.0).replace(mime.format, 'image/octet-stream');
			let link = Dom.el('a', {href: href, download: name});
			console.log(href);
			link[0].click();

		}.bind(this));

		this.map.resize();
	}
	getMimeType(type){
		let output = false;

		switch(type){
			case 'jpg':
			case 'jpeg':
				output = {format: 'image/jpeg', ext: '.jpg'};
			break;
			default:
				output = {format: 'image/png', ext: '.png'};
			break;
		}

		return output;
	}
}
