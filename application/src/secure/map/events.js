/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class MapEvents{
	constructor(instance){
		this.instance = instance;
		this.map = instance.map;
		this.registry = {};
		this.drawing = false;
		this.listeners();
	}
	listeners(){
		this.map.on('click', function(evt){ this.eventHandle(evt); }.bind(this));
		this.map.on('dblclick', function(evt){ this.eventHandle(evt); }.bind(this));
		this.map.on('mousedown', function(evt){ this.eventHandle(evt); }.bind(this));
		this.map.on('mouseup', function(evt){ this.eventHandle(evt); }.bind(this));
		this.map.on('mousemove', function(evt){ this.eventHandle(evt); }.bind(this));
		this.map.on('movestart', function(evt){ this.eventHandle(evt); }.bind(this));
		this.map.on('moveend', function(evt){ this.eventHandle(evt); }.bind(this));
		this.map.on('move', function(evt){ this.eventHandle(evt); }.bind(this));
		this.map.on('zoom', function(evt){ this.eventHandle(evt); }.bind(this));
	}
	eventHandle(evt){
		this.setStats();

		if(evt.lngLat){ evt.coord = [evt.lngLat.lng, evt.lngLat.lat]; }
		if(evt.type === 'movestart'){ this.instance.moving = true; }
		if(evt.type === 'moveend'){ this.instance.moving = false; }
		
		if(this.drawing){
			//evt.originalEvent.preventDefault();
			this.instance.editor.eventHandle(evt);
			return;
		}

		evt.mapIdx = this.instance.mapIdx;
		evt.instance = this.instance;

		this.fire(evt);
	}
	setStats(){
		let bounds = this.map.getBounds().toArray();
		let stats = {
			zoom: this.map.getZoom(),
			viewport: {
				minx: bounds[0][0],
				miny: bounds[0][1],
				maxx: bounds[1][0],
				maxy: bounds[1][1]
			}
		};

		stats.buffer = {
			minx: stats.viewport.minx - 0.25,
			miny: stats.viewport.miny - 0.25,
			maxx: stats.viewport.maxx + 0.25,
			maxy: stats.viewport.maxy + 0.25
		};

		this.instance.stats = stats;
	}
	on(event, callback){
		if(!this.registry[event]){ this.registry[event] = []; }

		this.registry[event].push(callback);
	}
	fire(evt){
		let events = this.registry[evt.type];

		if(!events){ return; }

		for(let event of events){
			event(evt);
		}
	}
}
