/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Tracker{
	constructor(){
		this.tracks = {};
	}
	feed(type, data){
		let msg = {data: data};

		switch(type){
			case 1:
				msg.type = 'trackHeader';
			break;
			case 2:
				msg.type = 'trackMerge';
			break;
			case 3:
				msg.type = 'trackRemove';
			break;
		}

		postMessage(msg);
	}
}
