/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Parser{
	constructor(){
		this.reader = new BinaryReader();

		this.queue = async.queue(function(msg, callback){
			this.process(msg.buffer, msg.timestamp);
			callback();
		}.bind(this), 1);
	}
	enqueue(buffer, timestamp){
		this.queue.push({buffer: buffer, timestamp: timestamp});
	}
	process(buffer, timestamp){
		let view = this.reader;
		let type = false;

		view.setView(buffer);
		type = view.uint8();

		switch(type){
			case 1:
				this.readHeader(view);
			break;
			case 2:
				this.readTracks(view);
			break;
			case 3:
				this.readDeletes(view);
			break;
		}
	}
	readTracks(view){
		let arr = [];

		while(!view.eof){
			this.parseTracks(view, arr);
		}

		Worker.Tracker.feed(2, arr);
	}
	parseTracks(view, arr){
		let tid = view.uint16();
		let lon = view.uint24();
		let lat = view.uint24();
		let sph = view.uint32();

		let obj = {
			tid: tid,
			speed: BitSubstr(sph, 12, 1),
			heading: BitSubstr(sph, 21, 13),
			alt: BitSubstr(sph, 31, 22),
			point: DecodePoint([lon, lat])
		};

		//obj.bearing = getTrueBearing(obj.heading, obj.point);
		obj.bearing = obj.heading;
		arr.push(obj);
	}
	readDeletes(view){
		let arr = [];

		while(!view.eof){
			this.parseDeletes(view, arr);
		}

		Worker.Tracker.feed(3, arr);
	}
	parseDeletes(view, arr){
		arr.push(view.uint16());
	}
	readHeader(view){
		let arr = [];

		while(!view.eof){
			this.parseHeader(view, arr);
		}

		Worker.Tracker.feed(1, arr);
	}
	parseHeader(view, arr){
		let obj = {
			tid: view.uint16(),
			mode3: view.uint16(),
			type: view.uint8(),
			callsign: view.string(7).trim(),
			dept: view.string(4).trim(),
			dest: view.string(4).trim(),
			acftType: view.string(4).trim()
		};

		arr.push(obj);
	}
}
