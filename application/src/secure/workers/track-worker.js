/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class TrackWorker{
	constructor(){
		this.Tracker = new Tracker();
		this.Parser = new Parser();
		this.startFeed();
	}
	startFeed(){
		let conn = {
			port: 8090,
			onMessage: function(a, b){ this.Parser.enqueue(a, b); }.bind(this)
		};

		this.feed = new Websocket(conn);
	}
	processMessage(msg){
		console.log(msg);
	}
}
