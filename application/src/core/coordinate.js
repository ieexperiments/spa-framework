/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Coordinate = {
	deg2dms(point, format = 'dms1', precision = 2, spaces = false, pad = true){
		let lon = this.parseDeg(point[0], precision, true);
		let lat = this.parseDeg(point[1], precision, false);

		lon = this.format(lon, format, precision, spaces, pad, true);
		lat = this.format(lat, format, precision, spaces, pad, false);

		return {lon: lon, lat: lat};
	},
	format(obj, format, precision, spaces, pad, lon){
		let template = this.formats[format].template;
		let dir = obj.neg ? (lon ? 'W' : 'S') : (lon ? 'E' : 'N');

		if(pad){
			obj.deg = Utils.lpad(obj.deg, '0', (lon ? 3 : 2));
			obj.min = Utils.lpad(obj.min, '0', 2);
			obj.sec = Utils.lpad(obj.sec, '0', 2);
			obj.frac = Utils.rpad(obj.frac, '0', precision);
		}

		template = template.replace('{dir}', dir);
		template = template.replace(/\{sp\}/g, (spaces ? ' ' : ''));
		template = template.replace('{d}', obj.deg);
		template = template.replace('{m}', obj.min);
		template = template.replace('{s}', obj.sec);
		template = template.replace('{f}', obj.frac);

		return template;
	},
	parseDeg(value, precision, lon){
		if(lon){ value = this.wrap(value, 180, -180); }

		let abs = Math.abs(Math.round(value * 1000000));
		let max = (lon ? 180 : 90);

		if(abs > (max * 1000000)){ return NaN; }

		let neg = value < 0 ? true : false;
		let dec = abs % 1000000 / 1000000;
		let deg = Math.floor(abs / 1000000);
		let min = Math.floor(dec * 60);
		let sec = (dec - min / 60) * 3600;

		if(sec > 60){
			min++;
			sec = sec - 60;
		}

		if(min > 60){
			deg++;
			min = min - 60;
		}

		let frac = Math.round((sec - Math.floor(sec)) * Math.pow(10, precision));
		sec = Math.floor(sec);

		return {neg: neg, deg: deg, min: min, sec: sec, frac: frac};
	},
	dms2deg(value){

	},
	wrap(n, min = -180, max = 180){
		const d = max - min;
		const w = ((n - min) % d + d) % d + min;
		return (w === min) ? max : w;
	},
	formats: {
		dms1: {template: '{d}°{sp}{m}\'{sp}{s}.{f}"{dir}', regexp: ''},
		dms2: {template: '{d}°{sp}{m}\'{sp}{s}"{dir}', regexp: ''}
	}
};
/*
$(document).ready(function(){
	Coordinate.deg2dms([-24.72504500749274, 58.74554729994484], 'dms1', 4, false);
	Coordinate.deg2dms([-24.72504500749274, 58.74554729994484], 'dms2', 4, false);
});
*/
