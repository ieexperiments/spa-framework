/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Spinner{
	constructor(opts, view = false){
		this.opts = Object.assign({animate: true, duration: 500, active: false, container: 'body'}, opts);

		this.spinner = Dom.el('spinner', {}, opts.container);
		let orb = Dom.el('orb', {}, this.spinner);
		Dom.el('bounce1', {}, orb);
		Dom.el('bounce2', {}, orb);

		if(opts.active){
			this.start();
		} else {
			this.spinner.hide();
		}

		if(view){ view.register('spinners', opts.key, this); }
	}
	start(){
		this.spinner.show();
	}
	stop(){
		if(this.opts.animate){
			this.spinner.fadeOut(this.opts.duration);
		} else {
			this.spinner.hide();
		}
	}
	destroy(){
		if(this.opts.animate){
			this.spinner.fadeOut(this.opts.duration, function(){ $(this).remove(); });
		} else {
			this.spinner.remove();
		}
	}
}
