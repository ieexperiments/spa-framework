/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class PanelMenu{
	constructor(opts, view = false){
		this.opts = opts;
		this.create();

		if(view){ view.register('panelMenus', opts.key, this); }
	}
	create(){
		this.opts.container.empty();
		this.panel = Dom.el('div', {class: 'panel-menu', }, this.opts.container);

		//if(!app.account.hasAnyRole(item.roles)){ return; }

		for(let item of this.opts.config){
			switch(item.type){
				case 'section':
					this.addSection(item);
				break;
				case 'menu':
					this.addMenu(item);
				break;
				case 'themepicker':
					this.addThemePicker(item);
				break;
			}
		}
	}
	addSection(section){
		let el = Dom.el('div', {text: section.text, open: 1, class: 'k-header block section'}, this.panel);
		Dom.el('div', {class: 'expand icon-circle-down'}, el);

		el.on('click', function(evt){ this.toggleSection(evt); }.bind(this));
	}
	toggleSection(evt){
		let section = $(evt.currentTarget);
		let icon = $(section.children(':first'));
		let next = $(section.next());
		let open = icon.hasClass('icon-circle-down');

		icon.removeClass('icon-circle-down');
		icon.removeClass('icon-circle-up');

		if(open){
			next.hide();
			icon.addClass('icon-circle-up');
		} else {
			next.show();
			icon.addClass('icon-circle-down');
		}
	}
	addMenu(menu){
		let block = Dom.el('div', {class: 'k-content block menu'}, this.panel);
		let ul = Dom.el('ul', {}, block);

		ul.kendoMenu({
			orientation: 'vertical',
			dataSource: menu.items,
			select: function(evt){ this.select(evt); }.bind(this)
		}).data('kendoMenu');

		ul.css('border', 'none');
	}
	addThemePicker(item){
		let el = Dom.el('select', {bind: 'value:theme', style: 'margin:10px'}, this.panel);

		el.kendoDropDownList({
			dataTextField: 'name',
			dataValueField: 'value',
			valuePrimitive: true,
			dataSource: LookupData.themes,
			change(evt){ app.setTheme(); }
		});
	}
	select(evt){
		Route(Utils.menuDataItem(evt));
		/*
		let item = $(evt.item);
		let menu = item.closest('.k-menu');
		let dataItem = evt.sender.options.dataSource;

		let index = item.parentsUntil(menu, '.k-item').map(function(){
			return $(this).index();
		}).get().reverse();

		index.push(item.index());

		for(let idx = -1, len = index.length; ++idx < len;){
			dataItem = dataItem[index[idx]];
			dataItem = idx < (len - 1) ? dataItem.items : dataItem;
		}

		Route({el: item, dataItem: dataItem});
		*/
	}
}
