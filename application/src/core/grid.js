/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Grid{
	constructor(opts, view = false){
		this.opts = opts;
		this.build(opts);

		if(view){ view.register('grids', opts.key, this); }
	}
	refresh(){
		this.grid.refresh();
	}
	read(){
		this.grid.dataSource.read();
		this.refresh();
	}
	autoFit(idx){
		if(!idx){
			for(let [idx, item] of this.opts.columns.entries()){
				this.grid.autoFitColumn(idx);
			}
		} else {
			this.grid.autoFitColumn(this.getIndex(idx));
		}
	}
	hideColumn(idx){
		this.grid.hideColumn(this.getIndex(idx));
	}
	showColumn(idx){
		this.grid.showColumn(this.getIndex(idx));
	}
	lockColumn(idx){
		this.grid.lockColumn(this.getIndex(idx));
	}
	unlockColumn(idx){
		this.grid.unlockColumn(this.getIndex(idx));
	}
	selectRow(idx){
		this.grid.select('tr:eq(' + this.getIndex(idx) + ')');
	}
	selectRows(arr){
		let selectors = [];

		if(!arr.length){ return; }

		for(let idx in arr){
			selectors.push('tr:eq(' + this.getIndex(idx) + ')');
		}

		this.grid.select(selectors.join(','));
	}
	setDataSource(data){
		this.opts.data = data;
		this.grid.dataSource.data(data);
		this.grid.dataSource.page(1);
		if(this.opts.autoFit){ this.autoFit(); }
	}
	getIndex(idx = 1){
		idx--;
		return idx;
	}
	getOptions(){
		return this.grid.getOptions();
	}
	setOptions(opts){
		this.grid.setOptions(opts);
	}
	appendTo(container){
		this.wrapper.appendTo(container);
	}
	build(opts){
		if(opts.editable && typeof(opts.navigatable) !== 'boolean'){
			opts.navigatable = true;
		}

		let options = {
			autoBind: opts.autoBind || false,
			allowCopy: opts.allowCopy || false,
			columnMenu: opts.columnMenu | false,
			columns: opts.columnsOveride || this.getColumns(),
			dataSource: this.getDataSource(),
			editable: opts.editable || false,
			filterable: opts.filterable || false,
			groupable: opts.groupable || false,
			navigatable: opts.navigatable || false,
			noRecords: opts.hasOwnProperty('noRecords') ? opts.noRecords : true,
			pageable: this.getPaging(),
			reorderable: opts.reorderable,
			resizable: opts.hasOwnProperty('resizable') ? opts.resizable : true,
			rowTemplate: opts.rowTemplate,
			scrollable: opts.hasOwnProperty('scrollable') ? opts.scrollable : true,
			selectable: opts.selectable,
			sortable: opts.sortable,
			toolbar: opts.toolbar,
			change: function(){ this.getSelected(); }.bind(this)
		};

		this.wrapper = Dom.el('div', {style: 'grid1'}, opts.container);
		this.container = Dom.el('div', {style: 'grid2'}, this.wrapper);
		this.grid = this.container.kendoGrid(options).data('kendoGrid');

		if(!this.opts.border){ this.container.css('border', 'none'); }
		if(opts.autoFit){ this.autoFit(); }

		this.resize();

		$(window).resize(function(){
			this.resize();
		}.bind(this));
	}
	resize(){
		this.grid.resize();
	}
	getSelected(evt){
		let selected = this.grid.select();
		let output = false;

		if(!this.opts.onSelect){ return; }

		if(selected.length === 1){
			let el = selected[0];
			let dataItem = this.grid.dataItem(el);
			output = {
				el: el,
				data: dataItem.toJSON(),
				dataItem: dataItem
			};
		} else {
			output = {el: [], data: [], dataItem: []};

			for(idx = 0; idx < selected.length; idx++){
				let el = selected[idx];
				let dataItem = this.grid.dataItem(el);

				output.el.push(el);
				output.data.push(dataItem.toJSON());
				output.dataItem.push(dataItem);
			}
		}

		this.opts.onSelect(output);
	}
	getPaging(){
		let opts = this.opts.paging;

		if(!opts){ return false; }

		if(typeof(opts.dropdown) === 'boolean' && opts.dropdown){
			opts.dropdown = [50, 100, 150, 200, 250];
		}

		let obj = {
			pageSize: opts.size || 100,
			previousNext: opts.prevNext,
			numeric: opts.hasOwnProperty('numeric') ? opts.numeric : true,
			buttonCount: opts.buttonCount || 5,
			input: opts.input || false,
			refresh: opts.refresh || false,
			info: opts.hasOwnProperty('info') ? opts.numeric : true,
			pageSizes: opts.dropdown || false
		};

		return obj;
	}
	getDirty(){
		let output = [];
		let data = this.grid.dataSource.data();

		if(!this.grid.dataSource.hasChanges()){ return output; }

		let dirty = $.grep(data, function(item){
			return item.dirty;
		});

		for(let item of dirty){
			output.push({
				data: item.toJSON(),
				dataItem: item
			});
		}

		return output;
	}
	getDataSource(){
		if(this.opts.dataSource){
			return this.opts.dataSource;
		}

		if(!this.opts.data && !this.opts.transport){ return []; }

		let options = {
			data: this.opts.data,
			sort: this.opts.sort,
			pageSize: this.opts.paging.size || 100,
			schema: this.getSchema(),
			batch: true
		};

		if(this.opts.transport){
			options.transport = {read: this.opts.transport};
			options.serverPaging = true;
			options.serverFiltering = true;
			options.serverSorting = true;
		}

		let dataSource = new kendo.data.DataSource(options);
		delete this.opts.data;
		return dataSource;
	}
	getSchema(){
		let model = {fields: {}};

		for(let item of this.opts.columns){
			model.fields[item.field] = {type: item.type || 'string'};
		}

		let schema = {model: model};

		if(this.opts.transport){
			schema.data = 'data';
			schema.total = 'total';
		}

		return schema;
	}
	getColumns(){
		let arr = [];

		for(let item of this.opts.columns){
			let obj = {
				field: item.field,
				title: item.title || item.text,
				filterable: item.hasOwnProperty('filterable') ? item.filterable : true,
				groupable: item.hasOwnProperty('groupable') ? item.groupable : true,
				hidden: item.hasOwnProperty('hidden') ? item.hidden : false,
				locked: item.hasOwnProperty('locked') ? item.locked : false,
				lockable: item.hasOwnProperty('lockable') ? item.lockable : true,
				sortable: item.hasOwnProperty('sortable') ? item.sortable : true,
				menu: item.hasOwnProperty('menu') ? item.menu : true,
				command: item.hasOwnProperty('command') ? item.command : undefined
			};

			switch(item.type){
				case 'date':
					item.format = item.format || '{0:MMM dd, yyyy}';
				break;
				case 'datetime':
					item.type = 'date';
					item.format = item.format || '{0:MMM dd, yyyy hh:mm:ss tt}';
				break;
				case 'number':
					item.format = item.format || '{0:n}';
				break;
			}

			if(this.opts.editable){
				if(item.type === 'boolean' && !item.template){
					item.template = '<input type="checkbox" #= ' + item.field + ' ? \'checked="checked"\' : "" # />';
				}
			}

			if(item.groupable){ obj.groupable = item.groupable; }
			if(item.attributes){ obj.attributes = item.attributes; }
			if(item.aggregates){ obj.aggregates = item.aggregates; }
			if(item.groupHeaderTemplate){ obj.groupHeaderTemplate = item.groupHeaderTemplate; }
			if(item.groupFooterTemplate){ obj.groupFooterTemplate = item.groupFooterTemplate; }
			if(item.headerAttributes){ obj.headerAttributes = item.headerAttributes; }
			if(item.footerAttributes){ obj.footerAttributes = item.footerAttributes; }
			if(item.headerTemplate){ obj.headerTemplate = item.headerTemplate; }
			if(item.footerTemplate){ obj.footerTemplate = item.footerTemplate; }
			if(item.minScreenWidth){ obj.minScreenWidth = item.minScreenWidth; }
			if(item.width || item.w){ obj.width = item.width || item.w; }
			if(item.template){ obj.template = item.template; }
			if(item.values){ obj.values = item.values; }
			if(item.format){ obj.format = item.format; }

			arr.push(obj);
		}

		arr.push({});

		return arr;
	}
}
