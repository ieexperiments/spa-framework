/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Clock{
	constructor(opts, view = false){
		this.opts = Object.assign({format: 'dddd, MMMM Do YYYY, HH:mm:ss', active: true}, opts);
		this.timer = false;
		this.clock = Dom.el('clock', {}, this.opts.container);

		if(this.opts.active){
			this.setTime(new Date());
			this.start();
		}

		if(view){ view.register('clocks', opts.key, this); }
	}
	setTime(date){
		this.clock.text(moment(date).format(this.opts.format));
	}
	start(){
		this.stop();

		this.timer = setInterval(function(){
			this.setTime(new Date());
		}.bind(this), 1000);
	}
	stop(){
		clearTimeout(this.timer);
	}
}
