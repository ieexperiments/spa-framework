/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Notification{
	constructor(opts, view){
		let location = opts.location;
		delete opts.location;

		this.types = {success: true, error: true, info: true, warning: true};
		this.single = opts.single;
		this.wrapper = opts.container;
		this.name = opts.name;
		this.setTemplates(opts);
		this.create(opts, location);

		if(view){ view.register('notifications', opts.key, this); }
	}
	create(opts, location){
		let options = Kendo.getOptions(opts, 'kendoNotification');
		let wrapper = Dom.el('notification', opts, opts.container);
		let notifier = Dom.el('div', {}, wrapper);
		options.appendTo = wrapper;

		if(location){
			wrapper.css({
				position: 'absolute',
				top: location.top,
				left: location.left,
				right: location.right,
				bottom: location.bottom
			});
		}

		this.notification = notifier.kendoNotification(options).data('kendoNotification');
	}
	setTemplates(opts){
		let styles = ['alert', 'success', 'info'];

		opts.templates = [];

		for(let type in styles){
			let wrapper = Dom.el('div', {});
			let div = Dom.el('div', {class: 'notify notify-' + type}, wrapper);
			Dom.el('div', {text: '#= header #', class: 'header'}, div);
			Dom.el('div', {text: '#= message #', class: 'message'}, div);

			opts.templates.push({type: type, template: wrapper.html()});
		}
	}
	show(opts){
		opts = Object.assign({type: 'info'}, opts);

		if(this.single){ this.hide(); }
		if(!this.types[opts.type]){ opts.type = 'info'; }
		this.notification.show(opts.message, opts.type);
	}
	hide(opts){
		this.notification.hide();
	}
	resizeWindow(){
		$(window).trigger('resize');
	}
}
