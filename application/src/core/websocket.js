
/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Websocket{
	constructor(options = {}){
		this.timer = 0;
		this.open(options);
	}
	createSocket(){
		let uri = Utils.websocketURI(this.options);

		this.socket = new WebSocket(uri);
		this.socket.binaryType = 'arraybuffer';
		this.socket.onclose = function(evt){ this.onClose(evt); }.bind(this);
		this.socket.onmessage = function(evt){ this.onMessage(evt); }.bind(this);
	}
	onClose(evt){
		this.options.onClose();
		this.timer = setTimeout(function(){ this.open(); }.bind(this), 5000);
	}
	onMessage(evt){
		this.options.onMessage(evt.data, evt.timeStamp);
	}
	send(msg){
		this.socket.send(JSON.stringify(msg));
	}
	close(){
		if(!this.socket){ return; }

		clearTimeout(this.timer);
		this.socket.onclose = function(){};
		this.socket.close();
	}
	open(options){
		this.options = options || this.options;
		this.options.onClose = this.options.onClose || function(){};
		this.options.onMessage = this.options.onMessage || function(){};

		this.close();
		this.createSocket();
	}
}
