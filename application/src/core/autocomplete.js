/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Autocomplete{
	constructor(opts, view = false){
		opts = Object.assign({type: 'text'}, opts);
		let options = Kendo.getOptions(opts, 'kendoAutoComplete');

		this.setOptions(options, opts);

		let wrapper = Dom.el('input', opts, opts.container);
		this.searcher = wrapper.kendoAutoComplete(options).data('kendoAutoComplete');

		if(view){ view.register('search', opts.key, this); }
	}
	getData(parms){
		let str = parms.data.filter.filters[0].value;

		Utils.ajax({
			route: this.route,
			data: {str: str},
			success: function(data){
				parms.success(data);
			}.bind(this),
			error: function(err){
				parms.error();
			}
		});
	}
	setOptions(options, opts){
		this.route = options.ajax;
		this.onSelect = opts.onSelect || function(){};

		delete opts.onSelect;

		options.dataSource = {
			group: opts.groupKey ? {field: opts.groupKey} : undefined,
			transport: {
				read: function(parms){ this.getData(parms); }.bind(this),
				dataType: 'json'
			},
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true
		};

		options.select = function(evt){
			let value = evt.dataItem.value;

			try{ value = value.toJSON(); } catch(err){}

			this.searcher.close();
			this.onSelect(value);
		}.bind(this);
	}
}
