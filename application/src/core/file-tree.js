/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class FileTree{
	constructor(opts, view = false){
		opts = Object.assign({root: 'Root Folder'}, opts);

		this.fsName = opts.fs;
		this.rootName = opts.root;
		this.setModel();
		this.setView(opts);
		this.createDropZone();
		this.createTree(opts);
		this.getData(opts);

		if(view){ view.register('filetrees', opts.key, this); }
	}
	fileLoad(type){
		//let url = 'chrome-extension://gbkeegbaiigmenfmjfclcdgdpimamgkj/views/app.html?http://localhost:8080/';
		let url = (type === 1 ? 'filePreview' : 'fileDownload') + '?uuid=' + this.model.node.uuid;

		this.iframe.setSrc(url);

	}
	nameChange(){
		let data = this.model.node;
		let name = data.text || 'untitled';
		let regex = new RegExp('^.*' + data.ext + '$');

		if(!name.match(regex) && data.ext){
			name = name + data.ext;
		}

		let sort = data.folder;
		this.model.set('node.text', name);
		this.tree.setText(data.uuid, name, sort);

		Utils.ajax({
			route: 'fsRename',
			data: {uuid: data.uuid, text: name},
			success: function(){}
		});
	}
	setModel(){
		this.model = kendo.observable({
			node: {},
			showGrid: false,
			showDetail: false,
			showPreview: false,
			showButtons: false,
			filePreview: function(){ this.fileLoad(1); }.bind(this),
			fileDownload: function(){ this.fileLoad(2); }.bind(this),
			nameChange: function(){ this.nameChange(); }.bind(this)
		});
	}
	setView(opts){
		let content = [
			{type: 'panelset', opts: {key: 'container', dir: 'row', flex: 'auto', items: [
				{key: 'tree', minWidth: 200},
				{border: true, key: 'right', flex: 'auto', items: [
					{key: 'details', bind: 'visible:showDetail', align: 'center', border: true, padding: '5px 10px', fontSize: '1.1em', items: [
						{text: 'Name', bold: true, padding: '0 10px 0 0'},
						{padding: '0 10px 0 0', content: [
							{type: 'inputText', opts: {key: 'node.text', id: 'fsfilename', change: 'nameChange'}}
						]},
						{text: 'Size', bold: true, padding: '0 10px 0 0'},
						{bind: 'text:node.dispSize', minWidth: 50, padding: '0 10px 0 0'},
						{text: 'Modified', bold: true, padding: '0 10px 0 0'},
						{bind: 'text:node.modifyDate', minWidth: 50, padding: '0 10px 0 0'},
						{text: 'Owner', bold: true, padding: '0 10px 0 0'},
						{bind: 'text:node.usernm', minWidth: 50, padding: '0 10px 0 0'},
						{text: 'Uploaded', bold: true, padding: '0 10px 0 0'},
						{bind: 'text:node.uploadDate', minWidth: 50, padding: '0 10px 0 0'},
					]},
					{border: true, bind: 'visible:showButtons', justify: 'start', key: 'buttons', content: [
							{type: 'inputButton', opts: {text: 'Preview', primary: true, click: 'filePreview'}},
							{type: 'inputButton', opts: {text: 'Download', primary: false, click: 'fileDownload'}}
					]},
					{key: 'preview', bind: 'visible:showPreview', flex: 'auto', content: [
						{type: 'iframe', opts: {key: 'preview'}}
					]},
					{border: true, bind: 'visible:showGrid', text: 'File List', style: 'k-header header'},
					{key: 'grid', bind: 'visible:showGrid', flex: 'auto', content: [
						{type: 'grid', opts: this.gridOptions()}
					]}
				]}
			]}}
		];

		this.view = new View({
			model: this.model,
			content: content,
			container: opts.container
		});

		this.grid = this.view.grids.files;
		this.iframe = this.view.iframes.preview;

		setTimeout(function(){
			kendo.bind(this.view.panels.right, this.model);
		}.bind(this), 0);
	}
	createTree(opts){
		let context = Dom.el('ul', {});
		let treeOpts = {
			container: this.view.panels.tree,
			autoSort: true,
			dragAndDrop: true,
			drag: function(evt){ this.treeDrag(evt); }.bind(this),
			drop: function(evt){ this.treeDrop(evt); }.bind(this),
			onSelect: function(evt){ this.treeSelect(evt); }.bind(this)
		};

		this.tree = new Treeview(treeOpts);
		this.context = context.kendoContextMenu({
			target: this.tree.wrapper,
			dataSource: [{text: 'New Folder'}],
			filter: '.k-item',
			open: function(evt){ this.contextOpen(evt); }.bind(this),
			select: function(evt){ this.contextSelect(evt); }.bind(this),
			close: function(evt){ this.contextClose(evt); }.bind(this)
		}).data('kendoContextMenu');
	}
	createDropZone(){
		let el = Dom.el('input', {name: 'file', id: 'file', type: 'file'});

		this.uploader = el.kendoUpload({
			async: {
				saveUrl: 'upload',
				removeUrl: 'remove',
				autoUpload: true
			},
			enabled: false,
			success: function(data){ this.onUpload(data); }.bind(this),
			upload: function(evt){ this.uploadTarget = {uuid: this.selected.data.uuid, el: this.selected.el}; }.bind(this),
			showFileList: false,
			dropZone: this.view.panels.container
		}).data('kendoUpload');
	}
	treeDrag(evt){
		let target = evt.sender.dataItem(evt.dropTarget);

		if(!target.folder || evt.statusClass !== 'i-add'){
			evt.setStatusClass('k-denied');
		}
	}
	treeDrop(evt){
		let source = evt.sender.dataItem(evt.sourceNode);
		let target = evt.sender.dataItem(evt.dropTarget);

		if(!target.folder){
			evt.setValid(false);
			return;
		}

		Utils.ajax({
			route: 'fsMove',
			data: {uuid: source.uuid, parent: target.uuid},
			success: function(){}
		});
	}
	contextOpen(evt){
		let dataSource = [];
		let target = $(evt.target);
		let node = this.tree.treeview.dataItem(target);
		this.tree.treeview.select(target);

		if(node.hasChildren){
			dataSource = [
				{text: 'New Folder', action: 'folder'},
				{text: 'Delete', action: 'delete'}
			];
		} else {
			dataSource = [
				{text: 'Download', action: 'download'},
				{text: 'Delete', action: 'delete'}
			];
		}

		this.context.setOptions({dataSource: dataSource});
		this.contextNode = {el: target, node: node};
	}
	contextSelect(evt){
		let action = Utils.menuDataItem(evt).dataItem.action;
		let node = this.contextNode;

		switch(action){
			case 'folder':
				this.createFolder(node);
			break;
			case 'delete':
				this.removeNode(node);
			break;
			case 'download':
				this.download(node);
			break;
		}
	}
	contextClose(evt){
		this.contextNode = false;
	}
	createFolder(node){
		let uuid = Utils.uuid();
		let item = {
			text: 'untitled folder',
			spriteCssClass: 'icon-folder-7',
			id: uuid,
			uuid: uuid,
			items: [],
			parent: node.node.uuid,
			fs: this.fsName,
			folder: 1,
			size: 0,
			sort: 1,
			usernm: userInfo.account.fullName,
			modifyDate: moment().format('MMM D YYYY, h:mm a'),
			uploadDate: moment().format('MMM d YYYY, h:mm a'),
			gridModified: new Date(),
			gridUploaded: new Date()
		};

		this.tree.addNode(node.el, item, true);

		setTimeout($('#fsfilename').focus().select(), 1000);

		Utils.ajax({
			route: 'fsAdd',
			data: item,
			success: function(){}
		});
	}
	removeNode(node){
		console.log('remove', node);
	}
	download(node){
		console.log('download', node);
	}
	treeSelect(node){
		let folder = node.data.folder;

		if(folder){
			this.uploader.enable();
			this.grid.setDataSource(node.data.items);
			this.model.set('showGrid', true);
			this.model.set('showDetail', true);
			this.model.set('showButtons', false);
			this.model.set('showPreview', false);
			setTimeout(function(){ this.grid.resize(); }.bind(this), 0);
		} else {
			this.uploader.disable();
			this.model.set('showGrid', false);
			this.model.set('showDetail', true);
			this.model.set('showButtons', true);
			this.model.set('showPreview', true);
			this.iframe.setSrc(undefined);
		}

		this.model.set('node', node.data);
		this.selected = node;
	}
	onUpload(data){
		for (let [idx, file] of data.response.entries()){
			let details = data.files[idx];
			let node = {
				id: file.uuid,
				text: file.filename,
				uuid: file.uuid,
				mimetype: file.mimetype,
				size: details.size,
				ext: details.extension,
				sort: 0,
				dispSize: Utils.formatBytes(details.size),
				usernm: userInfo.account.fullName,
				modified: details.rawFile.lastModified,
				modifyDate: moment(details.rawFile.lastModified).format('MMM D YYYY, h:mm a'),
				uploadDate: moment().format('MMM d YYYY, h:mm a'),
				gridModified: new Date(details.rawFile.lastModified),
				gridUploaded: new Date(),
				parent: this.uploadTarget.uuid,
				fs: this.fsName,
				folder: 0
			};

			node.spriteCssClass = this.getIcon(node);

			this.tree.addNode(this.uploadTarget.el, node);

			Utils.ajax({
				route: 'fsAdd',
				data: node,
				success: function(){}
			});
		}
	}
	gridSelect(obj){
		let node = this.tree.getNodeAndElement(obj.data.uuid);
		this.tree.select(node.el);
	}
	getData(){
		Utils.ajax({
			route: 'fsTree',
			data: {fs: this.fsName},
			success: function(data){
				for(let item of data){
					item.id = item.uuid;
					item.spriteCssClass = this.getIcon(item);
					item.modifyDate = moment(item.modified).format('MMM D YYYY, h:mm a');
					item.uploadDate = moment(item.uploaded).format('MMM D YYYY, h:mm a');
					item.gridModified = new Date(item.modified);
					item.gridUploaded = new Date(item.uploaded);
					item.dispSize = Utils.formatBytes(item.size);
					item.sort = item.folder;
				}
				let dataSource = [{
					text: this.rootName,
					uuid: 0,
					id: 0,
					expanded: true,
					folder: 1,
					sort: 1 + this.rootName,
					spriteCssClass: 'icon-folder-7',
					items: Utils.createHierarchy(data, 'uuid', 'parent', 0)
				}];

				this.tree.setData(dataSource);
				this.root = this.tree.getNodeAndElement(0);
				this.tree.select(this.root.el);

			}.bind(this)
		});
	}
	gridOptions(){
		let options = {
			key: 'files',
			autoBind: true,
			autoFit: false,
			selectable: 'row',
			scrollable: true,
			reorderable: true,
			editable: false,
			sortable: {mode: 'multiple', allowUnsort: true},
			paging: {
				size: 100,
				dropdown: true,
				prevNext: true,
				numeric: true,
				buttonCount: false,
				input: true,
				refresh: true,
				info: true
			},
			columns: [
				{title: 'Name', type: 'string', field: 'text', width: 250},
				{title: 'Size', type: 'string', field: 'dispSize', width: 100},
				{title: 'Modified', type: 'date', field: 'gridModified', format:'{0:MMM d yyyy, h:mm tt}', width: 170},
				{title: 'Uploaded By', type: 'string', field: 'usernm', width: 150},
				{title: 'Uploaded On', type: 'date', field: 'gridUploaded', format:'{0:MMM d yyyy, h:mm tt}', width: 170}
			],
			onSelect: function(obj){ this.gridSelect(obj); }.bind(this)
		};

		return options;
	}
	getIcon(item){
		let icons = ['3dm','3ds','3g2','3gp','7z','aac','ai','aif','angel','apk','app','asf','asp','aspx','asx','avi','bak','bat','bin','blank','bmp','cab','cad','cdr','cer','cfg','cfm','cgi','class','com','cpl','cpp','crx','csr','css','csv','cue','cur','dat','db','dbf','dds','debian','dem','demon','dll','dmg','dmp','doc','docx','drv','dtd','dwg','dxf','elf','eml','eps','exe','fla','flash','flv','fnt','fon','gam','gbr','ged','gif','gpx','gz','gzip','hqz','html','ibooks','icns','ico','ics','iff','indd','ipa','iso','jar','jpg','js','jsp','key','kml','kmz','lnk','log','lua','m3u','m4a','m4v','mach','max','mdb','mdf','mid','mim','mov','mp3','mp4','mpa','mpg','msg','msi','nes','object','odb','odc','odf','odg','odi','odp','ods','odt','odx','ogg','otf','pages','pct','pdb','pdf','pif','pkg','pl','png','pps','ppt','pptx','ps','psd','pub','python','ra','rar','raw','rm','rom','rpm','rss','rtf','sav','sdf','sitx','sql','srt','svg','swf','sys','tar','tex','tga','thm','tiff','tmp','torrent','ttf','txt','uue','vb','vcd','vcf','vob','wav','wma','wmv','wpd','wps','wsf','xhtml','xlr','xls','xlsx','xml','yuv','zip'];
		let style = 'mime-blank';

		if(item.folder){ return 'icon-folder-7'; }
		let ext = item.ext.replace('.', '');
		let idx = icons.indexOf(ext);
		if(idx >= 0){ style = 'mime-' + ext; }

		return style;
	}
}
