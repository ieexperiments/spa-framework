/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class AudioControl{
	constructor(){
		this.context = new window.AudioContext();
	}
	start(frequency, duration, volume){
		this.oscillator = this.context.createOscillator();
		this.oscillator.frequency.value = frequency;
		this.oscillator.type = 'sine';
		this.volume = this.context.createGain();
		this.volume.gain.value = volume || 1;
		this.volume.connect(this.context.destination);
		this.oscillator.connect(this.volume);
		this.oscillator.start(0);

		console.log('start');

		if(duration){
			setTimeout(function(){ this.stop(); }.bind(this), duration);
		}
	}
	stop(){
		this.oscillator.stop(0);
		this.oscillator.disconnect(this.volume);
		this.volume.disconnect(this.context.destination);
	}
}

//let player = new AudioControl();
//player.start(800, 5000, 0.5);
