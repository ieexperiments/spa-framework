/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Flex = {
		el(tag, opts = {}, container = false){
				let props = this.getProperties(opts);

				return Dom.el(tag, props, container);
		},
		getProperties(opts){
			let styles = opts.style ? opts.style.split(' ') : [];
			let props = {};

			styles.push('flex');

			for(let [key, value] of Object.entries(opts)){
				switch(key){
					case 'direction':
					case 'dir':
						styles.push('flex-dir-' + value);
					break;
					case 'wrap':
						styles.push('flex-wrap');
					break;
					case 'nowrap':
						styles.push('flex-nowrap');
					break;
					case 'wrapReverse':
						styles.push('flex-wrap-reverse');
					break;
					case 'justify':
						styles.push('flex-justify-' + value);
					break;
					case 'align':
						styles.push('flex-align-items-' + value);
					break;
					case 'alignContent':
						styles.push('flex-align-content-' + value);
					break;
					case 'alignSelf':
						styles.push('flex-align-self-' + value);
					break;
					default:
						props[key] = value;
					break;
				}
			}

			props.style = styles.join(' ');

			return props;
		}
};
