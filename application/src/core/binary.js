/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const geoComp = (360 / Math.pow(2, 24));
const geoReduce = 4;

class BinaryReader{
	constructor(data, dataType){
		if(data){ this.load(data, dataType); }
	}
	load(data, dataType){
		this.offset = 0;

		switch(dataType){
			case 1:
				this.setView(data);
			break;
			case 2:
				this.bufferFromBlob(data);
			break;
			case 3:
				this.bufferFromText(data);
			break;
		}
	}
	setView(buffer){
		this.len = buffer.byteLength;
		this.dataView = new DataView(buffer, 0, this.len);
		this.eof = this.len ? false : true;
		this.offset = 0;
	}
	bufferFromBlob(data){
		let reader = new FileReader();

		reader.onload = function(evt){
			this.setView(reader.result);
		}.bind(this);

		reader.readAsArrayBuffer(data);
	}
	bufferFromText(data){
		let len = data.length;
		let buffer = new ArrayBuffer(len);
		let view = new Uint8Array(buffer);

		for(let idx = 0; idx < len; idx++){
			view[idx] = data.charCodeAt(idx);
		}

		this.dataView = new DataView(buffer);
		this.len = buffer.byteLength;
	}
	setOffset(len){
		this.offset += len;

		if(this.offset === this.len){ this.eof = true; }
	}
	string(len){
		let str = '';

		if(len === 0){ return null; }

		for(let idx = 0; idx < len; idx++){
			str += String.fromCharCode(this.uint8());
		}

		return str;
	}
	uint32(){
		let num = this.dataView.getUint32(this.offset);
		this.setOffset(4);
		return num;
	}
	uint24(){
		let num = ((this.uint16() << 8) | this.uint8());
		return num;
	}
	uint16(){
		let num = this.dataView.getUint16(this.offset);
		this.setOffset(2);
		return num;
	}
	uint8(){
		let num = this.dataView.getUint8(this.offset);
		this.setOffset(1);
		return num;
	}
	int32(){
		let num = this.dataView.getInt32(this.offset);
		this.setOffset(4);
		return num;
	}
	int24(){
		let num = ((this.uint16() << 8) | this.uint8());
		this.setOffset(3);
		return num;
	}
	int16(){
		let num = this.dataView.getInt16(this.offset);
		this.setOffset(2);
		return num;
	}
	int8(){
		let num = this.dataView.getInt8(this.offset);
		this.setOffset(1);
		return num;
	}
	timestamp(){
		let high = this.uint32();
		let low = this.uint16();
		return Number((high.toString() + '0000')) + Number(low.toString());
	}
	tell(){
		return this.offset;
	}
	toJSON(){
		let arr = [];

		for(let idx = 0; idx < this.len; idx++){
			arr.push(this.dataView.getUint8(idx));
		}

		return arr;
	}
}

const EncodePoint = function(point){
	let lon = point[0];
	let lat = point[1];

	if(lon < 0){ lon = 360 + lon; }
	if(lat < 0){ lat = 180 + lat; }

	lon = Math.round((lon / geoComp) / geoReduce);
	lat = Math.round((lat / geoComp) / geoReduce);

	return [lon, lat];
};

const DecodePoint = function(point){
	let lon = point[0];
	let lat = point[1];

	lon = (lon * geoComp) * geoReduce;
	lat = (lat * geoComp) * geoReduce;

	if(lon > 180){ lon = lon - 360; }
	if(lat > 90){ lat = lat - 180; }

	return [lon, lat];
};

const BitSubstr = function(bytes, msb, lsb){
	return (bytes >>> (lsb - 1)) & (0xFFFFFFFF >>> (32 - (msb - lsb + 1)));
};
