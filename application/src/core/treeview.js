/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Treeview{
	constructor(opts, view = false){
		let options = Kendo.getOptions(opts, 'kendoTreeView');
		let wrapper = Dom.el('treewrapper', {}, opts.container);

		if(opts.checkChildren){
			options.checkboxes = {checkChildren: true};
		}

		options.check = function(evt){ this.eventHandle(evt, 1); }.bind(this);
		options.select = function(evt){ this.eventHandle(evt, 2); }.bind(this);
		options.expand = function(evt){ this.eventHandle(evt, 3); }.bind(this);
		options.collapse = function(evt){ this.eventHandle(evt, 4); }.bind(this);

		this.onSelect = opts.onSelect || function(){};
		this.onExpCol = opts.onExpCol || function(){};
		this.fieldKey = options.dataTextField;
		this.options = options;

		this.setDataSource(options);
		this.treeview = wrapper.kendoTreeView(options).data('kendoTreeView');
		this.wrapper = wrapper;
		this.setCheckboxes(options);
		wrapper.css('border', 'none');

		if(view){ view.register('treeviews', opts.key, this); }

		return this;
	}
	setData(data){
		let dataSource = new kendo.data.HierarchicalDataSource({
			data: data,
			sort: this.options.autoSort ? this.sortConfig : false
		});

		this.treeview.setDataSource(dataSource);
	}
	setDataSource(options){
		this.sortConfig = [
			{field: 'sort', dir: 'desc'},
			{field: this.fieldKey, dir: 'asc'}
		];
		let dataSource = new kendo.data.HierarchicalDataSource({
			data: options.dataSource || [],
			sort: options.autoSort ? this.sortConfig : false
		});

		options.dataSource = dataSource;
	}
	setCheckboxes(options){
		let disabled = false;

		if(!options.checkboxes){ return; }

		Utils.traverseDataSource(options.dataSource, function(node){
			disabled = (node.hasChildren && node.hasOwnProperty('checkChildren') && !node.checkChildren) ? true : false;
			this.getNodeElement(node).find('input').attr('disabled', disabled);
		}.bind(this));
	}
	getChildren(node, nodes){
		for(let item in node.children.view()){
			if(item.hasChildren){
				this.getChildren(item, nodes);
			} else {
				nodes.push(item);
			}
		}
	}
	setSort(items){
		for(let item of items){
			if(item.hasChildren){
				item.children.sort(this.sortConfig);
				this.setSort(item.children.view());
			}
		}
	}
	select(node){
		this.treeview.select(node);
		this.treeview.trigger('select', {node: node});
	}
	addNode(parent, node, select = false){
		let el = this.treeview.append(node, parent);
		//this.treeview.append(node, this.getNodeElement(parent));
		this.setSort(this.treeview.dataSource.view());
		if(select){ this.select(el); }
		return el;
	}
	getNode(id){
		return this.treeview.dataSource.get(id);
	}
	getNodeElement(node){
		return $(this.treeview.findByUid(node.uid));
	}
	getNodeCheckbox(node){
		return this.getNodeElement(node).find('input[type=checkbox]');
	}
	getNodeAndElement(id){
		let node = this.getNode(id);
		let el = this.getNodeElement(node);

		return {node: node, el: el};
	}
	setText(id, text, sort){
		let obj = this.getNodeAndElement(id);

		obj.node.set(this.fieldKey, text);
		obj.node.set('sort', sort);
		this.treeview.text(obj.el, text);
		this.setSort(this.treeview.dataSource.view());
	}
	eventHandle(evt, type){
		let node = evt.sender.dataItem(evt.node);
		let folder = node.hasChildren ? true : false;
		let obj = {
			el: this.getNodeElement(node),
			data: node.toJSON(),
			dataItem: node
		};

		if(type === 1 || type === 2){
			this.onSelect(obj);
		} else {
			this.onExpCol(obj);
		}
	}
}
