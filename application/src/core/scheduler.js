/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Scheduler{
	constructor(opts, view = false){
		let options = Kendo.getOptions(opts, 'kendoScheduler');
		let wrapper = Dom.el('div', {style: 'grid1'}, opts.container);
		let container = Dom.el('div', {style: 'grid2'}, wrapper);

		console.log(options);

		this.scheduler = container.kendoScheduler(options).data('kendoScheduler');

		if(!opts.border){ container.css('border', 'none'); }
		if(view){ view.register('schedulers', opts.key, this.scheduler); }
	}
}
