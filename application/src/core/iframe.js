/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Iframe{
	constructor(opts, view = false){
		//let sandbox= 'allow-scripts allow-modals allow-same-origin allow-popups';
		this.iframe = Dom.el('iframe', {src: opts.src}, opts.container);
		if(view){ view.register('iframes', opts.key, this); }

		return this;
	}
	setSrc(uri){
		uri = uri || 'about:blank';
		this.iframe.attr('src', uri);
	}
	embed(content){
		let body = $(this.iframe.contents().find('body'));
	}
}
