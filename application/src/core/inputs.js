/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Input = {
	input(opts, style = false, view = false, kendo = false, range = false){
		opts = Object.assign({enabled: true, visible: true}, opts);
		opts.id = opts.id || this.generateId();
		let visible = opts.visible;
		let enabled = opts.enabled;
		let wrapper = this.wrapper(opts, view, range);
		let label = this.label(opts.label, 'left', opts, wrapper);
		let el = false;
		let desc = false;

		if(kendo){
			let options = this.getKendoOptions(opts, kendo, range);
			el = Dom.el(opts.tag, opts, wrapper)[kendo](options);

			if(opts.primary){ el.addClass('k-primary'); }

			if(kendo === 'kendoSlider'){
				wrapper.css('height', this.getSliderHeight(wrapper));
			}
		} else {
			el = Dom.el(opts.tag, opts, wrapper);
			if(style){ el.addClass(style); }

			if(opts.type === 'checkbox'){
				desc = Dom.el('label', {style: 'k-checkbox-label checkRadio', for: opts.id, text: opts.desc}, wrapper);
			}

			if(opts.type === 'radio'){
				desc = Dom.el('label', {style: 'k-radio-label checkRadio', for: opts.id, text: opts.desc}, wrapper);
			}
		}

		let out = {wrapper: wrapper, label: label, el: el, desc: desc, enabled: enabled, visible: visible};

		if(view){ view.register('inputs', opts.key2 || opts.key, out); }

		return out;
	},
	wrapper(opts, view = false, range = false){
		let visible = true;

		if(opts.key){
			if(!range){
				if(opts.type === 'checkbox' || opts.type === 'radio'){
					opts.checked = opts.key;
				} else {
					opts.value = opts.key;
				}
			}

			if(view){
				opts.enabled = Utils.isBoolean(opts.enabled) ? 'inputs.' + (opts.key2 || opts.key) + '.enabled' : opts.enabled;
				visible = 'inputs.' + (opts.key2 || opts.key) + '.visible';
			}
		}
		let align = (opts.tag === 'textarea' || opts.type === 'file' || opts.columns ? 'top' : 'center');
		let wrapper;
		if(opts.wrapper){
			wrapper = opts.wrapper;
		} else {
			wrapper = Flex.el('inputblock', {direction: 'row', visible: visible, align: align}, opts.container);
		}

		delete opts.visible;
		return wrapper;
	},
	label(text, type, opts, wrapper){
		if(!text){ return false; }

		let label = Dom.el('label', {text: text, style: 'label label-' + type}, wrapper);

		if(type === 'left' && opts.type !== 'checkbox' && opts.type !== 'radio' && opts.tag !== 'button'){
			label.attr('for', opts.id);
		}

		return label;
	},
	header(opts){
		Dom.el('inputheader', {text: opts.text}, opts.container);
	},
	text(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'text'}, opts);
		return this.input(opts, 'k-textbox standard-input', view);
	},
	password(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'password'}, opts);
		return this.input(opts, 'k-textbox standard-input', view);
	},
	email(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'email'}, opts);
		return this.input(opts, 'k-textbox standard-input', view);
	},
	phone(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'phone'}, opts);
		return this.input(opts, 'k-textbox standard-input', view);
	},
	numeric(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'number'}, opts);
		return this.input(opts, false, view, 'kendoNumericTextBox');
	},
	numericRange(opts, view = false){
		this.range(opts, 'number', view);
	},
	button(opts, view = false){
		opts = Object.assign({tag: 'button'}, opts);
		return this.input(opts, false, view, 'kendoButton');
	},
	date(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'date'}, opts);
		return this.input(opts, false, view, 'kendoDatePicker');
	},
	dateRange(opts, view = false){
		this.range(opts, 'date', view);
	},
	time(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'time'}, opts);
		return this.input(opts, false, view, 'kendoTimePicker');
	},
	timeRange(opts, view = false){
		this.range(opts, 'time', view);
	},
	dateTime(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'datetime'}, opts);
		return this.input(opts, false, view, 'kendoDateTimePicker');
	},
	dateTimeRange(opts, view = false){
		this.range(opts, 'datetime', view);
	},
	color(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'color'}, opts);
		return this.input(opts, false, view, 'kendoColorPicker');
	},
	autocomplete(opts, view = false){
		opts = Object.assign({tag: 'input'}, opts);
		return this.input(opts, false, view, 'kendoAutoComplete');
	},
	combobox(opts, view = false){
		opts = Object.assign({tag: 'select'}, opts);
		return this.input(opts, false, view, 'kendoComboBox');
	},
	dropdown(opts, view = false){
		opts = Object.assign({tag: 'select'}, opts);
		return this.input(opts, false, view, 'kendoDropDownList');
	},
	multiselect(opts, view = false){
		opts = Object.assign({tag: 'select', minWidth: 400}, opts);
		return this.input(opts, false, view, 'kendoMultiSelect');
	},
	textarea(opts, view = false){
		opts = Object.assign({tag: 'textarea', minWidth: 400, minHeight: 200}, opts);
		return this.input(opts, 'k-textbox standard-input textarea', view);
	},
	editor(opts, view = false){
		opts = Object.assign({tag: 'textarea', width: 800, height: 400}, opts);
		return this.input(opts, false, view, 'kendoEditor');
	},
	slider(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'number'}, opts);
		return this.input(opts, false, view, 'kendoSlider');
	},
	getSliderHeight(wrapper){
		let tick = $(wrapper.find('span.k-label')[0]);
		let height = tick.position().top + tick.outerHeight();

		return height + 10;
	},
	upload(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'file'}, opts);
		return this.input(opts, false, view, 'kendoUpload');
	},
	checkbox(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'checkbox'}, opts);
		return this.input(opts, 'k-checkbox', view);
	},
	radiobutton(opts, view = false){
		opts = Object.assign({tag: 'input', type: 'radio'}, opts);
		return this.input(opts, 'k-radio', view);
	},
	checkboxGroup(opts, view){
		opts = Object.assign({tag: 'input', type: 'checkbox', enabled: true, visible: true}, opts);

		this.column(opts, 'k-checkbox', view);
	},
	radiobuttonGroup(opts, view){
		opts = Object.assign({tag: 'input', type: 'radio', enabled: true, visible: true}, opts);
		opts.name = opts.name || this.generateId();

		this.column(opts, 'k-radio', view);
	},
	column(opts, style, view){
		opts = Object.assign({direction: 'lr', sort: false}, opts);

		if(!opts.items || !opts.items.length){ return; }

		let visible = opts.visible;
		let dir = opts.direction;
		let len = opts.items.length;
		let col = opts.columns;
		let mod = Math.ceil(len / col);
		let values = opts.items;
		let columns = [];
		let boxes = [];
		let key = opts.key;

		delete opts.items;
		delete opts.direction;
		delete opts.sort;

		if(opts.columns){
			let wrapper = this.wrapper(opts, view);
			let label = this.label(opts.label, 'left', opts, wrapper);
			delete opts.label;

			for(let idx = 0; idx < opts.columns; idx++){
				columns.push(Dom.el('inputcolumn', {backgroundColor1: '#ff0000'}, wrapper));
			}

			for(let [idx, item] of values.entries()){
				let mod = (idx % col);

				opts.desc = item.name;
				opts.wrapper = columns[mod];
				opts.key = style === 'k-radio' ? opts.key : key + '[' + idx + ']';
				let obj = this.input(opts, style, view);
				obj.el.attr('value', item.value);
			}
		} else {
			for(let [idx, item] of values.entries()){
				opts.desc = item.name;
				opts.key = style === 'k-radio' ? opts.key : key + '[' + idx + ']';
				opts.key2 = key;
				let obj = this.input(opts, style, view);
				obj.el.attr('value', item.value);
				delete opts.label;
			}
		}

		//this.setView(view, opts, {wrapper: wrapper, label: label, enabled: opts.enabled, visible: visible});
	},
	range(opts, type, view = false){
		let kendo;
		let key = opts.key;
		let def = {tag: 'input', type: type, rangeText: 'to', minDiff: 1};

		switch(type){
			case 'number':
				kendo = 'kendoNumericTextBox';
			break;
			case 'date':
				kendo = 'kendoDatePicker';
				def.minUnit = 'day';
			break;
			case 'time':
				kendo = 'kendoTimePicker';
				def.minUnit = 'hour';
			break;
			case 'datetime':
				kendo = 'kendoDateTimePicker';
				def.minUnit = 'hour';
			break;
		}

		if(type !== 'number'){
			opts.min = opts.min || moment().subtract(4000, 'years').toDate();
			opts.max = opts.max || moment().add(4000, 'years').toDate();
		}

		opts = Object.assign(def, opts);
		let min = opts.min;
		let max = opts.max;
		if(opts.key){ opts.value = opts.key + '[0]'; }
		opts.min = min;
		opts.max = this.setRangeMax(opts, min, max);
		opts.rangeIdx = 0;
		let el1 = this.input(opts, false, view, kendo, true);
		let rangeText = this.label(opts.rangeText, 'middle', opts, el1.wrapper);
		if(opts.key){ opts.key2 = key; opts.value = opts.key + '[1]'; }
		opts.min = this.setRangeMin(opts, min, max);
		opts.max = max;
		opts.rangeIdx = 1;
		opts.wrapper = el1.wrapper;
		delete opts.label;
		let el2 = this.input(opts, false, view, kendo, true);
		let out = {wrapper: opts.wrapper, el1: el1, el2: el2};

		if(view){ view.register('input', key, out); }
		return out;
	},
	setRangeMin(options, min, max){
		let out = false;

		if(Utils.isDate(min)){
			out = moment(min).add(options.minDiff, options.minUnit).toDate();
		} else {
			out = min + options.minDiff;
		}
		return out;
	},
	setRangeMax(options, min, max){
		let out = false;

		if(Utils.isDate(min)){
			out = moment(max).subtract(options.minDiff, options.minUnit).toDate();
		} else {
			out = max - options.minDiff;
		}
		return out;
	},
	rangeCheck: function(evt){
		let idx = evt.sender.options.rangeIdx;
		let prop = 'kendo' + evt.sender.options.name;
		let skip = evt.sender.options.skip;
		let minDiff = evt.sender.options.minDiff;
		let minUnit = evt.sender.options.minUnit;
		let elements = evt.sender.element.parents('inputblock').find('input');
		let kendo = [];

		if(skip){
			evt.sender.options.skip = false;
			return;
		}

		for(let item of elements){
			item = $(item);
			if(item.data(prop)){
				kendo.push(item.data(prop));
			}
		}

		let values = [kendo[0].value(), kendo[1].value()];

		if(prop === 'kendoNumericTextBox'){
			Input.checkNumberRange(idx, kendo, values, minDiff);
		} else {
			Input.checkDateRange(idx, kendo, values, minDiff, minUnit);
		}
	},
	checkNumberRange(idx1, kendo, values, minDiff){
		let incr = minDiff * (idx1 ? -1 : 1);
		let idx2 = idx1 ? 0 : 1;
		let obj1 = {kendo: kendo[idx1], value: values[idx1]};
		let obj2 = {kendo: kendo[idx2], value: values[idx2]};
		let change = false;

		if(Utils.isNullOrUndefined(obj2.value)){
			change = true;
		} else if(idx1){
			if(obj2.value > (obj1.value - minDiff)){
				change = true;
			}
		} else {
			if(obj2.value < (obj1.value + minDiff)){
				change = true;
			}
		}

		if(change){
			obj2.kendo.value(obj1.value + incr);
			obj2.kendo.options.skip = true;
			obj2.kendo.trigger('change');
		}
	},
	checkDateRange(idx1, kendo, values, minDiff, minUnit){
		let incr = minDiff * (idx1 ? -1 : 1);
		let idx2 = idx1 ? 0 : 1;
		let obj1 = {kendo: kendo[idx1], value: moment(values[idx1])};
		let obj2 = {kendo: kendo[idx2], value: moment(values[idx2])};
		let clone = obj1.value.clone().add(incr, minUnit);
		let change = false;

		if(!obj2.value.isValid()){
			change = true;
		} else if(idx1){
			if(obj2.value.isSameOrAfter(clone)){
				change = true;
			}
		} else {
			if(obj2.value.isSameOrBefore(clone)){
				change = true;
			}
		}

		if(change){
			obj2.kendo.value(clone.toDate());
			obj2.kendo.options.skip = true;
			obj2.kendo.trigger('change');
		}
	},
	getKendoOptions(opts, kendo, range){
		let options = Kendo.getOptions(opts, kendo);

		if(range){
			options.change = this.rangeCheck;
			if(kendo === 'kendoNumericTextBox'){
				options.spin = this.rangeCheck;
			}
		}

		return options;
	},
	generateId(){
		this.idIndex++;

		return 'el' + this.idIndex;
	},
	idIndex: 0
};
