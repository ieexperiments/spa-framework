/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class View{
	constructor(config){
		this.internal = [];
		this.model = config.model;
		this.setContent(config.content, config.container);
		this.autoSize(config);
	}
	register(type, key, value){
		if(!key){ return; }

		let path = type + '.' + key;

		if(!this.model[type]){
			this.internal.push(type);
			if(type === 'inputs'){
				this.model.set(type, {});
			} else {
				this.model[type] = {};
			}
			this[type] = this.model[type];
		}

		this.checkPath(type, key);
		this.model.set(path, value);
	}
	checkPath(type, key){
		let arr = key.split('.');
		let path = type;

		for(let prop of arr){
			path += '.' + prop;

			if(!this.model.get(path)){
				this.model.set(path, {});
			}
		}
	}
	setContent(content, container){
		for(let obj of content){
			obj.opts.container = container;
			this.processContent(obj);
		}
	}
	data(){
		let data = this.model.toJSON();

		for(let prop of this.internal){
			delete data[prop];
		}

		return data;
	}
	processContent(obj){
		let opts = obj.opts;
		let instance = false;

		switch(obj.type){
			case 'panelset':
				PanelSet.create(opts, true, this);
			break;
			case 'panelmenu':
				instance = new PanelMenu(opts, this);
			break;
			case 'tabstrip':
				instance = new Tabstrip(opts, this);
			break;
			case 'toolbar':
				instance = new Toolbar(opts, this);
			break;
			case 'treeview':
				instance = new Treeview(opts, this);
			break;
			case 'scheduler':
				instance = new Scheduler(opts, this);
			break;
			case 'clock':
				instance = new Clock(opts, this);
			break;
			case 'spinner':
				instance = new Spinner(opts, this);
			break;
			case 'autocomplete':
				instance = new Autocomplete(opts, this);
			break;
			case 'filetree':
				instance = new FileTree(opts, this);
			break;
			case 'notification':
				instance = new Notification(opts, this);
			break;
			case 'dialog':
				instance = new Dialog(opts, this);
			break;
			case 'grid':
				instance = new Grid(opts, this);
			break;
			case 'inputText':
				Input.text(opts, this);
			break;
			case 'inputPassword':
				Input.password(opts, this);
			break;
			case 'inputUpload':
				Input.upload(opts, this);
			break;
			case 'inputCombobox':
				Input.combobox(opts, this);
			break;
			case 'inputDropdown':
				Input.dropdown(opts, this);
			break;
			case 'inputAutocomplete':
				Input.autocomplete(opts, this);
			break;
			case 'inputMultiselect':
				Input.multiselect(opts, this);
			break;
			case 'inputTextarea':
				Input.textarea(opts, this);
			break;
			case 'inputColor':
				Input.color(opts, this);
			break;
			case 'inputSlider':
				Input.slider(opts, this);
			break;
			case 'inputButton':
				Input.button(opts, this);
			break;
			case 'inputEditor':
				Input.editor(opts, this);
			break;
			case 'inputEmail':
				Input.email(opts, this);
			break;
			case 'inputPhone':
				Input.phone(opts, this);
			break;
			case 'inputNumeric':
				Input.numeric(opts, this);
			break;
			case 'inputNumericRange':
				Input.numericRange(opts, this);
			break;
			case 'inputCheckbox':
				Input.checkbox(opts, this);
			break;
			case 'inputCheckboxGroup':
				Input.checkboxGroup(opts, this);
			break;
			case 'inputRadiobutton':
				Input.radiobutton(opts, this);
			break;
			case 'inputRadiobuttonGroup':
				Input.radiobuttonGroup(opts, this);
			break;
			case 'inputDate':
				Input.date(opts, this);
			break;
			case 'inputDateRange':
				Input.dateRange(opts, this);
			break;
			case 'inputTime':
				Input.time(opts, this);
			break;
			case 'inputTimeRange':
				Input.timeRange(opts, this);
			break;
			case 'inputDateTime':
				Input.dateTime(opts, this);
			break;
			case 'inputDateTimeRange':
				Input.dateTimeRange(opts, this);
			break;
			case 'inputHeader':
				Input.header(opts, this);
			break;
			case 'iframe':
				instance = new Iframe(opts, this);
			break;
		}
	}
	enable(keys){
		keys = Utils.isArray(keys) ? keys : [keys];

		for(let key of keys){
			this.model.set('inputs.' + key + '.enabled', true);
		}
	}
	visible(keys){
		keys = Utils.isArray(keys) ? keys : [];

		for(let key of keys){
			this.model.set('inputs.' + key + '.visible', true);
		}
	}
	disable(keys){
		keys = Utils.isArray(keys) ? keys : [];

		for(let key of keys){
			this.model.set('inputs.' + key + '.enabled', false);
		}
	}
	invisible(keys){
		keys = Utils.isArray(keys) ? keys : [];

		for(let key of keys){
			this.model.set('inputs.' + key + '.visible', false);
		}
	}
	autoSize(config){
		let container = $(config.container);
		let labels = container.find('label.label-left');
		let matrix = {};

		if(!labels.length){ return; }

		for(let label of labels){
			label = $(label);
			let key = label.offset().left;
			matrix[key] = matrix[key] || [];
			matrix[key].push(label);
		}

		for(let [key, group] of Object.entries(matrix)){
			if(group.length > 1){
				let max = 0;

				for(let label of group){
					let width = label.outerWidth() - 10;
					max = width > max ? width : max;
				}

				for(let label of group){
					label.css('min-width', max);
				}
			}
		}
	}
}
