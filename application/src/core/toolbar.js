/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Toolbar{
	constructor(opts, view = false){
		this.processOptions(opts);
		this.customItems = {};

		let options = Kendo.getOptions(opts, 'kendoToolBar');
		let wrapper = Dom.el('div', {}, opts.container);

		wrapper.css({
			width: '100%',
			border: 'none',
			margin: opts.margin
		});

		options.click = opts.click;
		options.toggle = opts.toggle;
		this.toolbar = wrapper.kendoToolBar(options).data('kendoToolBar');
		wrapper.css({width: 'auto'});

		this.setCustom();
		if(view){ view.register('toolbars', opts.key, this.toolbar); }
	}
	setCustom(){
		if(!this.custom.length){ return; }

		for(let item of this.custom){
			let opts = Kendo.getOptions(item, item.kendo);
			opts.change = item.change;
			opts.spin = item.change;
			let el = $('#' + item.id);
			el.css({width: item.width || 'auto'});
			el[item.kendo](opts);
			el.attr('data-bind', item.bind);

			if(item.id){
				this.customItems[item.id] = el.data(item.kendo);
			}
		}
	}
	processOptions(opts){
		let output = [];
		let idx = 0;
		let id;
		let template;

		this.custom = [];
		this.dropdowns = [];
		this.numerics = [];

		for(let item of opts.items){
			switch(item.type){
				case 'dropdown':
				case 'numeric':
				case 'switch':
					item.id = item.id || 'tbid' + idx++;
					item.kendo = this.getKendoType(item);
					template = ('<input id="{id}" />').replace('{id}', item.id);
					if(item.label){ output.push({template: '<label>' + item.label + '</label>'}); }
					output.push({template: template, overflow: 'never'});
					this.custom.push(item);
				break;
				default:
					output.push(item);
				break;
			}
		}

		opts.items = output;
	}
	toggleCustom(list, bool){
		list = Utils.isArray(list) ? list : [list];

		for(let key of list){
			let el = this.customItems[key];

			if(el.enable){
				el.enable(bool);
			}
			if(el.toggle){
				el.toggle(bool);
			}
		}
	}
	getKendoType(item){
		let kendo;

		switch(item.type){
			case 'dropdown':
				kendo = 'kendoDropDownList';
			break;
			case 'numeric':
				kendo = 'kendoNumericTextBox';
			break;
			case 'switch':
				kendo = 'kendoMobileSwitch';
			break;
		}

		return kendo;
	}
}
