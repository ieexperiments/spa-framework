/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class ShortcutKeys{
	constructor(){
		this.setCharMap();
		this.profiles = {};
		this.active = false;
		this.enabled = false;
		this.history = [];
		this.func = function(evt){ this.process(evt); }.bind(this);
	}
	process(evt){
		if(!this.active || !this.active.length){ return; }
		if($(evt.target).is('textarea')){ return; }

		let hit = false;
		let results = {
			keyCode: evt.keyCode,
			altKey: evt.altKey,
			metaKey: evt.metaKey,
			shiftKey: evt.shiftKey,
			ctrlKey: evt.ctrlKey,
			action: evt.type,
			evt: evt
		};

		results.key = this.charMap[results.keyCode];

		for(let i = 0; i < this.active.length; i++){
			let obj = this.active[i];
			let pass = 0;
			pass += (obj.key === results.key) ? 1 : 0;
			pass += (obj.alt === results.altKey) ? 1 : 0;
			pass += (obj.shift === results.shiftKey) ? 1 : 0;
			pass += (obj.ctrl === results.ctrlKey) ? 1 : 0;
			pass += (obj.action === results.action) ? 1 : 0;

			if(pass === 5){
				hit = obj;
				break;
			}
		}

		if(hit.func){
			if(Utils.isFunction(hit.func)){
				hit.func(results);
			} else {
				let func = function(results){ this[hit.func](results); }.bind(hit.bind);
				func(results);
			}
		}
	}
	addProfile(name, profile, activate){
		if(this.profiles[name]){
			throw(createError(name + ' keypress profile already registered'));
		}

		for(let obj of profile){
			obj.key = obj.key.toLowerCase();
			obj.alt = obj.alt || false;
			obj.ctrl = obj.ctrl || false;
			obj.shift = obj.shift || false;
			obj.action = obj.action || 'keydown';
		}

		this.profiles[name] = profile;

		if(activate){ this.activate(name); }
	}
	activate(name){
		let profile = this.profiles[name];

		if(this.activeName){
			this.history.push(this.activeName);
		}

		this.active = profile;
		this.activeName = name;
		this.enable();
	}
	deactivate(){
		let previous = false;

		this.active = false;

		if(this.history.length){
			previous = this.history.pop();
			this.activate(previous);
		} else {
			this.disable();
		}
	}
	enable(){
		if(this.enabled){ return; }

		$(document).on('keyup', this.func);
		$(document).on('keydown', this.func);

		this.enabled = true;
	}
	disable(){
		if(!this.enabled){ return; }

		$(document).unbind('keyup', this.func);
		$(document).unbind('keydown', this.func);

		this.enabled = false;
	}
	setCharMap(){
		this.charMap = {
			8: 'backspace',
			9: 'tab',
			13: 'enter',
			16: 'shift',
			17: 'ctrl',
			18: 'alt',
			19: 'pause',
			20: 'caps-lock',
			27: 'escape',
			32: 'space',
			33: 'page-up',
			34: 'page-down',
			35: 'end',
			36: 'home',
			37: 'left-arrow',
			38: 'up-arrow',
			39: 'right-arrow',
			40: 'down-arrow',
			45: 'insert',
			46: 'delete',
			48: '0',
			49: '1',
			50: '2',
			51: '3',
			52: '4',
			53: '5',
			54: '6',
			55: '7',
			56: '8',
			57: '9',
			65: 'a',
			66: 'b',
			67: 'c',
			68: 'd',
			69: 'e',
			70: 'f',
			71: 'g',
			72: 'h',
			73: 'i',
			74: 'j',
			75: 'k',
			76: 'l',
			77: 'm',
			78: 'n',
			79: 'o',
			80: 'p',
			81: 'q',
			82: 'r',
			83: 's',
			84: 't',
			85: 'u',
			86: 'v',
			87: 'w',
			88: 'x',
			89: 'y',
			90: 'z',
			91: 'left-window-key',
			92: 'right-window-key',
			93: 'select-key',
			96: 'numpad-0',
			97: 'numpad-1',
			98: 'numpad-2',
			99: 'numpad-3',
			100: 'numpad-4',
			101: 'numpad-5',
			102: 'numpad-6',
			103: 'numpad-7',
			104: 'numpad-8',
			105: 'numpad-9',
			106: 'multiply',
			107: 'plus',
			109: 'minus',
			110: 'decimal-point',
			111: 'divide',
			112: 'f1',
			113: 'f2',
			114: 'f3',
			115: 'f4',
			116: 'f5',
			117: 'f6',
			118: 'f7',
			119: 'f8',
			120: 'f9',
			121: 'f10',
			122: 'f11',
			123: 'f12',
			144: 'num-lock',
			145: 'scroll-lock',
			186: 'semi-colon',
			187: 'equal-sign',
			188: 'comma',
			189: 'dash',
			190: 'period',
			191: 'forward-slash',
			192: 'grave-accent',
			219: 'open-bracket',
			220: 'back-slash',
			221: 'close-braket',
			222: 'single-quote'
		};
	}
}

const shortcutKeys = new ShortcutKeys();
