/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Tabstrip{
	constructor(opts, view = false){
		let options = Kendo.getOptions(opts, 'kendoTabStrip');
		let wrapper = Dom.el('tabstrip', {}, opts.container);

		for(let tab of opts.tabs){
			let content = Dom.el('tabcontent', {});
			options.dataSource.push({text: tab, content: content[0].outerHTML});
		}

		this.tabstrip = wrapper.kendoTabStrip(options).data('kendoTabStrip');
		this.tabstrip.select(opts.active || 0);

		if(opts.noBorder){
			this.tabstrip.wrapper.addClass('no-border');
		}

		if(view){ view.register('tabstrips', opts.key, this); }
	}
}
