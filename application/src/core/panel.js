/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const PanelSet = {
		create(opts, first = true, view = false, output = {}){
			let props = this.sanitize(opts);
			let tag = first ? 'panelset' : 'panel' + props.dir;
			let el = Flex.el(tag, props.attribs, props.container);

			this.setBorder(el, props, first);

			if(props.items){
					for(let panel of props.items){
						panel.dir = props.dir === 'col' ? 'row' : 'col';
						panel.container = el;
						this.create(panel, false, view, output);
					}
			}

			if(view && props.content){ view.setContent(props.content, el); }
			if(view){ view.register('panels', opts.key, el); }
			if(opts.key){ output[opts.key] = el; }

			return output;
		},
		setBorder(el, props, first){
			if(!props.border){
				el.addClass('k-content');
				return;
			}

			el.addClass('k-widget');

			if(!props.border){
				el.addClass('panel-border-none');
			} else if(!first){
				el.addClass('panel-border-' + props.dir);
			}

		},
		sanitize(obj){
			let props = {
				attribs: {},
				container: false,
				dir: (obj.dir || obj.direction) || 'col'
			};

			for(let [key, value] of Object.entries(obj)){
				switch(key){
					case 'border':
						props.border = value;
					break;
					case 'dir':
					case 'direction':
						props.dir = value;
					break;
					case 'items':
						props.items = value;
					break;
					case 'content':
						props.content = value;
					break;
					case 'container':
						props.container = value;
					break;
					case 'key':
						props.key = key;
					break;
					default:
						props.attribs[key] = value;
					break;
				}
			}

			props.attribs.dir = props.dir;
			return props;
		}
};
