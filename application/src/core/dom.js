/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Dom = {
		el(tag, opts = {}, container = false){
				let props = this.getProperties(Object.assign({}, opts));
				let el = $('<' + tag + '>', props.attribs);

				el.css(props.css);
				el.addClass(props.styles.join(' '));

				if(container){ el.appendTo(container); }

				return el;
		},
		getProperties(opts){
			let props = {
				css: {},
				attribs: {},
				other: {},
				styles: opts.style ? opts.style.split(' ') : []
			};

			delete opts.style;
			delete opts.tag;
			delete opts.container;

			for(let [key, value] of Object.entries(opts)){
				switch(key){
					case 'padding':
						props.css.padding = value;
					break;
					case 'paddingTop':
						props.css.paddingTop = value;
					break;
					case 'paddingBottom':
						props.css.paddingBottom = value;
					break;
					case 'paddingLeft':
						props.css.paddingLeft = value;
					break;
					case 'paddingRight':
						props.css.paddingRight = value;
					break;
					case 'margin':
						props.css.margin = value;
					break;
					case 'marginTop':
						props.css.marginTop = value;
					break;
					case 'marginBottom':
						props.css.marginBottom = value;
					break;
					case 'marginLeft':
						props.css.marginLeft = value;
					break;
					case 'marginRight':
						props.css.marginRight = value;
					break;
					case 'width':
					case 'w':
						props.css.width = value;
					break;
					case 'height':
					case 'h':
						props.css.height = value;
					break;
					case 'maxWidth':
					case 'maxW':
						props.css.maxWidth = value;
					break;
					case 'maxHeight':
					case 'maxH':
						props.css.maxHeight = value;
					break;
					case 'minWidth':
					case 'minW':
						props.css.minWidth = value;
					break;
					case 'minHeight':
					case 'minH':
						props.css.minHeight = value;
					break;
					case 'backgroundColor':
						props.css.backgroundColor = value;
					break;
					case 'background':
						props.css.background = value;
					break;
					case 'lineHeight':
						props.css.lineHeight = value;
					break;
					case 'bind':
						this.setBind(props, value);
					break;
					case 'role':
						props.attribs['data-role'] = value;
					break;
					case 'flex':
						props.css.flex = value;
					break;
					case 'fontSize':
						props.css.fontSize = value;
					break;
					case 'bold':
						props.css.fontWeight = 'bold';
					break;
					case 'overflow':
						props.css.overflow = value;
					break;
					case 'uppercase':
					case 'upper':
						props.styles.push('uppercase');
					break;
					case 'lowercase':
					case 'lower':
						props.styles.push('lowercase');
					break;
					case 'capitalize':
						props.styles.push('capitalize');
					break;
					case 'css':
						props.attribs.style = value;
					break;
					case 'change':
					case 'click':
					case 'visible':
					case 'enabled':
					case 'value':
					case 'invisible':
					case 'checked':
						this.setBind(props, key, value);
					break;
					case 'label':
					case 'primary':
					case 'rangeText':
					case 'wrapper':
					case 'key':
					case 'desc':
					case 'enabled':
						props.other[key] = value;
					break;
					default:
						props.attribs[key] = value;
					break;
				}
			}

			this.setBinds(props);

			return props;
		},
		setBinds(props){
			if(!props.binds){ return; }

			let bind = JSON.stringify(props.binds).match(/{(.*)}/)[1].replace(/"/g, '');

			props.attribs['data-bind'] = bind;
		},
		setBind(props, key, value){
			if(Utils.isNullOrUndefined(value)){
				this.parseBinds(props, key);
			}

			if(Utils.isBoolean(value) || Utils.isFunction(value)){
				if(key === 'visible'){
						props.css.display = value ? undefined : 'none';
				} else {
					props.attribs[key] = value;
				}
			} else {
				props.binds = props.binds || {};

				switch(key){
					case 'change':
						props.binds.events = props.binds.events || {};
						props.binds.events[key] = value;
					break;
					default:
						props.binds[key] = value;
					break;
				}
			}

		},
		parseBinds(props, str){
			let arr = str.replace(' ', '').replace('events:{', '').replace(',,', ',').replace('}','').split(',');

			for(let token of arr){
				let split = token.split(':');
				this.setBind(props, split[0], split[1]);
			}
	}
};
