/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class WebWorker{
	constructor(script, onMessage = function(){}){
		let path = 'secure/workers/{1}.include'.replace('{1}', script);
		this.worker = new Worker(path);

		this.worker.addEventListener('message', function(evt){
			onMessage(evt.data);
		}.bind(this), false);
	}
	postMessage(msg){
		this.worker.postMessage(msg);
	}
	terminate(){
		this.worker.terminate();
	}
}
