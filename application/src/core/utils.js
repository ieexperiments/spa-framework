/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Utils = {
	isNumber(val){
		return (typeof val === 'number');
	},
	isInteger(val){
		let x = parseFloat(val);
		return !isNaN(val) && (x | 0) === x;
	},
	isString(val){
		return (val instanceof String);
	},
	isArray(val){
		return (val instanceof Array);
	},
	isBoolean(val){
		return (typeof val === 'boolean');
	},
	isObject(val){
		return (val instanceof Object);
	},
	isDate(val){
		return (val instanceof Date);
	},
	isFunction(val){
		return (typeof val === 'function');
	},
	isUndefined(val){
		return (val === undefined);
	},
	isNull(val){
		return (val === null || val === '');
	},
	isNullOrUndefined(val){
		return (this.isNull(val) || this.isUndefined(val) || val === '');
	},
	isBetween(num, min, max){
		return (num <= max && num >= min);
	},
	lpad(value, pad, len){
		let str = this.isNumber(value) ? value.toString() : value;
		while(str.length < len){ str = pad + str; }
		return str;
	},
	rpad(value, pad, len){
		let str = this.isNumber(value) ? value.toString() : value;
		while(str.length < len){ str = str + pad;  }
		return str;
	},
	trim(str){
		return str.replace(/^\s+|\s+$/g, '');
	},
	arrayFirst(arr){
		return arr[0];
	},
	arrayLast(arr){
		return arr[arr.length - 1];
	},
	arrayRemove(arr, item){
		if(arr.indexOf(item) < 0){ return; }
		arr.splice(arr.indexOf(item), 1);
	},
	arrayExists(arr, item){
		return arr.indexOf(item) >= 0 ? true : false;
	},
	precision(value, precision){
		if(!precision){ return Math.round(value); }

		let multiplier = Math.pow(10, precision || 0);
		return Math.round(value * multiplier) / multiplier;
	},
	toRadians(num){
		return num * Math.PI / 180;
	},
	toDegrees(num){
		return num * 180 / Math.PI;
	},
	validateEmail: function(email){
		let regexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
		return regexp.test(email);
	},
	formatPhone: function(phone){
		phone = phone.toString().replace(/\D/g, '');
		let match = phone.match(/^(\d{3})(\d{3})(\d{4})$/);
		return (!match) ? null : '(' + match[1] + ') ' + match[2] + '-' + match[3];
	},
	formatBytes: function(size){
		let idx = 0;
		let units = [' Bytes',' KB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];

		while(size > 1000){
			size = size / 1000;
			idx++;
		}

		return Math.max(size, 0.1).toFixed(1) + units[idx];
	},
	sortObjArray(arr, prop){
		arr.sort(function(a, b){
			if(a[prop] < b[prop]){ return -1; }
			if(a[prop] > b[prop]){ return 1; }
			return 0;
		});

		return arr;
	},
	sortTree(a, b){
		let key1 = (a.hasChildren ? '0' : '1') + a.text;
		let key2 = (b.hasChildren ? '0' : '1') + b.text;

		if(key1 < key2){ return -1; }
		if(key1 > key2){ return 1; }
		return 0;
	},
	modelBulkSet(model, obj){
		for(let [key, value] of Object.entries(obj)){
			model.set(key, value);
		}
	},
	setFromDotPath(obj, path, value){
		path.split('.').reduce(function(prev, cur, idx, arr){
			let isLast = (idx === arr.length - 1);
			if(isLast){ return (prev[cur] = value); }
			return (Utils.isObject(prev[cur])) ? prev[cur] : (prev[cur] = {});
		}, obj);

		return obj;
	},
	uuid(){
		let date = new Date().getTime();
		let random = false;
		let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(chr){
			random = (date + Math.random() * 16) % 16 | 0;
			date = Math.floor(date / 16);
			return (chr === 'x' ? random : (random&0x7 | 0x8)).toString(16);
		});

		 return uuid;
	},
	createHierarchy(data, id, parent, root = 0){
		let hash = {};

		for(let item of data){
			let nid = item[id];
			let pid = item[parent];
			hash[nid] = hash[nid] || [];
			hash[pid] = hash[pid] || [];

			item.items = hash[nid];
			hash[pid].push(item);
		}
		/*
		function removeEmpty(node){
			if(!node.items.length){
				delete node.items;
			} else {
				for(let item of node.items){
					removeEmpty(item);
				}
			}
		}
		*/
		let dataSource = hash[root] || [];
		//for(let node of dataSource){ removeEmpty(node); }

		return dataSource;
	},
	traverseDataSource(source, func){
		function traverse(nodes){
			nodes.forEach(function(node){
				func(node);

				if(node.hasChildren){
					traverse(node.children.view());
				}
			 });
		}

		traverse(source.view());
	},
	ajax(obj){
		if(!obj.route){	return; }

		obj.data = obj.data || {};
		obj.data.route = obj.route;
		obj.success = obj.success || function(){};
		obj.error = obj.error || function(err){};

		$.ajax({
			url: obj.path || 'ajax',
			type: 'post',
			cache: false,
			data: JSON.stringify(obj.data),
			dataType: 'json',
			contentType: 'application/json',
			success: function(json){ obj.success(json); },
			error: function(err){ obj.error(err); }
		});
	},
	ajaxDownload(obj){
		 let url = false;

		if(!obj.route){	return; }
		if(!obj.filename){ self.error('AJAX file download requires a filename'); }

		obj.data = obj.data || {};
		obj.data.route = obj.route;
		obj.error = obj.error || function(err){console.log(err);};

		$.ajax({
			url: obj.path || 'ajax',
			type: 'post',
			cache: false,
			data: JSON.stringify(obj.data),
			dataType: 'binary',
			processData: false,
			xhrFields : {responseType: 'arraybuffer'},
			contentType: 'application/json',
			success: function(blob){
				blob = new Blob([blob], {type : 'application/octet-binary'});
				url = window.URL.createObjectURL(blob);
				dom.el('a', {href: url, download: obj.filename})[0].click();
				window.URL.revokeObjectURL(url);
			},
			error: function(err){ obj.error(err); }
		});
	},
	ajaxUpload(obj){
		let formData = new FormData();
		/*
		var file_data = $('#avatar').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data)
		form_data.append('user_id', 123)
		*/
		$.ajax({
			url: '/upload_avatar',
			type: 'post',
			cache: false,
			dataType: 'script',
			contentType: false,
			processData: false,
			data: formData
		});
	},
	websocketURI(obj = {}){
		let location = window.location;

		obj.secure = obj.host ? obj.secure : location.protocol === 'https:';
		obj.host = obj.host || location.hostname;
		obj.port = obj.port || location.port;

		let uri = obj.secure ? 'wss://' : 'ws://';
		uri += obj.host;
		uri += obj.port ? ':' + obj.port : '';
		uri += obj.path ? '/' + obj.path : '';

		return uri;
	},
	asyncAppend(async, module, method, payload){
		async.push(this.asyncFunc(module, method, payload));
	},
	asyncFunc(module, method, payload){
		return function(callback){ module[method](payload, callback); };
	},
	asyncBatch(obj, callback){
		let isArray = this.isArray(obj.payload);
		let list = isArray ? [] : {};

		obj.async = obj.async || 'series';
		callback = callback || function(){};

		if(isArray){
			for(let item of obj.payload){
				list.push(this.asyncFunc(obj.module, obj.method, item));
			}
		} else {
			for(let [key, payload] of Object.entries(obj.payload)){
				list[key] = this.asyncFunc(obj.module, obj.method, payload);
			}
		}

		if(obj.async === 'series'){
			async.series(list, callback);
		} else {
			async.parallel(list, callback);
		}
	},
	menuDataItem(evt){
		let item = $(evt.item);
		let menu = item.closest('.k-menu');
		let dataItem = evt.sender.options.dataSource;

		let index = item.parentsUntil(menu, '.k-item').map(function(){
			return $(this).index();
		}).get().reverse();

		index.push(item.index());

		for(let idx = -1, len = index.length; ++idx < len;){
			dataItem = dataItem[index[idx]];
			dataItem = idx < (len - 1) ? dataItem.items : dataItem;
		}

		return ({el: item, dataItem: dataItem});
	},
	getAllSelectors(){
		let output = {};
		for(let sheet of document.styleSheets){
			rules = sheet.rules || sheet.cssRules;

			console.log(rules);

			for(let rule of rules){
				if(Utils.isString(rule.selectorText)){ output[rule.selectorText] = true; }
			}
		}

		return output;
	},
	selectorExists(selector) {
		let selectors = Utils.getAllSelectors();

		console.log(selectors);
		return selectors[selector];
	}
};
