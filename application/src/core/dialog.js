/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
class Dialog{
	constructor(opts, view = false){
		let options = Kendo.getOptions(opts, 'kendoDialog');
		let wrapper = Dom.el('div', {}, opts.container);

		this.dialog = wrapper.kendoDialog(options).data('kendoDialog');
		if(view){ view.register('dialog', opts.key, this); }
	}

}
