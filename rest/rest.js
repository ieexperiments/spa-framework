/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/

const fs = require('fs');
const request = require('request');

const Rest = {
	getEndpoints(uri, callback){
		let parms = {
			uri: uri,
			method: 'get'
		};

		request(parms, function(err, res, json){
			if(err){
				callback(err, null);
				return;
			}

			try{ json = JSON.parse(json || '{}'); } catch(error){}

			callback(null, json);
		});
	}

};

module.exports = Rest;

Rest.getEndpoints('https://imaginationeverywhere.info/wp-json/wp/v2/', function(err, res){
	console.log(err);
	console.log(res);
});
