/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
global.archivePath = '/opt/archive/';
global.tempPath = './temp/';
global.histPath = './history/';
global.fm = require('../shared/file-manager');
global.SQLite = require('../shared/sqlite');
global.fs = require('fs');
global.async = require('async');
global.moment = require('moment');
global.Utils = require('../shared/utils');
global.TrackMerge = require('./modules/track-merge');

class Tracks{
	constructor(){
		global.sqlite = new SQLite(['tracks']);

		let sql = 'delete from tracks where callsign = 0';

		sqlite.exec({db: 'tracks', sql: sql}, function(err, results){
			if(err){ console.log(err); }
			global.TrackMerge.process();
		}.bind(this));
	}
}

new Tracks();
