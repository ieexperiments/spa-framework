/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
global.archivePath = '/opt/archive/';
global.tempPath = './temp/';
global.fm = require('../shared/file-manager');
global.SQLite = require('../shared/sqlite');
global.fs = require('fs');
global.async = require('async');
global.moment = require('moment');
global.Utils = require('../shared/utils');
global.binary = require('../shared/binary');
global.vector2d = require('../gis/Vector2d');
global.GisUtils = require('../gis/utils');
global.Geojson = require('../gis/geojson');
global.Reader = require('./modules/reader');
global.Aggregator = require('./modules/aggregator');
global.Densify = require('./modules/densify');
global.DBLoader = require('./modules/db-loader');
global.Tracks = {};
global.Seconds = 0;

const FileRotator = require('./modules/file-rotate');

class Tracks{
	constructor(){
		global.sqlite = new SQLite(['tracks']);

		let sql = 'delete from tracks;';

		sqlite.exec({db: 'tracks', sql: sql}, function(err, results){
			if(err){ console.log(err); }
			global.fileRotator = new FileRotator();
		}.bind(this));
	}
}

new Tracks();
