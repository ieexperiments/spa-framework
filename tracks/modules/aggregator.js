/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const attribIndex = require('./attrib-index');

const Aggregator = {
	merge(obj, append){
		let track = this.getTrack(obj);
		let attribs = this.getAttribs(obj);
		let len = track.hist.length;

		track.comm = attribs.comm || track.comm;
		track.ga = attribs.ga || track.ga;
		track.callsign = obj.callsign || track.callsign;
		track.dept = (obj.dept && obj.dept.length <= 4) ? obj.dept : track.dept;
		track.dest = (obj.dest && obj.dest.length <= 4) ? obj.dest : track.dest;
		track.mode3 = obj.mode3 || track.mode3;
		track.acftType = obj.acftType || track.acftType;

		//if(len && (Seconds - track.hist[len - 1].sec > 30)){
			//this.remove(obj.id);
			//track = this.getTrack(obj);
		//}

		//if(append && (!attribs.tfmdg && !attribs.coasting && obj.alt)){
		if(append){
			let hash = obj.point.join(':');

			if(!track.hash || (track.hash !== hash)){
				track.hist.push({
					sec: Seconds,
					point: obj.point,
					speed: obj.speed,
					heading: obj.heading,
					alt: obj.alt,
					mode3: track.mode3
				});

				track.hash = hash;
			}
		}
	},
	remove(id){
		if(!Tracks[id]){ return; }

		let track = Tracks[id];

		if((track.ga || track.comm) && track.hist.length >= 20){
			//Densify.process(track);
			DBLoader.process(track);
		}

		delete Tracks[id];
	},
	processTrack(track){
		let linestring = [];

		for(let data of track.hist){
			linestring.push(data.point);
		}
	},
	getAttribs: function(obj){
		let key = obj.flags;

		if(!key){ return false; }

		if(!attribIndex[key]){
			console.log('missing attribs', key);
			return false;
		}

		let attribs = attribIndex[key];

		let out = {
			comm: (attribs[1] & 32) ? true : false,
			ga: (attribs[1] & 64) ? true : false,
			tfmdg: (attribs[6] & 4) ? true : false,
			coasting: (attribs[0] & 32) ? true : false
		};

		return out;
	},
	getTrack: function(obj){
		let id = obj.id;

		if(!Tracks[id]){
			Tracks[id] = {tid: id, comm: false, ga: false, callsign: false, dept: false, dest: false, hist: []};
		}

		return Tracks[id];
	}
};

module.exports = Aggregator;
