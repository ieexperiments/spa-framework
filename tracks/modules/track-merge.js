/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const queue = async.queue(function(item, callback){
	TrackMerge.getTracks(item, callback);
}, 1);
let counter = 0;

const TrackMerge = {
	process(){
		let sql = 'select * from track_merge_callsign';

		sqlite.exec({db: 'tracks', sql: sql}, function(err, results){
			if(err){ console.log(err); }
			for(let item of results){
				queue.push(item);
			}
		});
	},
	getTracks(item, callback){
		let sql = 'select * from track_merge where callsign = $1 and dept = $2 and dest = $3 order by start';
		let bind = {$1: item.callsign, $2: item.dept, $3: item.dest};

		sqlite.exec({db: 'tracks', sql: sql, bind: bind}, function(err, results){
			if(err){ console.log(err); }
			let master = results.shift();
			this.merge(master, results, callback);
		}.bind(this));
	},
	merge(master, tracks, callback){
		let json = false;
		let remove = [];

		for(let track of tracks){
			let diff = track.start - master.end;

			if(Utils.isBetween(diff, 0, 1800)){
				if(!json){ json = fm.readJSON(global.histPath + master.id + '.json'); }
				master.end = track.end;
				let append = fm.readJSON(global.histPath + track.id + '.json').hist;
				fm.unlink(global.histPath + track.id + '.json');
				json.hist = json.hist.concat(append);
				remove.push(track.id);
				counter++;
				console.log(diff, counter);
			}
		}

		if(remove.length){
			fm.writeJSON(global.histPath + master.id + '.json', json);
			let sql = 'delete from tracks where id in (\'' + remove.join('\',\'') + '\')';

			sqlite.exec({db: 'tracks', sql: sql}, function(err, results){
				if(err){ console.log(err, sql); }
				callback();
			});
		} else {
			callback();
		}
	}
};

module.exports = TrackMerge;
