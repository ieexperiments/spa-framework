/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const queue = async.queue(function(bind, callback){
	DBLoader.exec(bind, callback);
}, 1);
let counter = 0;

const DBLoader = {
	process(data){
		let sec1 = data.hist[0].sec;
		let sec2 = Utils.arrayLast(data.hist).sec;
		let id = [data.callsign, sec1, sec2].join('_');
		let bind = {
			$1: id,
			$2: data.callsign,
			$3: sec1,
			$4: sec2,
			$5: data.dept,
			$6: data.dest,
			$7: data.mode3,
			$8: data.acftType
		};

		fm.writeJSON('./history/' + id + '.json', data);
		queue.push(bind);
	},
	exec(bind, callback){
		let sql = 'insert into tracks values ($1,$2,$3,$4,$5,$6,$7,$8)';

		sqlite.exec({db: 'tracks', sql: sql, bind: bind}, function(err, results){
			if(err){ console.log(err); }
			counter++;
			console.log(counter, 'tracks loaded');
			callback();
		});

	}
};

module.exports = DBLoader;
