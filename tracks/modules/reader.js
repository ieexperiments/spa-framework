/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const Reader = {
	read(file){
		let path = tempPath + file.basename.replace('.7z', '');
		this.reader = new binary.reader(fs.readFileSync(path, 'binary'));
		this.parseHeader();

		while(!this.reader.eof){
			this.parseSeconds(this.reader.timestamp());

			//console.log(Seconds);
			let buffer = false;
			let len = this.reader.uint32();
			let type = this.reader.uint8();

			len = this.reader.uint32();

			if(len){
				buffer = this.reader.raw(len);
				this.parseTracks(buffer, true);
			}

			type = this.reader.uint8();
			len = this.reader.uint32();

			if(len){
				buffer = this.reader.raw(len);
				this.parseDeletes(buffer);
			}
		}
	},
	parseSeconds: function(timestamp){
		let dt = moment(timestamp);
		let hour = Number(dt.format('HH'));
		let min = Number(dt.format('mm'));
		let sec = Math.round(Number(dt.format('ss.SS')));
		let out = sec + (min * 60) + (hour * 3600);

		Seconds = out;
	},
	parseHeader: function(){
		let buffer = this.reader.raw(this.reader.uint32());
		let reader = new binary.reader(buffer);

		while(!reader.eof){
			this.processTrack(reader, false);
		}
	},
	parseTracks: function(buffer){
		let reader = new binary.reader(buffer);

		while(!reader.eof){
			this.processTrack(reader, true);
		}
	},
	parseDeletes: function(buffer){
		let reader = new binary.reader(buffer);

		while(!reader.eof){
			Aggregator.remove(reader.uint16());
		}
	},
	processTrack: function(reader, append){
		let obj = {};

		let uint = reader.uint16();
		obj.id = binary.bitSubstr(uint, 15, 1);
		obj.deltaFlags = binary.bitSubstr(uint, 16, 16);

		uint = reader.uint24();
		obj.lon = binary.bitSubstr(uint, 22, 1);
		obj.deltaMode3 = binary.bitSubstr(uint, 23, 23);
		obj.deltaMode2 = binary.bitSubstr(uint, 24, 24);

		uint = reader.uint24();
		obj.lat = binary.bitSubstr(uint, 21, 1);
		obj.deltaArpt = binary.bitSubstr(uint, 22, 22);
		obj.deltaCallsign = binary.bitSubstr(uint, 23, 23);
		obj.deltaAcftType = binary.bitSubstr(uint, 24, 24);

		uint = reader.uint32();
		obj.speed = binary.bitSubstr(uint, 12, 1);
		obj.heading = binary.bitSubstr(uint, 21, 13);
		obj.alt = binary.bitSubstr(uint, 31, 22);
		obj.altSensor = binary.bitSubstr(uint, 32, 32);

		if(obj.deltaFlags){ obj.flags = reader.uint16(); }
		if(obj.deltaMode3){ obj.mode3 = reader.uint16(); }
		if(obj.deltaMode2){ obj.mode2 = reader.uint16(); }

		if(obj.deltaArpt){
			obj.dept = reader.string(reader.uint8());
			obj.dest = reader.string(reader.uint8());
			obj.eta = reader.uint16().toString();
		}

		if(obj.deltaCallsign){
			obj.callsign = reader.string(reader.uint8());
		}

		if(obj.deltaAcftType){
			obj.acftType = reader.string(reader.uint8());
		}

		obj.point = binary.decodePoint([obj.lon, obj.lat]);

		Aggregator.merge(obj, append);
	}
};

module.exports = Reader;
/*

parseHeader: function(buffer, sec){
	'use strict';

	var = this,
		reader = new binary.reader(buffer);

	while(!reader.eof){
		processTrack(reader, sec, false);
	}
},
parseDeletes: function(buffer, sec){
	'use strict';

	var = this,
		reader = new binary.reader(buffer);

	while(!reader.eof){
		processDeletes(reader, sec);
	}
},
parseTracks: function(buffer, sec){
	'use strict';

	var = this,
		reader = new binary.reader(buffer);

	while(!reader.eof){
		processTrack(reader, sec, true);
	}
},
processDeletes: function(reader, sec){
	'use strict';

	var = this,
		tid = reader.uint16();

	filter.remove(tid, sec);
},
processTrack: function(reader, sec, append){
	'use strict';

	var = this,
		uint = false,
		len = false,
		lon = false,
		lat = false,
		obj = {};

	uint 				= reader.uint16();
	obj.id				= binary.bitSubstr(uint, 15, 1);
	obj.deltaFlags		= binary.bitSubstr(uint, 16, 16);

	uint 				= reader.uint24();
	obj.lon				= binary.bitSubstr(uint, 22, 1);
	obj.deltaMode3		= binary.bitSubstr(uint, 23, 23);
	obj.deltaMode2		= binary.bitSubstr(uint, 24, 24);

	uint 				= reader.uint24();
	obj.lat				= binary.bitSubstr(uint, 21, 1);
	obj.deltaArpt		= binary.bitSubstr(uint, 22, 22);
	obj.deltaCallsign	= binary.bitSubstr(uint, 23, 23);
	obj.deltaAcftType	= binary.bitSubstr(uint, 24, 24);

	uint				= reader.uint32();
	obj.speed			= binary.bitSubstr(uint, 12, 1);
	obj.heading			= binary.bitSubstr(uint, 21, 13);
	obj.alt				= binary.bitSubstr(uint, 31, 22);
	obj.altSensor		= binary.bitSubstr(uint, 32, 32);

	if(obj.deltaFlags){ obj.flags = reader.uint16(); }
	if(obj.deltaMode3){ obj.mode3 = reader.uint16() }
	if(obj.deltaMode2){ obj.mode2 = reader.uint16() }

	if(obj.deltaArpt){
		obj.dept = reader.string(reader.uint8());
		obj.dest = reader.string(reader.uint8());
		obj.eta = reader.uint16().toString();
	}

	if(obj.deltaCallsign){
		obj.callsign = reader.string(reader.uint8());
	}

	if(obj.deltaAcftType){
		obj.acftType = reader.string(reader.uint8());
	}


	obj.point = geom.decompressPoint([obj.lon, obj.lat]);

	filter.merge(obj, sec, append);
}
*/
