/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/

const Densify = {
	process(obj){
		let hist = obj.hist;
		let min = Utils.arrayFirst(hist).sec;
		let max = Utils.arrayLast(hist).sec;
		let linestring = [];
		let fill = {
			arr: [],
			distance: 0,
			sumSpeed: 0,
			maxSpeed: 0,
			minSpeed: 9999
		};

		for(let [idx, curr] of hist.entries()){
			let next = hist[idx + 1];

			linestring.push(curr.point);

			fill.arr.push({
				sec: curr.sec,
				speed: curr.speed,
				heading: curr.heading,
				alt: curr.alt,
				point: curr.point
			});

			fill.maxSpeed = (curr.speed > fill.maxSpeed) ? curr.speed : fill.maxSpeed;
			fill.minSpeed = (curr.speed < fill.minSpeed) ? curr.speed : fill.minSpeed;
			fill.sumSpeed += curr.speed;

			if(next){
				let vector = new vector2d(curr.point);
				//let bearing = vector.bearing(next.point);
				//let diff = Math.abs(curr.heading - next.heading);
				fill.distance += vector.distance(next.point, 'nm');
				this.fillSeconds(curr, next, fill);
			}
		}

		fill.count = fill.arr.length;
		fill.avgSpeed = fill.sumSpeed / fill.count;
		fill.time = fill.count / 60 / 60;
		fill.path = GisUtils.spline(linestring);
		fill.line = linestring;
		fill.distance = GisUtils.lineLength(fill.path, 'nm').distance;

		if(fill.distance > 500){
			this.calculatePath(fill);
		}
	},
	calculatePath(fill){
		let sumSpeed = 0;
		let points = [];

		for(let obj of fill.arr){
			sumSpeed += obj.speed;
			let frac = sumSpeed / fill.sumSpeed;
			let dist = fill.distance * frac;
			let point = GisUtils.pointOnLine(fill.path, dist, 'nm');
			points.push(point);
		}

		let geojson = new Geojson();
		geojson.multipoint(fill.line);
		geojson.linestring(fill.line);

		//console.log(JSON.stringify(geojson.featureCollection()));

		process.exit();
	},
	fillSeconds(a, b, fill){
		let idx = 1;
		let rad = Utils.toRadians(a.heading);
		let deltas = {
			sec: b.sec - a.sec,
			alt: b.alt - a.alt,
			speed: b.speed - a.speed,
			heading: Utils.toRadians(b.heading) - Utils.toRadians(a.heading)
		};

		while(idx < deltas.sec){
			let frac = idx / deltas.sec;
			let obj = {
				sec: a.sec + idx,
				speed: a.speed + (deltas.speed * frac),
				heading: Utils.toDegrees(rad + (deltas.heading * frac)),
				alt: a.alt + (deltas.alt * frac),
			};

			fill.arr.push(obj);
			fill.sumSpeed += obj.speed;
			idx++;
		}
	}
};

module.exports = Densify;
