/*jshint esversion: 6, varstmt: true, nonbsp: true, nocomma: true, freeze: true, eqeqeq: true, curly: true*/
const node7z = require('node-7z');

class FileRotator{
	constructor(){
		this.setQueue();
	}
	setQueue(){
		let files = fm.fileList({path: archivePath, detailed: true});
		let batch = [];
		let arr = [];

		for(let file of files){
			batch.push(file);
		}

		Utils.asyncBatch({module: this, method: 'extract', payload: batch}, function(err, results){
			console.log('err', err);
			for(let [key, track] of Object.entries(Tracks)){
				console.log('cleanup', key);
				arr.push(key);
			}

			for(let key of arr){
				Aggregator.remove(key);
			}

		});
	}
	extract(file, callback){
		let extractor = new node7z();

		console.log(file.basename);

		extractor.extractFull(file.path, tempPath).then(function(){
			Reader.read(file);
			fm.unlink(tempPath + file.basename.replace('.7z', ''));
			callback();
		}.bind(this)).catch(function(err){
			console.log(err);
		});
	}
}

module.exports = FileRotator;
